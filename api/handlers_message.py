"Salesforce message syncer, and callback from celery_worker"

import datetime, time, json, re, zlib
import re, pdb, traceback

# http://docs.djangoproject.com/en/dev/ref/models/instances/?from=olddocs
from django.core.exceptions import MultipleObjectsReturned

from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from remote.models import *
from assist.models import *
import crawl.tasks, crawl.procmail
from crawl.models import NotedTaskMeta
from plugins.models import *
from django.utils.translation import ugettext as _
from emitters import XMLCDATAEmitter
from xml.dom import *
from payment_client import *

RE_CASE = re.compile(r'(?i)case\s+(\d{8,})\b')
RE_CASE_2 = re.compile(r'(?i)(\d{8,}).*ref:')    
    
class MessagesHandler(BaseHandler):
    
    allowed_methods = ('GET', 'POST')
    
    #==========================================================================
    # Getting messages from the API
    
    def _format_message_for_sf(self, e, alias, body=None, strip_subject=False, max_msg_length=None):
        """
        Format an email specifically for Salesforce
        Returns tuple: message, content, contact_addrs
        """
        
        is_twitter = e.is_twitter()
        is_quickbox = e.is_quickbox()
                
        def format_address(name, addr):
            if not name:
                return addr
            else:
                return '%s (%s)' % (decode_header_unicode(name), addr)
        
        def format_address_tuples(l):
            "Takes a list of (name, addr) tuples and returns a formatted string."
            return ', '.join([format_address(name, addr) for name, addr in l])

        def format_addresses(l):
            "Takes the result of get_all() for some header line and returns a formatted string."
            if not l:
                return None
            l = email.utils.getaddresses(l)
            return format_address_tuples(l)
                    
        def format_contacts(contacts, addr_space=None):
            if not contacts:
                return None
            formatted_contacts = []
            for c in contacts:
                if (addr_space == ADDR_SPACE_TWITTER) and c.twitter_screen_name:
                    formatted_contacts.append('@%s' % c.twitter_screen_name)
                else:
                    formatted_contacts.append(format_address(c.get_full_name(), c.email))
            return ', '.join(formatted_contacts)
               
        email_addresses = []
        contacts = list(e.contacts.all())
        contact_addrs = uniqify([c.email for c in contacts])
            # sfusercontact phrase: Must compare IDs, not the actual objects ... or crash
            #.filter(remoteusercontact__remote_user__id=self.remote_user.id)\
            
        # We use the Contacts count to determine whether an email should be pushed to SF or not,
        #  but we use the actual Alias address (not Contact addresses) to determine (on the SF end)
        #  which Contact, Lead etc. this should be filed under -- this helps avoid the problem with
        #  contact secondary emails
        if len(contact_addrs) == 0:
            log(" -- No contacts for email %d, returning" % e.pk)
            return None, None
        
        # Format the email
        dt = e.date_sent
        if alias and dt:
            dt = dt.astimezone(alias.account_user.get_timezone())
        if dt:  
            display_date = dt.strftime('%m/%d/%Y %I:%M:%S %p')
        else:
            display_date = None
        body = body or e.get_body()     
        
        if e.type == Email.TYPE_TWITTER:
            addr_space = 'twitter'
        else:
            addr_space = 'rfc822'
            
        # Format the to/from addresses
        # (Quickbox needs special handling to account for forwarding)
        from_contacts = uniqify(list(e.contacts.filter(email2contact__type__in=[Email2Contact.TYPE_FROM])))
        fromm = format_contacts(from_contacts, addr_space=addr_space)
        to_contacts = uniqify(list(e.contacts.filter(email2contact__type__in=[Email2Contact.TYPE_TO, Email2Contact.TYPE_FWD])))
        to = format_contacts(to_contacts, addr_space=addr_space)
        cc_contacts = uniqify(list(e.contacts.filter(email2contact__type__in=[Email2Contact.TYPE_CC])))
        cc = format_contacts(cc_contacts, addr_space=addr_space)
        bcc_contacts = uniqify(list(e.contacts.filter(email2contact__type__in=[Email2Contact.TYPE_BCC])))
        bcc = format_contacts(bcc_contacts, addr_space=addr_space)
                         
        content = ''
        if fromm:
            content += '* From: %s\n' % fromm
        if to:
            content += '* To: %s\n' % to
        if cc:
            content += '* Cc: %s\n' % cc
        if bcc:
            content += '* Bcc: %s\n' % bcc
        if display_date:
            content += '* Received at: %s\n' % display_date
        
        for f in e.get_files():
            domain = alias.get_site().domain
            token, created = FileToken.objects.get_or_create_token(alias, f)
            url = 'http%s://%s/dash/%s/?to=/dash/%s/retrieve_file%%3ftoken=%s' % \
                ('s' if domain in settings.SSL_DOMAINS else '', domain, alias.account.base_url, alias.account.base_url, token.slug)
            content += '* Attachment: %s (%s)\n' % (f.name, url)
                
        content += '\n'
        
        # Remove angle brackets surrounding URLs, like <http://blah.com> --> http://blah.com
        # They sometimes cause trouble with Salesforce (?) [eg 1/14/10]
        try:
            body = re.sub(r'\<([^\>\r\n]+)\>', r'\1', body)
        except:
            log('_format_message_for_sf: failed to perform body regex.')
            log(traceback.format_exc())
            body = ''
            log('_format_message_for_sf: failed to perform body regex... setting to null')
        content += body
        
#        remote_contact_ids, contact_addrs = [], []        
#        for c in contacts:
#            contact_addrs.append(c.email)
#            for rfc in RemoteUserContact.objects.filter(remote_user=self.remote_user, contact=c):
#                remote_contact_ids.append(rfc.remote_id)    # This may be None if we don't know it
#        remote_contact_ids = uniqify(remote_contact_ids)
#        contact_addrs = uniqify(contact_addrs)[:20]
        
        if is_twitter:
            subject = _('Tweet: ') # if alias.account.is_subscribed() else _('Calenvy (tweet): ') 
            subject += body
        elif is_quickbox:
            subject = _('Note: ')
            if strip_subject:
                subject += mail.strip_subject_reply_or_forward(e.subject_normalized)
            else:
                subject += e.subject_normalized
        else:
            #arrow = u'\u2192' if e.from_contact.alias else u'\u2190'    # Unicode arrow to indicate incoming vs. outgoing mail
            subject = _('Email: ') # if alias.account.is_subscribed() else _('Email: '))
            if strip_subject:
                subject += mail.strip_subject_reply_or_forward(e.subject_normalized)
            else:
                subject += e.subject_normalized
        
        subject, more = ellipsize_and_more(subject, length_max=75, length_with_url_max=75)    # subject = 80 chars max on SF, allow room for ...
        
        #subject = strip_nonascii(subject)
        #content = strip_nonascii(content)
        # Limit the amount of stuff we can push to Salesforce
        if (max_msg_length is not None) and (len(content) > max_msg_length):
            content = content[:max_msg_length] + '\n...'      
            
        content += '\n\n' + _('[Powered by Calenvy Email Sync: https://%s/products/salesforce-automation/]') % Site.objects.get_current().domain
        
        return subject, content
    
    def _format_contact_for_emitter(self, contact, formatting=None):
        if formatting == 'sf':
            if contact.email:
                result = {'c': contact.email}
            else:
                result = None
        else:
            raise NotImplementedError
            # TODO: Fuller formatting with type, relation, other fields etc.
            
        return result
        
    def _format_contact_as_sf_lead(self, contact):
        # see http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_lead.htm
        # LastName is required by SF, but some of our contacts only have a first name. In this case,
        #  use the first name as the last name and leave the first name blank.
        #  Use the same field names as in Salesforce (case doesn't seem to matter, I use lowercase)
        result = {
            'email':        (contact.email or '')[:50],
            'firstname':    (contact.last_name and contact.first_name or '')[:40],
            'lastname':     (contact.last_name or contact.first_name or (contact.email and strip_nonalpha(contact.email.split('@')[0].split('.')[-1]).title()) or '*')[:80],
            'description':  (contact.description or '')[:2000],
            'company':      (contact.company or (contact.email and get_company_from_email(contact.email)) or contact.last_name or contact.first_name or '*')[:40],      # SF requires Company
            'phone':        (contact.phone or '')[:50],
            'leadsource':   contact.source_text or _('Calenvy (email)')
        }
        if contact.source_text:
            result['leadsource'] = contact.source_text
        return result
        
    def _format_contact_as_sf_contact(self, contact):
        # see http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_contact.htm
        # LastName is required by SF, but some of our contacts only have a first name. In this case,
        #  use the first name as the last name and leave the first name blank.
        #  Be a bit more aggressive about creating a last name than Calenvy is for its own contacts --
        #  try to get one from the email address if possible.
        #  Use the same field names as in Salesforce (case doesn't seem to matter, I use lowercase)        
        result = {
            'email':        (contact.email or '')[:50],
            'firstname':    (contact.last_name and contact.first_name or '')[:40],
            'lastname':     (contact.last_name or contact.first_name or (contact.email and strip_nonalpha(contact.email.split('@')[0].split('.')[-1]).title()) or '*')[:80],
            'description':  (contact.description or '')[:2000],
            #'company':     (contact.company or contact.last_name or contact.first_name or '*')[:40],      # SF requires Company
            'phone':        (contact.phone or '')[:50],
        }
        return result
    
    def _format_message_as_sf_case(self, message, alias, contact, prefix_lists):
        # see http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_case.htm
        #  Use the same field names as in Salesforce (case doesn't seem to matter, I use lowercase)
        subject, content = self._format_message_for_sf(message, alias, body=None, strip_subject=True, max_msg_length=10000)
        subject = ('%s | %s' % (contact.email, subject))[:255]
        result = {
            'subject':      subject,
            'description':  content[:32000],
            'origin':       'Email'
        }
        prefix_lists['case_subjects'].append(subject)
        return result
    
    def _format_actions_for_emitter(self, contacts_to_create, leads_to_create, cases_to_create, prefix_lists, formatting=''):
        if formatting == 'sf':
            result = []
            for contact in contacts_to_create:
                result.append({'create': {'Contact': self._format_contact_as_sf_contact(contact)}})   # 'Contact' must be capitalized
            for contact in leads_to_create:
                result.append({'create': {'Lead': self._format_contact_as_sf_lead(contact)}})         # 'Lead' must be capitalized
            for (message, alias, contact) in cases_to_create:
                result.append({'create': {'Case': self._format_message_as_sf_case(message, alias, contact, prefix_lists)}})              
                
        else:
            raise NotImplementedError
        
        return result
    
    
    def _format_all_contacts_for_emitter(self, contacts, formatting=''):
        if not isinstance(contacts, list):
            contacts = [contacts]
        contacts = uniqify(contacts)
        return [self._format_contact_for_emitter(c, formatting=formatting) for c in contacts]
    
    
    def _find_case_number(self, e, formatting=''):
        if e.subject:
            m = RE_CASE.search(e.subject) or RE_CASE_2.search(e.subject)
            if m:
                return m.group(1)
        return None
    

    def _format_all_case_keys_for_emitter(self, prefix_lists, formatting=''):
        # Format all the keys the SF app might use to look up cases:
        #  case numbers and case subjects
        result = []
        for s in prefix_lists['case_numbers']:
            result.append({'case_number': s})
        for s in prefix_lists['case_subjects']:
            result.append({'case_subject': s})
        return result

         
    def _format_message_for_emitter(self, e, alias, contacts, body=None, prefix_lists=None, formatting='', max_msg_length=None, include_date_sent=False, task_type=None, sync_opp=None):
        
        try: 
            if formatting == 'sf':
                subject, data = self._format_message_for_sf(e, alias, body=body, max_msg_length=max_msg_length)
                if not data:
                    return None
                    contacts = contacts[:10]
            else:
                raise NotImplementedError
                            
            case_number = self._find_case_number(e, formatting=formatting)
            if case_number and prefix_lists:
                prefix_lists['case_numbers'].append(case_number)
                
            message_attrs = {
                'ATTRS': True,
                'type': Email.TYPE_TWITTER if e.is_twitter() else Email.TYPE_RFC822,  # TODO: Cleanup to get rid of is_twitter()
                'db_id': e.id,
                'message_id': e.message_id,
                'formatting': formatting                                 
            }
            
            message_data = [
                {'contacts': self._format_all_contacts_for_emitter(list(contacts), formatting=formatting)},
                {'case_numbers': [{'case_number': case_number}] if case_number else []},
                {'subject': subject}
            ]
            
            if task_type:               # Task type field in Salesforce
                message_data.append({'task_type': task_type[:40]})  # 40 is max # of chars on Salesforce
            
            if sync_opp: #should we sync to opportunities? an experimental beta feature?
                message_data.append({'sync_opp': sync_opp})
            
            if include_date_sent:
                message_data.append({'date_sent': (e.date_sent or now()).strftime("%Y-%m-%d %H:%M:%S")})
            message_data.append({'data': data})
            
            result = {'message': [
                message_attrs,
                message_data
            ]}
        except:             # Raises exception if, e.g., message body is expired
            log(traceback.format_exc())
            return None
          
        return result
                
    def _mark_message_status(self, e, remote_user, status):
        """
        Mark the message as having been sent / synced / whatever.
        If it's been sent n times (n=3) without a response, mark the sync as failed so we can move on
        
        ??? What's the point of this?
        
        """
        
        e2r, created = Email2RemoteUser.objects.get_or_create(account=e.owner_account, email=e, remote_user=remote_user, remote_userid=remote_user.remote_userid, remote_organizationid=remote_user.remote_organizationid)
        e2r.status = status
        if status == Email2RemoteUser.STATUS_SENT:
            e2r.num_times_sent += 1
            if e2r.num_times_sent >= Email2RemoteUser.MAX_TIMES_SENT:
                log('[If its been sent n times (n=3), mark it as failed!] e2r.status = Email2RemoteUser.STATUS_FAILED')
                e2r.status = Email2RemoteUser.STATUS_FAILED
        e2r.save()
        
    def _get_message_status(self, e, remote_user):
        try:
            e2r = Email2RemoteUser.objects.get(email=e, remote_user=remote_user)
            return e2r.status
        except:
            return None
            
    def read(self, request, debug=False):
        try:
            """
            Get messages for syncing with Salesforce.
            
            If debug=True, don't actually mark any messages as synced -
            this is for stepping through code with pdb without changing the state,
            in order to debug problems with syncing for specific users.
            """
                                      
            n = now()
            alias = request.session['alias']
            account = alias.account
            remote_user = request.session['remote_user']  
            
            log("MessagesHandler.read: alias = %s (%d), remote_user = %d" % (alias.email, alias.pk, remote_user.pk))
            log('[1]')
                        
            max_bytes = None
            max_msgs = 80 # ???? Lowered from 100
            max_days = None if account.archive_mode else 7              # Sync from the beginning if archive mode
            max_msg_length = 22000 # ???? lowered from 25000
            timeout = None
            formatting = ''
            prefix_actions = False
            prefix_all_contacts = False
            prefix_all_case_keys = False
            prefix_more_flag = False
            prefix_more_wait_ms = 0
            prefix_task = None
            include_date_sent = False
            include_task_type = False
            mark_synced_db_ids = []
            mark_failed_db_ids = []
            sync_actions_messages_separately = False
            contact_emails = None
            include_failed = False
            include_incoming = True
            include_outgoing = True
            latest = False
            
            if remote_user.type == RemoteUser.TYPE_SF:
                if not remote_user.alias.account.service.allow_sf:
                    return {'success': False, 'msg': _('Your service level does not allow Salesforce integration.')}
            
                formatting = 'sf'
                                
                max_bytes = int(int(request.GET.get('max_bytes', '100000')) * 0.95)
                max_msg_length = int(max_bytes / 50) - 320
                max_days = None if account.archive_mode else 21     # Sync from the beginning if archive mode
                timeout = 50                    # don't take more than this many seconds (ALEX: was 40...)
                prefix_more_flag = False        # by default, don't do this -- only versions >= 2.16 understand this
                prefix_more_wait_ms = 0         # by default, don't do this -- only versions >= 2.30 understand this
                prefix_task = None              # by default, don't do this -- only versions >= 2.30 understand this
                prefix_all_contacts = True
                prefix_all_case_keys = False
                include_incoming = remote_user.alias.account.service.allow_sf_incoming
                include_outgoing = remote_user.alias.account.service.allow_sf_outgoing
                sync_actions_messages_separately = True

            def split_nums(s):
                if s:
                    return [int(i) for i in s.split(',')]
                else:
                    return []
            
            if 'max_bytes' in request.GET:
                max_bytes = int(request.GET['max_bytes']) 
            if 'max_msg_length' in request.GET:
                max_msg_length = int(request.GET['max_msg_length'])
            if 'max_msgs' in request.GET:
                max_msgs = min(int(request.GET['max_msgs']), 1000)
            if 'max_days' in request.GET:                           # if max_days is actually specified, this overrides everything else (including archive mode)
                max_days = min(int(request.GET['max_days']), 100)            
            if 'formatting' in request.GET:
                formatting = request.GET['formatting']
            if 'prefix_more_flag' in request.GET:
                prefix_more_flag = bool(int(request.GET['prefix_more_flag']))
            if 'prefix_more_wait_ms' in request.GET:
                prefix_more_wait_ms = bool(int(request.GET['prefix_more_wait_ms']))
            if 'prefix_task' in request.GET:                            
                prefix_task = int(request.GET['prefix_task'])               # actual ID of the NotedTaskMeta to get status for
            if 'prefix_actions' in request.GET:
                prefix_actions = bool(int(request.GET['prefix_actions']))    
            if 'prefix_all_contacts' in request.GET:
                prefix_all_contacts = bool(int(request.GET['prefix_all_contacts']))
            if 'prefix_all_case_keys' in request.GET:
                prefix_all_case_keys = bool(int(request.GET['prefix_all_case_keys']))
            if 'include_date_sent' in request.GET:
                include_date_sent = bool(int(request.GET['include_date_sent']))
            if 'include_task_type' in request.GET:
                include_task_type = bool(int(request.GET['include_task_type']))
            if 'mark_synced_db_ids' in request.GET:
                mark_synced_db_ids = split_nums(request.GET['mark_synced_db_ids'])
            if 'mark_failed_db_ids' in request.GET:
                mark_failed_db_ids = split_nums(request.GET['mark_failed_db_ids'])
            if 'contact_emails' in request.GET:
                contact_emails = uniqify([e.strip().lower() for e in request.GET['contact_emails'].split(',')])
            if 'include_failed' in request.GET:
                include_failed = bool(int(request.GET['include_failed']))       
            if 'latest' in request.GET:
                latest = bool(int(request.GET['latest']))
                            
            log('[2]')
    
            # By default, there's no need to sync again
            more_flag, more_wait_ms = False, 0
            
            # Are we doing an (IMAP) crawl in the background? If so, tell the client to wait a bit and then ask for more messages
            if NotedTaskMeta.objects.filter(status=NotedTaskMeta.STATUS_STARTED, type=NotedTaskMeta.TYPE_CRAWL, alias=alias).count() > 0:
                more_flag, more_wait_ms = True, 4000     # Still crawling messages in the background -- wait a bit before requesting more messages
                          
            # Messages this user has permission to sync
            #  If account has special "transparent_sync" flag enabled, one sync gets messages from all aliases
            if account.transparent_sync:
                queryset_perms = Email.objects.filter(owner_account=account)
            else:
                queryset_perms = Email.objects.filter(owner_alias=alias)

            #queryset_perms = queryset_perms.filter(Q(owner_alias=alias) | Q(contacts=alias.contact) | Email.objects.twitter_q())    # TODO: Revisit this
            
            log("contact_emails:", contact_emails)
            log("include_failed:", include_failed)
            log("latest:", latest)
            
            # Mark previous messages synced (or failed)
            log("mark_synced_db_ids:", mark_synced_db_ids)
            queryset_mark_synced = queryset_perms.filter(id__in=mark_synced_db_ids, remote_users=remote_user)
            for e in queryset_mark_synced:
                log("marking ", e.pk, " as synced")
                if not debug:
                    self._mark_message_status(e, remote_user, Email2RemoteUser.STATUS_SYNCED)
    
            # charge for the correctly synced emails
            amount = account.service.calc_discounted_price_per_synced_email(promo=account.promo)
            if account.remote_subscription_id and amount:
                for e in queryset_mark_synced:
                    try:
                        log("Trying to charge %f for email %d..." % (amount, e.id))
                        charge(amount, account, e)
                    except:
                        log("cannot charge for email ", e)
                        log(traceback.format_exc())
            
            log("mark_failed_db_ids:", mark_failed_db_ids)
            queryset_mark_failed = queryset_perms.filter(id__in=mark_failed_db_ids, remote_users=remote_user)
            for e in queryset_mark_failed:
                log("marking ", e.pk, " as failed: this could mean that Salesforce could not find a match") # ??? Does this mean that Salesforce can't find a match?
                if not debug:
                    self._mark_message_status(e, remote_user, Email2RemoteUser.STATUS_FAILED)
            
            log('[3]')
    
            if max_msgs == 0:
                log('[4]')
                return [{'success': True}, {'results': []}]
                        
            # Messages to be synced this time around
            log('[3aa]')
            queryset_sync = queryset_perms.exclude(message_type=EMAIL_TYPE_NOTED)
            queryset_sync = queryset_sync.exclude(source__in=[Email.SOURCE_FORMMAILCONF, Email.SOURCE_FOLLOWUP_REMINDER])   # Don't sync these to Salesforce
            if max_days is not None:                                                            # None = archive mode
                queryset_sync = queryset_sync.filter(date_sent__gte=now()-max_days*ONE_DAY)     #  only sync back this many days
            
            # Don't sync messages that have already been synced by anyone within the same Salesforce organization.
            # Get messages that are only in the STATUS_SENT_ACTIONS_ONLY or STATUS_SENT states (by excluding everything else).
            if include_failed:
                if remote_user.remote_organizationid is not None:
                    log(" -- using remote_organizationid [1]")
                    queryset_sync = queryset_sync.exclude(email2remoteuser__account=account, email2remoteuser__remote_organizationid=remote_user.remote_organizationid, email2remoteuser__status=Email2RemoteUser.STATUS_SYNCED)
                else:
                    queryset_sync = queryset_sync.exclude(email2remoteuser__account=account, email2remoteuser__remote_user=remote_user, email2remoteuser__status=Email2RemoteUser.STATUS_SYNCED)
            else:
                if remote_user.remote_organizationid is not None:
                    log(" -- using remote_organizationid [2]")
                    queryset_sync = queryset_sync.exclude(email2remoteuser__account=account, email2remoteuser__remote_organizationid=remote_user.remote_organizationid, email2remoteuser__status__in=[Email2RemoteUser.STATUS_SYNCED, Email2RemoteUser.STATUS_FAILED])
                else:
                    queryset_sync = queryset_sync.exclude(email2remoteuser__account=account, email2remoteuser__remote_user=remote_user, email2remoteuser__status__in=[Email2RemoteUser.STATUS_SYNCED, Email2RemoteUser.STATUS_FAILED])
            if not include_outgoing:
                # Exclude outgoing mails (i.e., those from somebody on the team)
                queryset_sync = queryset_sync.filter(from_contact__alias=None)
            if not include_incoming:
                # Exclude incoming mails (i.e., those from somebody not on the team)
                queryset_sync = queryset_sync.exclude(from_contact__alias=None)                
            if contact_emails is not None:
                queryset_sync = queryset_sync.filter(contacts__email__in=contact_emails)
                
            # Mails must have at least one non-team member as the From: or To: address
            # (Optimization since all current EmailRules require this)
            queryset_sync = queryset_sync.filter(email2contact__account=account, email2contact__contact__alias=None)

            if latest:
                queryset_sync = queryset_sync.order_by('-date_sent')
            else:
                queryset_sync = queryset_sync.order_by('date_sent')
            
            log('[3a]')
            
            # Get one more than max_msgs, so we know if we need to set the "more" flag
            queryset_sync = queryset_sync.distinct()[:max_msgs+1]
            messages_found = list(queryset_sync)
            if len(messages_found) > max_msgs:
                more_flag = True
            messages_found = messages_found[:max_msgs]
            
            # Use this optimization to retrieve a distinct list
            messages_with_actions, messages_formatted, messages_ignored = [], [], []
            contacts_to_create, leads_to_create, cases_to_create = [], [], []
            
            prefix_lists = {
                'contacts': [],
                'case_subjects':    [],
                'case_numbers':     []
            }
            
            # Iterate through all the possible messages for syncing, and find the relevant actions to perform.
            # Format the message bodies in this loop, since that's the slowest part of the syncer and we need to bail out
            # early if it's taking too long (before client times out). The other stuff (prefix_contacts, etc.) we
            # can format later.
                 
            while messages_found:
                
                # Iterate through the messages_found list by popping one element at a time
                #  We need to do it this way instead of using a for loop, so that we can 
                #  check further down if there are more messages 
                m = messages_found.pop(0)
                
                try:
                    
                    contacts_from_rules = []
                    process_this_message = True
                    sync_this_message = False
                    task_type = None                        # Salesforce Task object type field
                    sync_opp = None                         # Salesforce beta - do we want to  sync to opps?
                                                  
                    rules = EmailRule.objects.find_email_rules(m, remote_user.type)
                    
                    if rules:                               # None means ignore this mail completely
                        
                        # Grab the message text here -- it's used for two things:
                        #  using regexes to create a Contact or Lead, if applicable
                        #  the actual sync of the email
                        try:
                            body = m.get_body()
                        except:
                            log('Failed to get body via body = m.get_body()')
                            body = ''
                            log('Failed to get body via body = m.get_body()... setting to null')
                            
                        contacts_from_regex = []
                        contacts_from_nonregex = []
                                                         
                        for rule in rules:                 
                            
                            # The contacts for this one message+rule
                            # This also extracts special fields like leadsource into the contacts
                            
                            contacts_from_rule, from_regex = rule.find_email_contacts(m, content=body, viewing_alias=alias)
                            
                            if contacts_from_rule is None:      # None means ignore this mail completely
                                process_this_message = False
                                break
                                
                            if from_regex:
                                contacts_from_regex.extend(contacts_from_rule)
                                
                            actions = rule.actions()
                            for a in actions:
                                
                                if a['action'] == 'sync_opp':
                                    sync_opp = True                                   
                                
                                if a['action'] == 'sync':
                                    sync_this_message = True
                                    task_type = include_task_type and a.get('task_type')      # Salesforce task type
                                   
                                # Only get the actions for mails which haven't had their actions sent over.
                                #  (Actions are creating contact/lead/case -- syncing the message itself doesn't count)
                                if (not sync_actions_messages_separately) or (self._get_message_status(m, remote_user) is None):
                                    if a['action'] == 'create':
                                        if m not in messages_with_actions:
                                            messages_with_actions.append(m)
                                        if a['type'] == 'contact':
                                            contacts_to_create.extend(contacts_from_rule)
                                        elif a['type'] == 'lead':
                                            leads_to_create.extend(contacts_from_rule)
                                        elif a['type'] == 'case' and contacts_from_rule:
                                            cases_to_create.append((m, alias, contacts_from_rule[0]))
                                        
                            contacts_from_rules.extend(contacts_from_rule)
                            prefix_lists['contacts'].extend(contacts_from_rule)
                        
                        if process_this_message is False:
                            continue
                        
                        # If contacts from regex, ignore all other contacts for this mail 
                        if contacts_from_regex:
                            contacts_from_rules = contacts_from_regex
                            
                        contacts_from_rules = uniqify(contacts_from_rules)
                        prefix_lists['contacts'].extend(contacts_from_rules)

                        if sync_this_message and m not in [message for message, formatted in messages_formatted]:
                            mf = self._format_message_for_emitter(m, alias, contacts_from_rules, 
                                body=body, prefix_lists=prefix_lists, formatting=formatting, max_msg_length=max_msg_length,  
                                include_date_sent=include_date_sent, task_type=task_type, sync_opp=sync_opp)
                            messages_formatted.append((m, mf))
                        
                        # If any of the contacts_from_rules were created by regex, instead of being part of the original message,
                        #  add them to the message                        
                        for c in contacts_from_rules:
                            if c not in m.contacts.all():
                                m.create_email2contact_by_contact(c, alias.account, Email2Contact.TYPE_RULE, viewing_alias=alias, footer_fields=None)
                    
                    else:
                        # Ignore this message -- mark it as synced so we don't keep trying to resync it, otherwise we never get past it
                        messages_ignored.append(m)
                                                                
                except:
                    log("MessagesHandler.read: Couldn't sync message %d" % m.pk)
                    log(traceback.format_exc())
                    messages_ignored.append(m)
                
                # If we're getting close to the client's timeout limit, we better finish up
                #  (For local debugging, disable the timeout)
                if (settings.SERVER != 'local') and (timeout and (now()-n > timeout*ONE_SECOND)):
                    log("Timing out: timeout = ", timeout)
                    if messages_found:      # More messages we didn't get around to?    
                        more_flag = True    #  Do them in the next sync
                    break
                        
            # Also send to Salesforce any contacts that have been tagged "salesforce"
            if prefix_actions:
                additional_contacts_to_create = list(Contact.objects.filter(owner_account=alias.account, sf_sync_pending=True))
                contacts_to_create.extend(additional_contacts_to_create)
                prefix_lists['contacts'].extend(additional_contacts_to_create)
                
            # Format the actions
            contacts_to_create = uniqify(contacts_to_create)    
            leads_to_create = uniqify(leads_to_create)
            cases_to_create = uniqify(cases_to_create)
            actions_create_formatted = prefix_actions and self._format_actions_for_emitter(contacts_to_create, leads_to_create, cases_to_create, prefix_lists, formatting=formatting)
            
            # Format the prefix lists -- do this last because the previous steps all modify prefix_lists
            for key in prefix_lists.keys():
                prefix_lists[key] = uniqify(prefix_lists[key])
            prefix_contacts_formatted = prefix_all_contacts and self._format_all_contacts_for_emitter(prefix_lists['contacts'], formatting=formatting)
            prefix_case_keys_formatted = prefix_all_case_keys and self._format_all_case_keys_for_emitter(prefix_lists, formatting=formatting)
                                    
            log("MessagesHandler.read: actions_create_formatted: ", alias, actions_create_formatted)
            
            if latest:
                messages_formatted.reverse()
            
            # Special workaround for SF not being able to create leads/etc. and sync mails in one go
            if sync_actions_messages_separately:
                if actions_create_formatted:
                    messages_formatted = []
                

            log('[3b]')
            # Mark the messages as processed (or failed if sent some max # of times) 
            # Workaround for SF: mark messages as "SENT_ACTIONS_ONLY" first time through, then STATUS_SENT second time.
                        
            if sync_actions_messages_separately and prefix_actions and messages_with_actions:
                for message in messages_with_actions:
                    if not debug:
                        self._mark_message_status(message, remote_user, Email2RemoteUser.STATUS_SENT_ACTIONS_ONLY)
                    more_flag = True        # We'll need another sync to get to STATUS_SENT
            else:                
                for message, formatted in messages_formatted:
                    if not debug:
                        self._mark_message_status(message, remote_user, Email2RemoteUser.STATUS_SENT)
                for message in messages_ignored:
                    if not debug:
                        self._mark_message_status(message, remote_user, Email2RemoteUser.STATUS_SENT)
                        
            # Generate the output suitable for the XML emitter, under max_bytes bytes if necessary, and mark the messages 
            def xml_data(messages_formatted):
                result = []
                result.append({'success': True})
                result_inner = [] 
                if prefix_task:
                    task = NotedTaskMeta.objects.get(id=prefix_task, alias=alias)
                    result_inner.append({'task': {'num_processed_msgs': task.get_custom().get('num_processed_msgs', 0)}})
                if prefix_more_flag:
                    result_inner.append({'more': more_flag})         
                if prefix_more_wait_ms:
                    result_inner.append({'more_wait_ms': more_wait_ms})     # TODO: Use a real value here    
                if prefix_all_contacts:
                    result_inner.append({'all_contacts': prefix_contacts_formatted})       
                if prefix_all_case_keys:
                    result_inner.append({'all_case_keys': prefix_case_keys_formatted})
                if prefix_actions:
                    result_inner.append({'actions': actions_create_formatted}) 
                if not (sync_actions_messages_separately and messages_with_actions and prefix_actions):
                    result_inner.extend([formatted for message, formatted in messages_formatted])
                result.append({'results': result_inner})
                return result
                    
            if max_bytes is None:
                log('[3c]')
                result = xml_data(messages_formatted)
            else:
                # Result is too big for the max # of bytes we can send back,
                # remove messages until we're under the limit
                log('[3d]')
                result = xml_data(messages_formatted)
                emit = XMLCDATAEmitter(None, None, None)
                xml = emit.render_data(result)
                while (len(xml) > max_bytes) and messages_formatted:
                    log("len(xml):", len(xml), " max_bytes:", max_bytes, "# messages:", len(messages_formatted))
                    emit = XMLCDATAEmitter(None, None, None)
                    messages_formatted = messages_formatted[:-1]
                    more_flag = True            # We're not sending all the messages, so send more of them next time
                    result = xml_data(messages_formatted)
                    xml = emit.render_data(result)
                if len(xml) > max_bytes:
                    log('[6]')
                    return {'success': False, 'msg': 'Cannot return result under %d bytes' % max_bytes}
                     
            # Mark any contacts we created on the SF end as no longer needing sync to SF
            if prefix_actions:
                for contact in contacts_to_create:
                    contact.sf_sync_pending = False
                    contact.save()
                           
            log('[8] result is:')
            log(result)
            
            return result
        except:
            log("MessagesHandler.read: failed")
            log(traceback.format_exc())
            
    #==========================================================================
    # Sending messages to the API
            
    def _format_sync_message_for_emitter(self, message_dict, status=Email2RemoteUser.STATUS_SYNCED, formatting=''):
        
        if formatting:
            raise NotImplementedError
            
        result = {'message': [
            {
                'ATTRS': True,
                'type': message_dict['type'],
                'message_id': message_dict['message_id'],
                'status': status,
                'formatting': formatting
            },
            []
        ]}
        return result
        
    def _parse_dt(self, dt):
        if not dt:
            return None
        try:
            return pytz.UTC.localize(datetime.strptime(dt, '%Y-%m-%d %H:%M:%S'))
        except:
            return None
    
    def _message_dict_from_xml(self, node):
        message_dict = {}
                
        message_dict['version'] = node.getAttribute('version')
        message_dict['type'] = node.getAttribute('type')
        message_dict['message_id'] = node.getAttribute('message_id')
        if node.getAttribute('parent_message_id'):
            message_dict['parent_message_id'] = node.getAttribute('parent_message_id')
        message_dict['in_reply_to'] = node.getAttribute('in_reply_to')
        message_dict['references'] = node.getAttribute('references')
        message_dict['date_sent'] = self._parse_dt(node.getAttribute('date_sent'))
        message_dict['date_processed'] = self._parse_dt(node.getAttribute('date_processed'))
        message_dict['date_thread'] = self._parse_dt(node.getAttribute('date_thread'))
        message_dict['from_user'] = (node.getAttribute('from_user').lower() == 'true')
        
        d = node.getElementsByTagName('data')
        if d:
            message_dict['data'] = get_xml_text(d[0])
        d = node.getElementsByTagName('body')
        if d:
            message_dict['body'] = get_xml_text(d[0])            
        d = node.getElementsByTagName('extra')
        if d:
            message_dict['extra'] = json.loads(get_xml_text(d[0]))
        d = node.getElementsByTagName('from')
        if d:
            message_dict['from'] = json.loads(get_xml_text(d[0]))
        d = node.getElementsByTagName('to_all')
        if d:
            message_dict['to_all'] = json.loads(get_xml_text(d[0]))
        d = node.getElementsByTagName('subject')
        if d:
            message_dict['subject'] = get_xml_text(d[0])
                
        return message_dict
    
    
    
    def create(self, request):
        '''
        Formerly, the interface for posting messages to Calenvy via the desktop application (no longer supported).
        '''
        
        return {'success': False, 'msg': 'Syncing messages to Calenvy is no longer supported.'}
        
                
        
class InternalMessagesHandler(AnonymousBaseHandler):
        
    allowed_methods = ('GET', 'POST')
    
    def read(self, request):
        return {'success': True}
    
    
    def create(self, request):
        '''
        Interface for posting pre-processed messages to Calenvy
        When celery_worker crawls messages, it calls this callback API after every 10 messages
        (or after the messages are taking up a certain number of bytes in RAM).
        This callback API:
            - stores crawled messages to the DB
            - updates the EmailFolder objects for the given Alias, so that subsequent crawls
                will not re-crawl the same messages
            - updates the NotedTaskMeta object, if any, for the progress bar (if this is a live crawl)

        The request should contain JSON data formatted as follows:
        {
            'success':              True,
            'done':                 done,                     # a flag indicating whether the crawl is complete, if False there will be more messages
            'callback_struct': {                              # a JSON structure (opaque to celery_worker) indicating which crawl task this is
                'alias_id':            34,
                'noted_task_meta_id':  567
            },
            'message_dicts':        message_dicts,            # a list of Vijul-style message dictionaries
            'num_fetched_msgs':     num_fetched_msgs          # total number of messages fetched so far, if this callback is one in a sequence
            'folders': {
                'INBOX': {
                    'name':             'INBOX',              # name of folder
                    'highest_crawled':  ...
                    'highest_path':     '872',                # IMAP UID, or other unique indicator of the last message we crawled 
                    'highest_date':     '2006-02-23 12:34:56'
                },
                'Sent': {
                    ...
                }    
            }
        }
        
        '''
        
        # Failure codes we may get from celery_worker
        CRAWL_FAILURE_CONNECT   = 'failure_connect'
        CRAWL_FAILURE_KEYBOARD  = 'failure_keyboard'
        CRAWL_FAILURE_API       = 'failure_api'
    
        from crawl import procmail
        
        try:
            log("InternalMessagesHandler.create: start")
                        
            callback_struct = request.data['callback_struct']
                        
            try:
                noted_task_meta = NotedTaskMeta.objects.get(id=callback_struct['noted_task_meta_id'])
            except:
                noted_task_meta = None
            
            # If unsuccessful crawl, bail out early
            if not request.data['success']:
                if request.data['failure_reason'] == CRAWL_FAILURE_CONNECT:
                    msg = _('Could not connect to your inbox. Please check your mail settings.')
                else:
                    msg = _('Could not connect to your inbox.')
                if noted_task_meta:
                    noted_task_meta.mark_finished(success=False, message=msg)
                return {'success': False, 'msg': msg}
                
            alias_id = callback_struct['alias_id']              
            alias = Alias.objects.get(id=alias_id)
            
            # Update the EmailFolder objects
            folders = request.data.get('folders', [])
            for folder in folders:
                try:
                    ef, created = EmailFolder.objects.get_or_create(alias=alias, name=folder['name'])
                except MultipleObjectsReturned:
                    log('Warning: multiple email folders returned with name %s' % folder['name'])
                    EmailFolder.objects.filter(alias=alias, name=folder['name']).order_by('-id')[0]
                    log('Warning: multiple email folders returned with name %s... selected newest one!' % folder['name'])
                if folder.get('highest_crawled'):
                    ef.highest_crawled = folder['highest_crawled']
                if folder.get('highest_path'):
                    ef.highest_path = folder['highest_path']
                if folder.get('highest_date'):
                    ef.highest_date = pytz.UTC.localize(datetime.strptime(folder['highest_date'], '%Y-%m-%d %H:%M:%S'))
                ef.save()
                
            message_dicts = request.data['message_dicts']

            # Use the same CloudFilesManager object for all calls to procmail_message_dict, so that
            #  we can use the same connection for all the messages (reduce the # of HTTP calls)
            from lib.cloud import CloudFilesManager
            cloud_manager = CloudFilesManager()
            
            e = None
            for m in message_dicts:
                try:
                    e = procmail.procmail_message_dict(m, alias, cloud_manager=cloud_manager)
                except:
                    pass

            if e:                           # Update the crawl bar with the last message processed
                if request.data['num_total_msgs']:
                    percent_done = int(100 * float(request.data['num_fetched_msgs']) / request.data['num_total_msgs'])
                else:
                    percent_done = 0 
                date_sent = e.date_sent.astimezone(alias.account_user.get_timezone())
                if noted_task_meta:
                    noted_task_meta.mark_updated(custom={
                        'percent_done': percent_done,
                        'date':         '%d/%d' % (date_sent.month, date_sent.day),
                        'folder':       e.server_emailfolder and e.server_emailfolder.name or '',
                        'subject':      ellipsize(e.subject, 60)                                                              
                    })
                
            if request.data['done']:
                # End of the crawl
                log("MessagesHandler.create: received DONE flag from celery_worker")
                if noted_task_meta:
                    noted_task_meta.mark_finished(success=True, message=_('Done!'), custom={
                        'percent_done': 100,
                        'refresh_needed': request.data['num_total_msgs'] > 0
                    })
                alias.server_last_crawled = now()
                alias.save()
                    
            log("MessagesHandler.create: done")
            return {'success': True}
        except:
            log (traceback.format_exc())
            return {'success': False, 'msg': 'Could not parse messages'}
        

