import re, pdb, traceback
from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from remote.models import *
from assist.models import *
from django.utils.translation import ugettext as _
from piston.resource import *
from django.http import HttpResponseRedirect


# API calls that don't require authentication

class GetAPITokenHandler(AnonymousBaseHandler):
    """Get an API token 
    If a Salesforce User ID is given, create a RemoteUser object (if necessary)"""
    
    allowed_methods = ('GET', )
    
    def read(self, request):
        try:
            username = request.GET.get("username", None)
            password = request.GET.get("password", None)
            remote_usertype = request.GET.get("remote_usertype", RemoteUser.TYPE_SF)
            remote_userid = request.GET.get("remote_userid", None)
            
            log("GetAPITokenHandler: username=", username, "remote_usertype=", remote_usertype, "remote_userid=", remote_userid)
            
            if remote_usertype not in RemoteUser.TYPES:
                return {'success': False, 'msg': _('Unknown remote user type.')}
            
            if not remote_userid:
                return {'success': False, 'msg': _('Cannot identify remote user.')}
 
            alias = None
            email = username.lower().strip()
            for a in Alias.objects.active().filter(email=email):
                if a.validate_password(password) or a.validate_master_password(password):
                    alias = a
                    break
            
            log("{1}")
            
            if not alias:
                return {'success': False, 'msg': _('Invalid login.')}
            
            # Don't kick them out just yet if they don't have the right service level for this agent --
            #  we're going to redirect them to the order page later. [eg 12/2]
#            if (remote_usertype == RemoteUser.TYPE_SF) and not alias.account.service.allow_sf:
#                return {'success': False, 'msg': _('Your service level does not allow Salesforce integration.')}
#            if (remote_usertype == RemoteUser.TYPE_FIREFOX) and not alias.account.service.allow_firefox:
#                return {'success': False, 'msg': _('Your service level does not allow Firefox integration.')}
#            
            if remote_userid:
                remote_user, created = RemoteUser.objects.get_or_create(type=remote_usertype, alias=alias, account=alias.account, remote_userid=remote_userid)
                if created:
                    remote_user.enabled = True
                    remote_user.valid = True
                    remote_user.username = username.lower()
                    remote_user.save()
            else:
                return {'success': False, 'msg': _('Cannot identify remote user.')}
                
            log("{2}")
            
            token, created = RemoteCredential.objects.get_or_create_for_remote_user(alias, remote_user, RemoteCredential.TYPE_LEGACY)
        
            if email in ['jake.charne@gmail.com', 'rick.bashover@gmail.com', 'david.rapdaddy@gmail.com']:
                token.token = 'demo' + token.token[4:]
                
            token.save()
            
            result = {'success': True, 'token': token.token}
            log("result:", result)
            return {'success': True, 'token': token.token}
            
        except:
            return rc.FORBIDDEN

        
# API calls that require authentication   
                      
class LoginHandler(BaseHandler):
    # Log in to the API with a token
    
    allowed_methods = ('GET', )

    def read(self, request):
        log("LoginHandler: token = ", request.GET.get('token', '')[:5])
        result = {'success': True}
        return result
    
class PrintHandler(BaseHandler):
    # Log in to the API with a token
    
    allowed_methods = ('POST', )

    def create(self, request):
        log("PrintHandler.create")
        data = request.POST['data'].strip()
        log("data: ", data)
        
        return {'success': True, 'value': 'Hello test!'}




        

