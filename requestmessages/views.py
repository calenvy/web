# -*- coding:utf-8 -*-
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

def redirect(request):
    request.messages['notices'].append("Redirected message")
    return HttpResponseRedirect("/usermessages/perfectview/")

def perfectview(request):
    request.messages['warnings'].append("This view message")
    return render_to_response('perfectview.html', context_instance=RequestContext(request))
