### just run pip install -r requirements.txt

# Quick environment setup for Ubunutu! 
# Windows - you can run this from Cygwin c://cygwin//bin//bash.exe setup_env.sh

# sudo apt-get update
# sudo aptitude -f -y safe-upgrade

sudo apt-get -f -y install python-git
sudo apt-get -f -y install python-setuptools
pip install virtualenv
sudo apt-get -f -y install libpq-dev
sudo apt-get -f -y install memcached
sudo apt-get -f -y install python-memcache
sudo apt-get -f -y install python-dev

# Uncomment below to intall Django if not installed already
# pip install -U http://www.djangoproject.com/download/1.1.2/tarball/

sudo apt-get -f -y install postgresql
sudo apt-get -f -y install pgadmin3
sudo apt-get -f -y install python-psycopg2

pip install -U pytz
pip install http://keyczar.googlecode.com/svn/trunk/python

sudo apt-get -f -y install python-pyasn1

pip install -U celery
pip install -U South
pip install -U django-tagging
pip install -U django-piston

pip install -e git://github.com/nathanborror/django-basic-apps#egg=django-basic-apps

pip install -U sorl-thumbnail
pip install -U django-filebrowser

pip install -U beautifulsoup

pip install -U icalendar

pip install -U jsmin

# useful to determine which packages you have installed
# yolk -l lists installed packages
pip install yolk

pip install -U gdata

pip install -U authorize

pip install -U dnspython

echo "\nInstalling python modules for desktoppy"

pip install -U sqlobject
pip install -U cherrypy
pip install -U jinja2
pip install -U pyDes

echo "\nSetup complete! - Make sure you've created your Postgresql Database"

# sudo python recreate.py -f


