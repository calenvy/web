from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.contrib import admin
import hashlib
import re, string, os
import json
from utils import *
# from datetime import *
import datetime
from assist.constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from assist.models import *
from schedule.models import *
import pdb, traceback
import urllib, urllib2, sys, twitter_api, oauthtwitter
from oauth import oauth

TWITTER_BASE_URL = 'http://twitter.com'
TWITTER_INFO_URL = 'http://twitter.com/users/show.json'
TWITTER_FRIENDS_URL = 'http://twitter.com/statuses/friends.json'
TWITTER_FOLLOWERS_URL = 'http://twitter.com/statuses/followers.json'
TWITTER_FRIENDSHIP_CREATE_URL = 'http://twitter.com/friendships/create'
TWITTER_DM_GET_URL = 'http://twitter.com/direct_messages.json'
TWITTER_DM_SEND_URL = 'http://twitter.com/direct_messages/new.json'
TWITTER_TWEET_GET_URL = 'http://twitter.com/statuses/user_timeline.json'

class TwitterUserManager(Manager):
    
    def get_keys(self, site):
        domain = site.domain.replace(':3636', '').lower()
        return settings.TWITTER_KEYS[domain]
        
    def get_auth_url(self, site):
        consumer_key, consumer_secret = self.get_keys(site)
        api = oauthtwitter.OAuthApi(consumer_key, consumer_secret) 
        request_token = api.getRequestToken() 
        auth_url = api.getAuthorizationURL(request_token) 
        return request_token, auth_url
    
    def get_or_create_by_request_token(self, request_token, site):    
        consumer_key, consumer_secret = self.get_keys(site)
        api = oauthtwitter.OAuthApi(consumer_key, consumer_secret, request_token) 
        access_token = api.getAccessToken()
        api = oauthtwitter.OAuthApi(consumer_key, consumer_secret, access_token) 
        apiuser = api.GetUserInfo() 
        
        d = apiuser.AsDict()
        
        tu, created = TwitterUser.objects.get_or_create(user_id=d['id'])
        tu.is_complete = True
        tu.slug = create_slug(length=10)
        tu.followers_count = d.get('followers_count', 0)
        tu.protected = d.get('protected', False)
        tu.screen_name = d['screen_name']
        tu.friends_count = d.get('friends_count', 0)
        tu.name = d.get('name')
        tu.access_token = access_token.to_string()
        tu.site = site
        tu.save()
        return tu, created
        
    def get_or_create_by_id(self, user_id, complete=False):
        "Retrieve the user info locally, or get it from Twitter if necessary"
        # TODO: Do we need authentication to get protected users?
        
        queryset = TwitterUser.objects.filter(user_id=user_id)            
        if queryset.count() > 0:
            tu, created = queryset[0], False
        else:
            tu, created = TwitterUser(user_id=user_id, is_complete=False, slug=create_slug(length=10)), True

        if complete and (not tu.is_complete):
            tu.populate_from_twitter()
            
            if tu.is_error:
                return None, None
            else:
                tu.save()
                return tu, created
            
        return tu, created
    
    def get_or_create_by_screen_name(self, screen_name):
        "Retrieve the info on the user who has this screen name"
        
        log("get_or_create_by_screen_name:", screen_name)
        screen_name = screen_name.strip().lower()
        
        queryset = TwitterUser.objects.filter(screen_name__iexact=screen_name)            
        if queryset.count() > 0:
            tu, created = queryset[0], False
            return tu, created
              
        tu, created = TwitterUser(screen_name=screen_name, is_complete=False, slug=create_slug(length=10)), True
        tu.populate_from_twitter()
                
        # User may have changed their screen name, in which case there may be another user with the same ID
        if tu.is_error and tu.user_id:
            queryset = TwitterUser.objects.filter(user_id=tu.user_id)
            if queryset.count() > 0:
                tu, created = queryset[0], False
                tu.is_complete = False
                tu.populate_from_twitter()      # Refresh the old user with the new name
        
        if tu.is_error:
            return None, None
            
        tu.save()
        return tu, created
                    

    def link_alias_to_twitteruser(self, request, alias, tu):
        
        def network_phrase(alias):
            if alias.account.name:
                return _('on the %s network ' % alias.account.name) # ending space is important here
            else:
                return _('')
            
        "Helper function, link a given Twitter user to an alias"
        
        log("login: linking alias to Twitter account", tu.screen_name)
        # Always set the alias, even if the service doesn't allow it (incase they change service level later)
        
        if not alias.account.service.allow_twitter:
            request.session['notices'] = [_('Twitter functionality has not been enabled because your service level does not include it.')]
            return
        
        # Unlink any other TwitterUsers from this alias
        for other_tu in TwitterUser.objects.filter(alias=alias):
            other_tu.alias = None
            other_tu.save()
        
        old_alias = tu.alias
        tu.alias = alias
        tu.account = None           # Unlink company from this Twitter account, if any
        tu.save()
        
        contact = alias.contact
        contact.twitter_screen_name = tu.screen_name
        contact.save()
        
#        if old_alias:
#            old_alias.events_enabled = False
#            old_alias.save()
#        del request.session['tolink_twitteruser']
        # This MUST use request.session['notices'] rather than request.messages['notices'], since the latter is cleared before
        #  we get to the newsfeed feed.
        request.session['notices'] = [_('Your email address %s %shas been linked to Twitter name %s.') % (alias.email, network_phrase(alias), tu.screen_name)]
    
    def link_account_to_twitteruser(self, request, account, tu):

        log("login: linking account to Twitter account", tu.screen_name)
        
        if not account.service.allow_twitter:
            request.session['notices'] = [_('Twitter functionality has not been enabled because your service level does not include it.')]
            return
        
        # Unlink any other TwitterUsers from this account
        for other_tu in TwitterUser.objects.filter(account=account):
            other_tu.account = None
            other_tu.save()
        
        tu.account = account
        tu.alias = None           # Unlink individual from this Twitter account, if any
        tu.save()
#        if old_alias:
#            old_alias.events_enabled = False
#            old_alias.save()
#        del request.session['tolink_twitteruser']
        # This MUST use request.session['notices'] rather than request.messages['notices'], since the latter is cleared before
        #  we get to the newsfeed feed.
        if account.name:
            request.session['notices'] = [_('The account %s has been linked to Twitter name %s.') % (account.name, tu.screen_name)]
        else:
            request.session['notices'] = [_('Your account has been linked to Twitter name %s.') % tu.screen_name]

            
    def get_crawlable(self):
        '''
        Which accounts to crawl every 15 minutes
        (a list, not a QuerySet)
        '''
        account_crawlable = list(self.filter(account__status=ACCOUNT_STATUS_ACTIVE))
        
        alias_crawlable = list(\
            self.filter(alias__status=ALIAS_STATUS_ACTIVE, 
                        alias__is_admin=True,
                        alias__account__status=ACCOUNT_STATUS_ACTIVE))
        
        accounts = [ac.account for ac in account_crawlable]
        alias_crawlable = [a for a in alias_crawlable if a.alias.account not in accounts]
        
        return uniqify(account_crawlable + alias_crawlable)
        

class TwitterUser(Model):
    # Noted-specific fields
    is_noted =          models.NullBooleanField(null=True, blank=True)      # Is this one of our accounts?
    site =              models.ForeignKey(Site, null=True, blank=True)  # For is_noted TwitterUsers, which Site this is the main Twitter account for
                                                                        # For others, which site they used when setting up this TwitterUser
    is_complete =       models.NullBooleanField(default=False)      # Many instances will contain only the user ID
    is_error =          models.NullBooleanField(default=False)      # Did the last attempt to get info result in error?
    last_updated =      UTCDateTimeField(default=now)
    slug =              models.SlugField(null=True,blank=True, unique=True)
    alias =             models.ForeignKey('assist.Alias', null=True, blank=True)
    account =           models.ForeignKey('assist.Account', null=True, blank=True)
    
    # Twitter fields                                                 
    user_id =           models.IntegerField(unique=True) 
    followers_count =   models.IntegerField(null=True, blank=True)
    protected =         models.NullBooleanField(null=True, blank=True)
    location =          TruncCharField(max_length=128, null=True, blank=True)
    utc_offset =        models.IntegerField(null=True, blank=True)
    statuses_count =    models.IntegerField(null=True, blank=True)
    description =       TruncCharField(max_length=160, null=True, blank=True)
    friends_count =     models.IntegerField(null=True, blank=True)
    profile_image_url = models.URLField(null=True, blank=True)
    name =              TruncCharField(max_length=64, null=True, blank=True)
    favourites_count =  models.IntegerField(null=True, blank=True)
    screen_name =       TruncCharField(max_length=64, null=True, blank=True)
    url =               models.URLField(null=True, blank=True)
    created_at =        TruncCharField(max_length=64, null=True, blank=True)
    time_zone =         TruncCharField(max_length=64, null=True, blank=True)
    
    # Twitter authentication
    password_aes =      models.CharField(max_length=255, null=True, blank=True)
    access_token =      models.CharField(max_length=255, null=True, blank=True)
    
    # Relationships
    friends =           models.ManyToManyField('self', symmetrical=False, related_name='followers', blank=True, null=True)
    friends_last_updated = UTCDateTimeField(null=True, blank=True)
    followers_last_updated = UTCDateTimeField(null=True, blank=True)
    
    # Messages
    highest_message_id = models.IntegerField(null=True, blank=True)         # Highest Direct Message to this user that we've processed so far
    
    # Promos
    promo =            models.ForeignKey('assist.Promo', null=True, blank=True)

    objects = TwitterUserManager()
    
    def __unicode__(self):
        return u'%d: %s' % (self.user_id, self.screen_name)
 
    def get_api(self):        
        if self.access_token:
            access_token = oauth.OAuthToken.from_string(self.access_token)
            consumer_key, consumer_secret = TwitterUser.objects.get_keys(self.site)
            api = oauthtwitter.OAuthApi(consumer_key, consumer_secret, access_token) 
            return api
        elif self.screen_name and self.password_aes:
            api = twitter_api.Api(username=self.screen_name, password=CRYPTER.Decrypt(self.password_aes))
            return api
        raise Exception("TwitterUser.get_api: no login credentials")
    
    def get_name_or_screen_name(self):
        return self.name if self.name else self.screen_name
    
    
    def populate_from_json(self, s):
        if type(s) is str:             
            tu_info = json.loads(s)
        elif type(s) is dict:
            tu_info = s
        else:
            raise Exception("populate_from_json: unknown type for s", s)
        
        if ('error' in tu_info) or ('id' not in tu_info):
            self.is_error = True
        else:
            try:                
                self.user_id = tu_info['id']
                self.followers_count = tu_info.get('followers_count', None)
                self.protected = tu_info.get('protected', None)
                self.location = tu_info.get('location', None)
                self.utc_offset = tu_info.get('utc_offset', None)
                self.statuses_count = tu_info.get('statuses_count', None)
                self.description = tu_info.get('description', None)
                self.friends_count = tu_info.get('friends_count', None)
                profile_image_url = tu_info.get('profile_image_url', None)
                if profile_image_url and len(profile_image_url) <= self._meta.get_field_by_name('profile_image_url')[0].max_length:
                    self.profile_image_url = profile_image_url
                self.name = tu_info.get('name', None)
                self.favourites_count = tu_info.get('favourites_count', None)
                self.screen_name = tu_info.get('screen_name', None)
                url = tu_info.get('url', None)
                if url and len(url) <= self._meta.get_field_by_name('url')[0].max_length:
                    self.url = url                               
                self.created_at = tu_info.get('created_at', None)
                self.time_zone = tu_info.get('time_zone', None)
                self.is_noted = self.screen_name in settings.TWITTER_NOTED_ACCOUNTS
                self.is_complete = True
                self.is_error = False
                self.last_updated = now()
            except:
                self.is_error = True
 
 
    def populate_from_twitter(self):
#                
        if self.is_complete:
            return True
        
        if self.user_id is not None:
            params = urllib.urlencode([('user_id', str(self.user_id))])
        elif self.screen_name is not None:
            params = urllib.urlencode([('screen_name', str(self.screen_name))])
        else:
            raise Exception("complete_from_twitter requires user_id or screen_name")
        
        try:
            data = urllib2.urlopen(TWITTER_INFO_URL+'?'+params)
        except:
            self.is_error = True
            return False

        try:
            self.populate_from_json(data.read())
            if not self.is_error:
                self.is_complete = True
        except:
            pass
    
        data.close()
        return True


# {"request":"\/statuses\/friends.json","error":"This method requires authentication."}[~/Snipd/twitter] curl http://twitter.com/friends/ids.json
# {"request":"\/friends\/ids.json","error":"This method requires authentication."}[~/Snipd/twitter] curl http://twitter.com/friends/ids.json?id=ccnoted
# [14777759,13330522,28297250]

    def get_followers_from_twitter(self):   
        "Returns a list of dictionaries, or None"
        try:
            opener = self.get_auth_opener()
            data = opener.open(TWITTER_FOLLOWERS_URL)
            s = data.read()
            data.close()
            followers = json.loads(s)
            if type(followers) is not list:
                return None
            return followers
        except:
            return None

    def update_followers_from_twitter(self):
        
        # TODO: Use pagination if there's too many new followers

        followers = self.get_followers_from_twitter()
        if followers is not None:
            modified = False
            new_followers = []
            follower_ids = [f['id'] for f in followers]
            old_follower_ids = [f.user_id for f in self.followers.all()]
            # Follower list is sorted in reverse order of when they followed you
            for f in followers:
                if f['id'] not in old_follower_ids:
                    log("New follower id:", f['id'], f['screen_name'])
                    new_tu, created = TwitterUser.objects.get_or_create_by_id(f['id'], complete=False)
                    new_tu.populate_from_json(f)
                    new_tu.save()
                    self.followers.add(new_tu)
                    new_followers.append(new_tu)
                    modified = True
                    
            if modified:
                self.followers_last_updated = now()
                self.save()
                
            return new_followers
        else:
            return None
        
    def befriend(self, user):
        '''
        Follow the given user
        Returns:
            already_befriended = user was already a friend
            (exception) something went horribly, horribly wrong!
        '''
                        
        already_befriended = False
           
        if type(user) is TwitterUser:
            user_id = user.user_id
            if self.friends.filter(user_id=user_id).count() > 0:
                return True                     # already a friend in the DB (and hence on Twitter)
            friend, created = TwitterUser.objects.get_or_create_by_id(user_id)
        elif type(user) is int:
            user_id = user
            if self.friends.filter(user_id=user_id).count() > 0:
                return True                     # already a friend in the DB (and hence on Twitter)
            friend, created = TwitterUser.objects.get_or_create_by_id(user)
        else:
            user_id = user
            if self.friends.filter(screen_name__iexact=user).count() > 0:
                return True                     # already a friend in the DB (and hence on Twitter)
            friend, created = TwitterUser.objects.get_or_create_by_screen_name(user)            
            if friend is None:
                raise Exception("Cannot befriend this user.")
        try:
            api = self.get_api()
            result = api.CreateFriendship(str(user_id))
            
        except twitter_api.TwitterError, e:
            if 'is already' in e.message:
                already_befriended = True
            else:
                raise

        self.friends.add(friend)
        self.friends_last_updated = now()
        self.save()
        
        return already_befriended
    
    # TODO: Convert this to twitter_api
    def send_dm(self, user, text):
        "Send a direct message to the given user"
        try:         
            log("send_dm: user=", user, "text=", text)
               
            if type(user) is TwitterUser:
                user_id = user.user_id
            elif type(user) is int:
                user_id = user
            elif type(user) is str:
                user_id = user
                        
            opener = self.get_auth_opener()
            
            params = urllib.urlencode([('user', str(user_id)), ('text', text[:140])])
            data = opener.open(TWITTER_DM_SEND_URL, params)
            s = data.read()
            data.close()
            
            tu_info = json.loads(s)
            if 'error' in tu_info:
                return False
            
            log("... send_dm success")
            return True
            
        except:
            return False
    
    # TODO: Convert this to twitter_api
    def get_latest_dms(self):
        "Get the latest direct messages sent to this user"
        try:         
            log("get_latest_dms")
                        
            opener = self.get_auth_opener()
            
            params = [('count', 200)]
            if self.highest_message_id:
                params.append(('since_id', self.highest_message_id))
            params = urllib.urlencode(params)
            data = opener.open(TWITTER_DM_GET_URL+'?'+params)   # GET request
            s = data.read()
            data.close()
            
            tu_info = json.loads(s)
            if 'error' in tu_info:
                return False
            
            log("... get_latest_dms success")
            
            if (type(tu_info) is list) and tu_info:
                self.highest_message_id=max([msg['id'] for msg in tu_info])
                self.save()
            
            return tu_info
            
        except:
            log(traceback.format_exc())
            return None
    
    # TODO: Convert this to twitter_api
    def get_latest_tweets(self, auth_user=None):
        "Get the latest direct messages sent to this user"
        try:         
            log("get_latest_tweets")
            
            if auth_user:
                opener = auth_user.get_auth_opener()
            else:
                opener = self.get_auth_opener()
                
            params = [('count', 200), ('user_id', self.user_id)]
            params = urllib.urlencode(params)
            data = opener.open(TWITTER_TWEET_GET_URL+'?'+params)   # GET request
            s = data.read()
            data.close()
            
            tu_info = json.loads(s)
            if 'error' in tu_info:
                return False
            
            log("... get_latest_tweets success")    
            return tu_info
            
        except:
            log(traceback.format_exc())
            return None
        

           
    
admin.site.register(TwitterUser)