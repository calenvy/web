from django.http import HttpResponse
from django.template import Context, loader, Template
from django.shortcuts import render_to_response
from django.core.cache import cache
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.db.models.query import QuerySet
from django.conf import settings
from django.core import serializers
from lib.csrf.middleware import csrf_exempt, csrf_required

import time
from utils import *  # do we need the entire utils file impoorted?

from assist.models import *  # do we need every single model here?
from assist.constants import *
from assist import mail
from assist import constants
from assist.parsedate.tokens import Span

from crawl.models import *   # do we need every crawl model?
from twitter.models import * # do we need every twitter model?
from schedule.periods import *

import traceback, pdb, pprint

# for debugging only
def print_stale(request):
    rr = get_widget_manager(request)
    for r in rr.widgets:
        log(r, r.stale)

def clear_stale(request):
    rr = get_widget_manager(request)
    for r in rr.widgets:
        r.stale = False
        log(r, r.stale)


def get_widget_manager(request):
    # Get or create the correct widget manager for this agent
    # (This way we keep salesforce, outlook, firefox separate from the main browser)
    if 'widget_managers' not in request.session:                # Initialize the dict of widget managers if necessary
        request.session['widget_managers'] = {}
    agent = get_agent(request)                                  # a string like 'iphone', or None for default
    if agent not in request.session['widget_managers']:
        request.session['widget_managers'][agent] = WidgetManager(request)
    return request.session['widget_managers'][agent]

def get_search_display(request):
    widget_manager = get_widget_manager(request)
    alias = request.session['alias']
    search_display = widget_manager.search and widget_manager.search.get_search_display(alias) or ''
    return search_display

def get_bucket_display(request):
    widget_manager = get_widget_manager(request)
    alias = request.session['alias']
    bucket_display = widget_manager.bucket and widget_manager.bucket.name or ''
    return bucket_display

def format_buckets_for_display(alias, bucket, intro_text=None):

    buckets, display_buckets = Bucket.objects.filter(account=alias.account).order_by('name'), []

    if intro_text is not None:
        display_buckets.append({\
            'slug':                 '',
            'text':                 intro_text,
            'selected':             bucket is None
        })

    for b in buckets:
        display_buckets.append({\
            'slug':             b.slug,
            'text':             b.name,
            'selected':         (b == bucket)
        })
    return display_buckets


DEBUG_LEVEL_NONE        = 0
DEBUG_LEVEL_PRINT       = 1
DEBUG_LEVEL_PDB         = 2



def render_email_for_update(request, email, widget_id):
    alias = request.session['alias']
    user_tz = alias.account_user.get_timezone()
    newsfeed_groups, any_from_this_account = format_emails_for_display(alias, [email], user_tz=user_tz, mode={'widget_id': widget_id})

    if newsfeed_groups:
        t = loader.get_template('newsfeed_group.html')
        c = get_widget_manager(request).retrieve_by_id(widget_id).context(request)
        c.update({
            #'alias': alias,
            'group': newsfeed_groups[0],
            'newsfeed_allow_reply': True,
            'root': email.original_parent is None,
            'widget_id': widget_id
        })
        c.update(global_context(request))
        return t.render(c)
    else:
        return None


class WMessage(object):
    def __init__(self, message=None):
        self.message = message

class WMUpdateAll(WMessage):
    def __init__(self, message=None):
        self.message = message

class WMNewEmails(WMessage):
    def __init__(self, new_emails, message=None):
        "new_emails: the emails that just got added to the newsfeed"
        self.message = message
        self.new_emails = new_emails

class WMSearch(WMessage):
    def __init__(self, search_query=None, search_type=None):
        self.search_query = search_query
        self.search_type = search_type or SEARCH_TYPE_TEXT
        self.message = None

    def __nonzero__(self):
        # Return True if the search "does" anything -- that is, it returns anything other than the full list of
        # objects.
        # Careful: This means that SEARCH_TYPE_NORESULTS actually should return True!
        try:
            return (self.search_type in [SEARCH_TYPE_NORESULTS, SEARCH_TYPE_NEWCONTACT]) or bool(self.search_query.strip())
        except:
            return bool(self.search_query)

    def get_search_display(self, viewing_alias):
        if not self.search_query:
            return ''
        elif self.search_type == SEARCH_TYPE_CONTACT:
            return str_or_none(self.search_query.get_display_name())
        elif self.search_type in [SEARCH_TYPE_TEXT, SEARCH_TYPE_COMPANY]:
            return self.search_query.strip()
        else:
            return ''

    def get_search_query_as_string(self):
        if not self.search_query:
            return ''
        elif self.search_type == SEARCH_TYPE_CONTACT:
            return self.search_query.id
        elif self.search_type in [SEARCH_TYPE_TEXT, SEARCH_TYPE_COMPANY]:
            return self.search_query.strip()
        else:
            return ''

    def __unicode__(self):
        return self.search_query

class WMUpdateNewsfeeds(WMessage):
    pass

class WMUpdateScheduledEvents(WMessage):
    pass

class WMUpdateFiles(WMessage):
    pass

class WMUpdateContacts(WMessage):
    pass

class WMUpdateContactSocialData(WMessage):
    def __init__(self, contact_id, social_data):
        self.contact_id = contact_id
        self.social_data = social_data

class WMUpdateEvents(WMessage):
    pass

class WMChangeBucket(WMessage):
    def __init__(self, bucket=None):
        self.bucket = bucket
        self.message = None

    def __nonzero__(self):
        return bool(self.bucket)

class WMEditBuckets(WMessage):
    # Possible values for cmd: "add", "remove"
    def __init__(self, cmd, bucket):
        self.cmd = cmd
        self.bucket = bucket
        self.message = None

class WMChangeFollowupOnly(WMessage):
    def __init__(self, followup_only=False):
        self.followup_only = followup_only
        self.message = None

class WMGoBack(WMessage):
    pass

class WMBanner(WMessage):
    """Replace the banner on a widget.
    For now, only support one kind: show how many emails have been synced from Salesforce."""
    def __init__(self, num_emails_synced=None, temporary=False):
        if num_emails_synced is None:
            self.banner = None
        else:
            self.banner = {
                'message': '%d message%s synced' % (num_emails_synced, '' if num_emails_synced==1 else 's'),
                'icon': 'sf',
                'temporary': temporary
            }

WIDGET_NAME_CONVERSATIONS =     'conversations'
WIDGET_NAME_DISCOVEREDMEETINGS ='discoveredmeetings'
WIDGET_NAME_CONTACTS =          'contacts'
WIDGET_NAME_SEARCH =            'search'
WIDGET_NAME_COMINGUP =          'comingup'

# When we return widgets to the Javascript (in AJAX calls), these actions tell the JS what to do with them
WIDGET_ACTION_REPLACE_OR_APPEND = 'replace_or_append_widget'              # Replace entire widget, appending it to its container if it doesn't exist (default)
WIDGET_ACTION_REPLACE_OR_PREPEND = 'replace_or_prepend_widget'            # Replace entire widget, prepending it to its container if it doesn't exist
WIDGET_ACTION_APPEND_WITHIN =   'append_within_'                          # action = this + id of element to append the html to
WIDGET_ACTION_PREPEND_WITHIN =  'prepend_within_'                         # action = this + id of element to prepend the html to
WIDGET_ACTION_NEW_CONVERSATIONS = 'new_conversations_'                    # Show the new emails in the special "new conversations" box
WIDGET_ACTION_NONE = 'none'                             # Do nothing


class WidgetManager(object):

    def __init__(self, request):
        self.init(request)

    def refresh_manager(self):
        self.team_aliases = Alias.objects.active().filter(account=self.alias.account).order_by('ez_name')
        for w in self.widgets:
            w.clear_cache()
        # self.retrieve_by_id('contacts').refresh_

    def init(self, request):
        self.alias = request.session['alias']
        self.account = self.alias.account
        self.agent = get_agent(request)
        self.followup_only = False
        self.search = None                          # global search (type WMSearch)
        self.bucket = None                          # global bucket select (type Bucket)
        self.widgets_remaining = []                 # used only by retrieve_one_stale
        self.disable_widgets = request.session.get('disable_widgets', [])   # for profiling, don't create these widgets
        self.summary_enabled = request.session.get('summary_enabled', True) # for profiling, disable the summary generation
        self.widgets = []

        if self.agent == 'outlook':
            # if WIDGET_NAME_COMINGUP not in self.disable_widgets:        self.widgets.append(WidgetComingUp(self, 2))
            #if WIDGET_NAME_CONTACTS not in self.disable_widgets:        self.widgets.append(WidgetContacts(self, 3))
            if WIDGET_NAME_CONVERSATIONS not in self.disable_widgets:   self.widgets.append(WidgetConversations(self, 5))
        elif self.agent == 'iphone':
            if WIDGET_NAME_COMINGUP not in self.disable_widgets:        self.widgets.append(WidgetComingUp(self, 2))
            if WIDGET_NAME_CONTACTS not in self.disable_widgets:        self.widgets.append(WidgetContacts(self, 3))
            if WIDGET_NAME_CONVERSATIONS not in self.disable_widgets:   self.widgets.append(WidgetConversations(self, 5))
        elif self.agent == 'firefox':
            #if WIDGET_NAME_CONTACTS not in self.disable_widgets:        self.widgets.append(WidgetContacts(self, 3))
            if WIDGET_NAME_CONVERSATIONS not in self.disable_widgets:   self.widgets.append(WidgetConversations(self, 5))
        elif self.agent == 'salesforce':
            #if WIDGET_NAME_CONTACTS not in self.disable_widgets:        self.widgets.append(WidgetContacts(self, 1))
            if WIDGET_NAME_CONVERSATIONS not in self.disable_widgets:   self.widgets.append(WidgetConversations(self, 2))
            if WIDGET_NAME_COMINGUP not in self.disable_widgets:        self.widgets.append(WidgetComingUp(self, 3, position='right'))

        elif self.agent == 'windowsmob':
            #if WIDGET_NAME_CONTACTS not in self.disable_widgets:        self.widgets.append(WidgetContacts(self, 1))
            if WIDGET_NAME_COMINGUP not in self.disable_widgets:        self.widgets.append(WidgetComingUp(self, 3))
            if WIDGET_NAME_CONVERSATIONS not in self.disable_widgets:   self.widgets.append(WidgetConversations(self, 5))
        else:
            if WIDGET_NAME_CONVERSATIONS not in self.disable_widgets:   self.widgets.append(WidgetConversations(self, 6))
            if WIDGET_NAME_CONTACTS not in self.disable_widgets:        self.widgets.append(WidgetContacts(self, 1, position='left'))
            if WIDGET_NAME_COMINGUP not in self.disable_widgets:        self.widgets.append(WidgetComingUp(self, 2))

        self.refresh_manager()

            # if sys.platform == 'darwin':
            # self.widgets.append(WidgetKnowledge(self, 9))

    def retrieve_by_ids(self, ids):
        result = []
        for wid in ids:
            ww = [w for w in self.widgets if w.widget_id == wid]
            if len(ww) == 1:
                result.extend(ww)
            else:
                raise Exception("get_widgets: %d occurrences of widget_id %d" % (len(ww), wid))
        return result

    def retrieve_by_id(self, id):
        try:
            id = int(id)
        except:
            pass
        if type(id) is int:
            return self.retrieve_by_ids([id])[0]
        for w in self.widgets:
            if w.name.lower() == id.lower():
                return w
        return None

    def data_list(self, l, type):
        if type == 'json':
            return list(l)
        elif type == 'xml':
            return '<widgets>' + '\n'.join(l) + '</widgets>'

    def retrieve_updated(self, request, type, include_widgets=True, extra_data=None, action=WIDGET_ACTION_REPLACE_OR_APPEND):
        log("retrieve_updated start...")
        timings = []
        result = []
        if include_widgets:
            for w in self.widgets:
                if w.stale:
                    log(" - appending widget: ", w)
                    start_time = now()
                    data = w.as_data(request, type, action=action)
                    total_time = now() - start_time
                    timings.append("retrieve_updated_json: %s %s" % (w.name, str(total_time)))
                    result.append(data)
        for t in timings:
            log(t)
        result = self.data_list(result, type)
        if extra_data:
            if type == 'json':
                raise NotImplementedError("retrieve_updated: no support for json + extra_data")
            elif type == 'xml':
                result += '<extradata>' + encode_cdata(json.dumps(extra_data)) + '</extradata>'
        if type == 'xml':
            result = '<result>%s</result>' % result
        return result

    def clear_cache_all(self):
        for w in self.widgets:
            w.stale = True
            w.clear_cache()

    # Rarely call this!
    def force_refresh_all(self, request, type):
        self.clear_cache_all()
        return self.retrieve_updated(request, type)

    def go_back(self, request):
        for w in self.widgets:
            w.go_back(request)
        if not any([w.search for w in self.widgets]):
            self.search = None
        if not any([w.bucket for w in self.widgets]):
            self.bucket = None

        # Clear the search and send an empty WMSearch to all the widgets
        #  (so that, e.g., the Contacts box will collapse).
        #  This needs to be done last so that w.go_back will work properly!
#        old_info = [(w.page, w.bucket) for w in self.widgets]      # Preserve which page we're on now, and which bucket we're on
#        self.change_search(None, None, request)
#        for w, old_info in zip(self.widgets, old_info):
#            w.page = old_info[0]
#            w.bucket = old_info[1]
#            w.stale = True
#
    def retrieve_all(self, request, type):
        log("*** WidgetManager.retrieve_all START...")
        result = self.data_list([w.as_data(request, type) for w in self.widgets], type)
        log("*** WidgetManager.retrieve_all END")
        return result

    def retrieve_one_stale(self, request, first, type):
        """Call this function several times to retrieve all the widgets, but with only
        one stale (i.e., expensive-to-compute) widget returned each time"""

        log("retrieve_one_stale: ", first)

        if first:
            self.widgets_remaining = self.widgets[:]   # shallow copy

        widgets_to_return = []

        while self.widgets_remaining:
            w = self.widgets_remaining.pop(0)
            widgets_to_return.append(w)
            if w.stale:
                break

        more = any([w.stale for w in self.widgets_remaining])
        if not more:        # no more stale widgets, just add all the remaining ones
            widgets_to_return.extend(self.widgets_remaining)
            self.widgets_remaining = []

        data_list = self.data_list([w.as_data(request, type, action=WIDGET_ACTION_REPLACE_OR_APPEND) for w in widgets_to_return], type)
        return data_list, more

    def retrieve_selected(self, ids, request, type):
        ww = self.retrieve_by_ids(ids)
        return self.data_list([w.as_data(request, type) for w in ww], type)

    def force_refresh_selected(self, id, request, type):
        w = self.retrieve_by_id(id)
        w.clear_cache()
        w.stale = True
        return self.data_list([w.as_data(request, type)], type)

    def toggle_size_by_id(self, id, request):
        w = self.retrieve_by_id(id)
        w.toggle_size()

    def change_page_by_id(self, id, page_num, request):
        w = self.retrieve_by_id(id)
        w.change_page(page_num)

    def change_search(self, search_query, search_type, request, widget_ids=None):

        alias = request.session['alias']
        if search_type == SEARCH_TYPE_CONTACT:
            try:
                # Make sure the contact is one they can view.
                # (For Salesforce, we allow them to click on not-responded contacts as well)
                search_query = Contact.objects.viewable(alias, include_notresponded=True).get(pk=search_query, owner_account=alias.account)
            except:
                self.notify_all(WMUpdateAll(), request)
                raise NotifyUserException(_('This contact no longer exists.'))
        elif search_type in [SEARCH_TYPE_TEXT, SEARCH_TYPE_COMPANY]:
            search_query = search_query.strip()     # the literal search text
        elif search_type == SEARCH_TYPE_NEWCONTACT:
            pass                                    # search_query is a dictionary
        else:
            search_query = None

        search = WMSearch(search_query=search_query, search_type=search_type)

        if widget_ids in [None, 'all']:
            if (not search_query):
                self.bucket = None
                self.notify_all(WMChangeBucket(), request)
            self.search = search
            self.notify_all(self.search, request)
        else:
            try:
                self.retrieve_by_id(widget_ids).notify(search, request=request)
            except:
                pass

    def change_bucket(self, bucket_slug, request, widget_ids=None):
        self.change_search('', SEARCH_TYPE_TEXT, request, widget_ids=widget_ids)   # clear the search whenever changing buckets [eg 7/17]
        log("change_bucket: slug = ", bucket_slug)
        if bucket_slug:
            bucket = Bucket.objects.get(account=request.session['alias'].account, slug=bucket_slug)
            log("Setting bucket to %s" % bucket.name)
        else:
            bucket = None
            log("Setting bucket to None")

        if widget_ids in [None, 'all']:
            self.bucket = bucket
            self.notify_all(WMChangeBucket(bucket=bucket), request)
        else:
            try:
                self.retrieve_by_id(widget_ids).notify(WMChangeBucket(bucket=bucket), request=request)
            except:
                pass

    def change_followup_only(self, followup_only):
        self.followup_only = followup_only
        self.notify_all(WMChangeFollowupOnly(followup_only))

    def notify_all(self, wmessage_list, request=None):
        if type(wmessage_list) is not list:
            wmessage_list = [wmessage_list]
        for w in self.widgets:
            for wm in wmessage_list:
                if type(wm) in w.listening_for:
                    w.notify(wm, request=request)

    def notify_all_except(self, exclude_widget_id, wmessage_list, request=None):
        if type(wmessage_list) is not list:
            wmessage_list = [wmessage_list]
        for w in self.widgets:
            if w.widget_id != exclude_widget_id:
                for wm in wmessage_list:
                    if type(wm) in w.listening_for:
                        w.notify(wm, request=request)

    def class_from_name(self, name):
        if name == WIDGET_NAME_CONVERSATIONS:       return WidgetConversations
        if name == WIDGET_NAME_CONTACTS:            return WidgetContacts
        #if name == WIDGET_NAME_SEARCH:              return WidgetSearch
        if name == WIDGET_NAME_COMINGUP:            return WidgetComingUp

    def get_or_create_by_name(self, name):
        for w in self.widgets:
            if w.name.lower() == name.lower():
                return w                        # found, return that one
        if self.widgets:
            next_id = max([w.widget_id for w in self.widgets]) + 1
        else:
            next_id = 1
        w = (self.class_from_name(name))(self, next_id)
        self.widgets.append(w)
        return w

    def select_from_args(self, request):

        alias = request.session['alias']
        if request.GET.get('contact_email'):

            email = request.GET['contact_email'].strip().lower()
            first_name = request.GET.get('contact_first_name', '')
            last_name = request.GET.get('contact_last_name', '')
            name = request.GET.get('contact_name', '')

            log("select_from_args:", alias, ":", email, first_name, last_name, name)

            contact, created = Contact.objects.get_or_create_smart(\
                name = name, first_name = first_name, last_name = last_name,
                email = email, viewing_alias = alias, account=alias.account,
                status=Contact.STATUS_NEEDSACTIVATION)

            # Contact exists and viewer has permissions to see it... zoom into it
            self.change_search(contact.pk, SEARCH_TYPE_CONTACT, request)

            log("changed search to: ", contact.pk, contact.email, " created is", created)

        if 'num_emails_synced' in request.GET:
            self.notify_all(WMBanner(num_emails_synced=int(request.GET['num_emails_synced']), temporary=True))

class Widget(object):

    def __init__(self, manager, name, widget_id, position):
        log("Widget.__init__", name)
        self.manager = manager
        self.name = name
        self.widget_id = widget_id
        self.position = position
        self.orig_position = position
        self.reset_page()
        self.search = None
        self.bucket = None
        self.followup_only = False
        self.listening_for = [WMUpdateAll, WMSearch, WMChangeBucket, WMEditBuckets, WMGoBack]
        self.stale = True
        self.cache_key_set = set()
        self.old_page = None
        self.old_page_list = None
        self.old_search = None
        self.old_bucket = None

    def center_zoomed(self):
        return self.position == 'center' and self.orig_position != 'center'

    def compact(self):
        return (self.position != 'center') or self.manager.agent

    def context(self, request):
        alias = request.session['alias']
        agent = get_agent(request)

        if not hasattr(self, 'old_search'):
            self.old_search = None
        if not hasattr(self, 'old_bucket'):
            self.old_bucket = None

        c = Context({
            'alias':                alias,
            'team_aliases':         self.manager.team_aliases,
            'all_buckets':          Bucket.objects.filter(account=alias.account).order_by('name'),
            'agent':                self.manager.agent,
            'agent_url':            request.session.get('login_params') and request.session['login_params'].get('agent_url'),
            'constants':            constants,
            'settings':             settings,
            'time_zones':           COMMON_TIMEZONES,
            'fullscreen':           'default',
            'display_more_link':    (self.position != 'center') and not agent,
            'compact':              self.compact(),
            'center_zoomed':        self.center_zoomed(),
            'widget_id':            self.widget_id,
            'page':                 self.page,
            'old_page':             self.old_page,
            'old_page_list':        self.old_page_list,
            'old_search':           self.old_search,
            'old_bucket':           self.old_bucket,
            'page_list':            self.page_list,
            'search':               self.search,
            'bucket':               self.bucket,
            'followup_only':        self.followup_only,
            'now':                  now(),
            'summary_enabled':      self.manager.summary_enabled
        })
        c.update(global_context(request))
        return c

    def cache_key_parts(self):
        """
        Return a list of the things that uniquely determine this particular widget view --
        used by cache_key to create a unique hash for memcached
        """

        result = [
            self.manager.alias.slug,        # slug rather than id, in case Alias ID gets reused!
            self.manager.agent,
            self.compact(),
            self.center_zoomed(),
            self.widget_id,
            self.page,
            self.search.search_query if self.search else None,
            self.search.search_type if self.search else None,
            self.bucket.name if self.bucket else None,
            self.followup_only
        ]
        return result

    def cache_key(self):
        from django.utils.hashcompat import md5_constructor
        from django.utils.http import urlquote

        ck = md5_constructor(u':'.join(urlquote(unicode(v)) for v in self.cache_key_parts())).hexdigest()
        return ck

    def clear_cache(self):
        '''
        On anything that changes our data, everything in our cache is discarded
        By default, only clears rendered html from cache
        '''
        for key in self.cache_key_set:
            cache.delete(key)
        self.cache_key_set.clear()
        self.stale = True           # Javascript will need to re-get this widget

    def refresh_html(self, request):
        html = "<div><b>Hello from %s #%d</b></div>" % (self.name, self.widget_id)
        self.stale = False
        return html

    def add_to_cache(self, cache_key, html, timeout):
        self.cache_key_set.add(cache_key)
        cache.set(cache_key, html, timeout)

    def as_html(self, request):
        log("as_html: widget_id = ", self.widget_id, " page=", self.page)
        cache_key = self.cache_key()

        html = None

        if (cache_key in self.cache_key_set) and not self.stale:     # Only get cached pages that we know about
            log("Found cache_key ", cache_key)
            html = cache.get(cache_key)

        if html is None:                                            # It wasn't in the cache...
            log("Missed cache_key ", cache_key)
            html = self.refresh_html(request)

            # For Firefox/Chrome plugins, transform image URLs to absolute paths
            from embed.middleware import URLMiddleware
            html = URLMiddleware().modify_html(html, request, True)

            agent = get_agent(request)
            timeout = CACHE_TIMEOUTS.get(agent, CACHE_TIMEOUTS[None])
            self.add_to_cache(cache_key, html, timeout)

        self.stale = False
        return html

    def as_outerhtml(self, request):
        "Used only by the 'widget' template tag for non-AJAX use"
        return "<div class='widget_position_%s' id='widget_%s'>%s</div>" % \
            (self.position, self.widget_id, self.as_html(request))

    def as_data(self, request, type, action=WIDGET_ACTION_REPLACE_OR_APPEND):

        log("as_data: widget_id = ", self.widget_id)
        content = self.get_content(request, type)

        if type == 'json':
            result = {\
                'widget_id':        self.widget_id,
                'position':         self.position,
                'center_zoomed':    self.center_zoomed(),
                'name':             self.name,
                'action':           action,
                'html':             content,        # TODO: Rename this to 'content'
            }
        elif type == 'xml':
            result = "<widget id='%d' position='%s' center_zoomed='%s' name='%s' action='%s'>%s</widget>" \
                % (self.widget_id, self.position, self.center_zoomed(), self.name, action, content)
        return result

    def get_content(self, request, type):
        html = self.as_html(request)
        html = RE_REMOVEWS.sub('\n', html)

        if type == 'json':
            return html
        elif type == 'xml':
            return encode_cdata(html)

    def notify(self, wm, request=None):
        log("Notify: ", self.name, type(wm))
        if type(wm) is WMSearch:
            self.reset_page(remember_old_state=bool(wm))
            self.search = wm
        elif type(wm) is WMChangeBucket:
            self.reset_page(remember_old_state=bool(wm))
            self.bucket = wm.bucket
        elif type(wm) is WMEditBuckets:
            log("WMEditBuckets!")
            if wm.cmd == 'add':
                pass                # Don't change the current bucket to the new one, for now
                # self.bucket = wm.bucket
                # self.page = 1
            if wm.cmd == 'remove' and wm.bucket == self.bucket:
                self.bucket = None
                self.reset_page()
            self.clear_cache()
        elif type(wm) is WMGoBack:
            self.go_back(request)
        elif type(wm) is WMChangeFollowupOnly:
            self.followup_only = wm.followup_only
            self.stale = True

        # TODO: Make this smarter
        self.stale = True

    def toggle_size(self):
        if self.position != 'center':
            self.position = 'center'
        else:
            self.position = self.orig_position
        self.reset_page()
        self.stale = True

    def reset_page(self, remember_old_state=False):
        if remember_old_state:
            self.old_page = self.page
            self.old_page_list = self.page_list
            self.old_search = self.search
            self.old_bucket = self.bucket
        else:
            self.old_page = None
            self.old_page_list = [None, None]
            self.search = None
            self.bucket = None

        self.page = 1
        self.page_list = [None, None]   # Page 0 isn't used; page 1 is the null queryset

    def go_back(self, request):
        if self.old_page:
            self.page = self.old_page
            self.page_list = self.old_page_list
            self.search = self.old_search
            self.bucket = self.old_bucket
            self.old_page, self.old_page_list, self.old_search, self.old_bucket = None, None, None, None
        else:
            self.reset_page()
            self.search = None
            self.bucket = None
        self.stale = True

    def change_page(self, page_num):
        if page_num == 'all':
            self.page = page_num
        else:
            if page_num == 'next':
                self.page += 1
            elif page_num == 'prev':
                self.page -= 1
            else:
                self.page = int(page_num)
            self.page = max(1, self.page)
        self.stale = True


class WidgetConversations(Widget):

    def __init__(self, manager, widget_id, position='center'):
        super(WidgetConversations, self).__init__(manager, _('Conversations'), widget_id, position)
        self.name = WIDGET_NAME_CONVERSATIONS
        self.banner = None
        self.listening_for.extend([WMChangeFollowupOnly, WMNewEmails, WMUpdateNewsfeeds, WMBanner])

    def context(self, request):
        alias = request.session['alias']

        #messages, page_obj = self.calc_messages()
        threads, page_obj = self.calc_threads()

        c = super(WidgetConversations, self).context(request)
        c.update({\
            'threads': threads,
            #'messages': messages,
            'page_obj': page_obj,
            'refresh_allowed':          request.session['alias'].refresh_allowed(request),
            'banner':                   self.banner,
            'sf_enabled':               self.sf_enabled(), # Is SF enabled for the account?
        })

        # Banner that only displays once (i.e, Lead and Contact widgets on Salesforce)
        if self.banner and self.banner['temporary']:
            self.banner = None
            self.stale = True

        return c

    def notify(self, wm, request=None):
        super(WidgetConversations, self).notify(wm, request=request)
        if type(wm) == WMBanner:
            self.banner = wm.banner
        elif type(wm) == WMNewEmails:
            self.clear_cache()

    def cache_key_parts(self):
        """
        Return a list of the things that uniquely determine this particular widget view --
        used by cache_key to create a unique hash for memcached
        """
        ck = super(WidgetConversations, self).cache_key_parts()
        ck.append(self.banner['message'] if self.banner else '')
        return ck

    def sf_enabled(self):
        """
        Determine whether the newsfeed should only show messages that have been synced to salesforce
        True if the agent is Salesforce, or there is a Salesforce token
        """
        from remote.models import RemoteUser
        return (self.manager.agent == 'salesforce') or (RemoteUser.objects.filter(type=RemoteUser.TYPE_SF, alias=self.manager.alias).count() > 0)


    def calc_messages(self):
        """
        Return a list of the messages to appear in the newsfeed
        """

        log("WidgetConversations.calc_messages start: ", self.manager.alias)

        # Only show messages that are owned by you
        queryset = Email.objects.viewable(self.manager.alias).exclude(from_contact=None).order_by('-date_sent')
        require_distinct = False

        if self.search:
            # Showing only the messages that include a particular contact
            if self.search.search_type == SEARCH_TYPE_CONTACT:
                queryset = queryset.filter(email2contact__account=self.manager.account, contacts=self.search.search_query)
                require_distinct = True

            # For these search types, we don't show anything
            elif self.search.search_type == SEARCH_TYPE_NORESULTS:
                queryset = queryset.none()

            # For this search type, Conversations should show everything (even though none
            #  of the messages will include the new contact!) so that, if the user types a
            #  new message into the quickbox, it will show up. Awkward, but it'll work.
            elif self.search.search_type == SEARCH_TYPE_NEWCONTACT:
                pass


        if self.followup_only:
            # In "followup" mode show:
            #    1.) People you set up a followup reminder for (the "followup" button in the Contacts widget detail view)
            #    2.) Emails assigned to you that don't have a response yet
            queryset = queryset.annotate(num_children=models.Count('original_email_children'))
            queryset = queryset.filter(
                Q(owner_alias=self.manager.alias, events__status=EVENT_STATUS_FOLLOWUP_REMINDER) |
                Q(email2contact__type=Email2Contact.TYPE_ASSIGNED, email2contact__contact=self.manager.alias.contact, num_children=0)
            )
            queryset = queryset.exclude(completed=True)
            require_distinct = True

        if self.bucket:
            queryset = queryset.filter(contacts__contact_buckets=self.bucket)
            require_distinct = True

        if require_distinct:
            queryset = queryset.distinct()
        log("WidgetConversations.calc_messages [2]: ", self.manager.alias)

        max_items = NEWSFEED_MAX_ITEMS.get(self.manager.agent, NEWSFEED_MAX_ITEMS['default'])

        from lib.lazy_paginator.paginator import *

        try:
            page = LazyPaginator(queryset, max_items).page(self.page if self.page is not None else 1)
            log("WidgetConversations.calc_messages [3]: ", self.manager.alias)
        except:
            # Invalid page number, most likely... just reset the page number
            self.page = 1
            page = LazyPaginator(queryset, max_items).page(self.page)
            log("WidgetConversations.calc_messages [3a]: ", self.manager.alias)

        if require_distinct:
            return page.object_list, page
        else:
            return uniqify(list(page.object_list)), page  # If we didn't do a distinct(), we should still filter out dups "manually"

    def calc_threads(self):
        """
        Return a list of the threads to appear in the newsfeed
        """

        log("WidgetConversations.calc_threads start: ", self.manager.alias)
        messages, page = self.calc_messages()

        # Sort the messages into unique threads

        threads = []

        for m in messages:
            done = False
            root = m.find_thread_root()
            for t in threads:
                if t['first'] == root:
                    done = True
                    if m != root:
                        t['replies'].insert(0, m)
                    break
            if not done:
                if m == root:
                    threads.append({
                        'first': root,
                        'replies': []
                    })
                else:
                    threads.append({
                        'first': root,
                        'replies':  [m]
                    })

        for t in threads:
            t['last'] = t['replies'][-1] if t['replies'] else t['first']
            t['reply_info'] = t['last'].get_reply_info(self.manager.alias)

        return threads, page

    def refresh_html(self, request):
        log("WidgetConversations.refresh_html start: ", self.manager.alias)
        c = self.context(request)
        t = loader.get_template('widgets/conversations.html')
        html = t.render(c)
        log("WidgetConversations.refresh_html start: ", self.manager.alias)
        self.stale = False
        return html



class WidgetContacts(Widget):
    from django.core.paginator import EmptyPage
    def __init__(self, manager, widget_id, position='right'):
        super(WidgetContacts, self).__init__(manager, _('Contacts'), widget_id, position)
        self.contact_detail = None
        self.contact_detail_json = None         # Initially, we're not zoomed into any specific contact
        self.prev_contact_detail = None         # When you click "Save & New", store the contact you just finished editing here
        self.prev_contact_detail_json = None
        self.name = WIDGET_NAME_CONTACTS
        self.listening_for.extend([WMUpdateContacts, WMChangeFollowupOnly, WMUpdateContactSocialData])
        self.expanded = False
        self.expanded_mode = None

    def go_back(self, request):
        "When they click the Back button, go back to the old search and bucket but collapse the contacts widget"
        super(WidgetContacts, self).go_back(request)
        self.collapse(request, notify=False)

    def context(self, request):
        alias = request.session['alias']
        #allowable_modes = self._allowable_modes(request)

        # Determine whether the Contacts widget needs to do an Ajax call to get the social data
        # True only when:
        #    - we're in expanded view
        #    - we don't already have the social data for that contact
        #    - we're not in edit view of a new contact

        auto_get_social_data = self.expanded and self.contact_detail and self.contact_detail_json and \
            (self.contact_detail_json.get('social_data') is None or self.contact_detail_json.get('social_data') == '{}' or self.contact_detail_json.get('social_data') == {}) and \
            self.contact_detail.id not in [0, '0', 'new'] and \
            alias.account.service.allow_social_lookups

        c = super(WidgetContacts, self).context(request)
        c.update({\
            'assistant': alias.account.get_assistant(),
            'buckets': format_buckets_for_display(alias, None, intro_text=_('Filter by tag')),
            'display_alias_recent': False,
            'expanded': self.expanded,
            'expanded_mode': self.expanded_mode,
            'has_multi_users': alias.account.has_multi_users(),
            'auto_get_social_data': auto_get_social_data
        })

        return c

    def expand(self, id_or_slug, mode, request, email=None, name=None, notify=True):
        log("EXPAND START")

        alias = request.session['alias']

        self.expanded_mode = mode

        if mode == 'new':
            contact_new = True
            n = now()
            self.prev_contact_detail = self.contact_detail
            self.prev_contact_detail_json = self.contact_detail_json
            self.contact_detail = None
            self.contact_detail_json = {}
            self.contact_detail_json['id'] = id_or_slug
            self.contact_detail_json['edit'] = True
            self.contact_detail_json['subscribed'] = True
            if email:
                self.contact_detail_json['email'] = email
            if name:
                first, last = get_first_last_name(name, addr=email)
                name = ' '.join([first, last]).strip()
                self.contact_detail_json['name'] = name
                self.contact_detail_json['first_name'] = first
                self.contact_detail_json['last_name'] = last
                self.contact_detail_json['display_name'] = name
                self.contact_detail_json['display_name_only'] = name
        else:
            contact_new = False
            if isinstance(id_or_slug, basestring) and id_or_slug.isalpha():
                contact = Contact.objects.get(unsubscribe_slug=id_or_slug, owner_account=alias.account)
            else:
                id = int(id_or_slug)
                contact = Contact.objects.get(id=id, owner_account=alias.account)
            self.prev_contact_detail = self.contact_detail
            self.prev_contact_detail_json = self.contact_detail_json
            self.contact_detail = contact
            self.contact_detail_json = self.contact_detail.as_json_details(account=alias.account, alias=alias)
            self.contact_detail_json['id'] = contact.id
            self.contact_detail_json['edit'] = (mode == 'edit')
            if notify and not self.contact_detail_json['edit']:
                # Send a search message to all the other boxes, see what we come up with
                self.manager.change_search(str(contact.id), SEARCH_TYPE_CONTACT, request)

        self.expanded = True
        self.stale = True

        log("EXPAND END")
        return None if contact_new else contact

    def edit(self):
        self.contact_detail_json['edit'] = True
        self.stale = True

    def update(self, d, request):
        alias = request.session['alias']

        contact = self.contact_detail

        kwargs = d
        kwargs['source'] = Contact.SOURCE_USER
        kwargs['contact_to_update'] = contact
        kwargs['update'] = True
        kwargs['update_alias'] = bool(contact)
        kwargs['replace_extrainfo'] = True
        kwargs['viewing_alias'] = alias
        kwargs['update_source_user'] = True
        kwargs['status'] = Contact.STATUS_ACTIVE
        updated_contact, created = Contact.objects.get_or_create_smart(**kwargs)
        updated_contact.save()

        # Add the currently selected category to the contact
        if updated_contact:
            if self.bucket:
                # Need to pass updated_contact directly, in case the id is None (which it will be for a new contact)
                self.add_to_bucket(self.bucket, updated_contact.id, request, contact=updated_contact)
            self.contact_detail = updated_contact

        # self.calc_local_contact_list(request)
        self.expand(updated_contact.id, 'readonly', request)
        self.clear_cache()
        return updated_contact

    def collapse(self, request, notify=True):
        self.contact_detail = None
        self.contact_detail_json = None
        self.prev_contact_detail = None
        self.prev_contact_detail_json = None
        self.expanded = False
        self.expanded_mode = None
        # Clear the search in all the other boxes
        if notify:
            self.search = None
            self.manager.change_search(None, None, request)
        self.stale = True

#    def set_display_all(self, display_all):
#        # True = all contacts
#        # False = most recent 10 contacts (but only when not in search or bucket)
#        self.display_all = display_all
#        self.stale = True

    def get_contact_by_id(self, id):
        try:
            return Contact.objects.viewable(self.manager.alias, include_notresponded=True).get(id=id)
        except:
            return None

    def add_to_bucket(self, bucket, id, request, contact=None):
        alias = request.session['alias']
        if contact is None:
            contact = self.get_contact_by_id(id)
        bucket.contacts.add(contact)
        contact.status = Contact.STATUS_ACTIVE
        if bucket.name == SF_BUCKET_NAME:           # Adding a contact to the "salesforce" category sets it to sync
            contact.sf_sync_pending = True
        contact.save()

        log("add_to_bucket: clearing cache ", contact, contact.pk)
        self.clear_cache()      # TODO: Clear just the relevant cache keys

    def remove_from_bucket(self, bucket, id, request):
        alias = request.session['alias']
        contact = self.get_contact_by_id(id)        # Guaranteed to be a contact from this account
        bucket.contacts.remove(contact)
        if bucket.name == SF_BUCKET_NAME:           # Removing a contact to the "salesforce" category cancels the sync
            contact.sf_sync_pending = False
            contact.save()
        log("remove_from_bucket: clearing cache ", contact, contact.pk)
        self.clear_cache()

    def notify(self, wm, request=None):

        super(WidgetContacts, self).notify(wm, request=request)
        if (type(wm) == WMSearch) and (wm.search_type == SEARCH_TYPE_CONTACT):
            # Can only search on active contacts
            contact = Contact.objects.get(id=wm.search_query.id, owner_account=self.manager.alias.account)
            self.expand(wm.search_query.id, 'readonly', request, notify=False)
#            if contact.status == Contact.STATUS_ACTIVE:
#                self.expand(wm.search_query.id, 'readonly', request, notify=False)
#            else:
#                # For inactive/not-responded contacts, offer the user the opportunity to create
#                # a new contact, and fill in the existing email and name if we have them
#                self.expand(0, 'new', request, notify=False, email=contact.email, name=' '.join([contact.first_name, contact.last_name]).strip())
        elif type(wm) in [WMSearch, WMChangeBucket, WMGoBack]:
            self.collapse(request, notify=False)             # don't propagate this notify
        elif type(wm) is WMUpdateContacts:
            self.clear_cache()
        elif type(wm) is WMUpdateContactSocialData:
            if self.contact_detail_json and self.contact_detail_json['id'] == wm.contact_id:
                self.contact_detail_json['social_data'] = wm.social_data
                self.contact_detail.social_json = json.dumps(wm.social_data)
                self.stale = True

    def cache_key_parts(self):
        """
        Return a list of the things that uniquely determine this particular widget view --
        used by cache_key to create a unique hash for memcached
        """
        ck = super(WidgetContacts, self).cache_key_parts()
        ck.append(self.expanded)
        if self.contact_detail_json:
            ck.append(self.contact_detail_json['id'])
            ck.append(self.contact_detail_json['edit'])
            ck.append(self.contact_detail_json['subscribed'])
        return ck


    def calc_current_contacts(self, request):
        from lib.lazy_paginator.paginator import *

        alias = request.session['alias']

        # The modes are:
        #    - My recent contacts
        #    - My contacts DONE
        #    - Everyone's recent contacts
        #    - Everyone's contacts

        queryset = Contact.objects.viewable(self.manager.alias).order_by('-id')

        if self.search and self.search.search_query:
            if self.search.search_type == SEARCH_TYPE_TEXT:
                for word in self.search.search_query.replace(',', ' ').split():
                    if word[0] =='@' and len(word) > 1:
                        word = word[1:]
                    queryset = queryset.filter(\
                        Q(first_name__icontains=word) | \
                        Q(last_name__icontains=word) | \
                        Q(company__icontains=word) | \
                        #Q(website__icontains=word) | \
                        #Q(phone__icontains=word) | \
                        #Q(mobile_phone__icontains=word) | \
                        #Q(description__icontains=word) | \
                        #Q(title__icontains=word) | \
                        Q(email__icontains=word) | \
                        #Q(department__icontains=word) | \
                        #Q(extrainfo__data__icontains=word) | \
                        Q(twitter_screen_name__icontains=word) | \
                        Q(alias__ez_name__icontains=word)
                    )
            elif self.search.search_type == SEARCH_TYPE_COMPANY:
                queryset = queryset.filter(company__iexact=self.search.search_query)

            elif self.search.search_type == SEARCH_TYPE_CONTACT:
                # When we're trying to get a specific contact, don't use the currently
                # selected contact set
                queryset = Contact.objects.viewable(alias).filter(id=self.search.search_query.id)

            elif self.search.search_type in [SEARCH_TYPE_NORESULTS, SEARCH_TYPE_NEWCONTACT]:
                queryset = queryset.none()

        if self.bucket:
            queryset = queryset.filter(contact_buckets=self.bucket)

        if self.followup_only:
            # Filter for contacts that you have a (pending) follow up event with

            # Faster to just find all the follow up events, then filter for contacts in this list
            # (typically there are lots of contacts and just a few followup events)

            events = Event.objects.filter(status=EVENT_STATUS_FOLLOWUP_REMINDER)
            events = events.filter(creator=alias)
            events = events.filter(start__gt=now())
            followup_contacts = combine_lists([event.original_email.contacts.filter(email2contact__type=Email2Contact.TYPE_REF) for event in events])
            queryset = queryset.filter(pk__in=[c.pk for c in followup_contacts])

        if self.page == 'all':
            contacts = distinct_list_from_queryset(queryset)
            page_struct = None
        else:
            p = LazyPaginator(queryset, HOWMANY_CONTACTS_COMPACT if self.compact() else HOWMANY_CONTACTS_FULL)
            try:
                page_struct = p.page(self.page)
            except (EmptyPage, InvalidPage):
                page_struct = p.page(1)
            contacts = page_struct.object_list

        contacts_json = [c.as_json(alias=alias, include_secondary_emails=False, viewable_contacts=Contact.objects.viewable(alias)) for c in contacts]

        # If only one result (and it was searched for), we'll want to expand it
        if self.search and self.search.search_query and (self.search.search_type in [SEARCH_TYPE_CONTACT, SEARCH_TYPE_COMPANY]) and (queryset.count() == 1):
            one_contact = queryset.get()
        else:
            one_contact = None

        # If we're being told to create a new contact, that's what we'll do
        if self.search and self.search.search_type == SEARCH_TYPE_NEWCONTACT:
            new_contact = self.search.search_query
        else:
            new_contact = None

        return page_struct, one_contact, new_contact, contacts_json


    def refresh_html(self, request):

        log("WidgetContacts: refresh_html start...")
        #log("how we got here: ", ''.join(traceback.format_stack()))

        alias = request.session['alias']
        c = self.context(request)

        page_struct = None
        if not self.expanded:
            # Collapsed view - get all the contacts for this mode
            # (This might set expanded view if the user's search resulted in one contact)
            page_struct, one_contact, new_contact, contacts_json = self.calc_current_contacts(request)
            if one_contact:
                self.expand(one_contact.id, 'readonly', request)
                # this will set self.contact_detail
            elif new_contact:
                if new_contact.get('name'):
                    name = new_contact['name']
                else:
                    name = ' '.join([new_contact['first_name'], new_contact['last_name']]).strip()
                self.expand(0, 'new', request, email=new_contact['email'], name=name)
            else:
                c['contacts'] = contacts_json
                c['other_contacts'] = []                    # not used, currently

        if self.contact_detail:
            old_edit = self.contact_detail_json.get('edit', None)
            self.contact_detail_json = self.contact_detail.as_json_details(account=alias.account, alias=alias)
            self.contact_detail_json['id'] = self.contact_detail.id
            self.contact_detail_json['edit'] = old_edit
            self.contact_detail_json['buckets'] = Bucket.objects.filter(account=alias.account).filter(contacts=self.contact_detail).order_by('name')

        c['expanded'] = self.expanded
        c['contact_detail'] = self.contact_detail_json
        c['prev_contact_detail'] = self.prev_contact_detail_json
        c['default_fields'] = constants.CONTACT_DEFAULT_FIELDS
        c['page_struct'] = page_struct

        t = loader.get_template('widgets/contacts.html')
        html = t.render(c)
        self.stale = False
        return html
        log("WidgetContacts: refresh_html end")




#class WidgetSearch(Widget):
#
#    def __init__(self, manager, widget_id, position='right'):
#        super(WidgetSearch, self).__init__(manager, _('Search'), widget_id, position)
#        self.name = WIDGET_NAME_SEARCH
#
#    def context(self, request):
#        alias = request.session['alias']
#        account = alias.account
#
#        c = super(WidgetSearch, self).context(request)
#        c.update({\
#            'buckets': format_buckets_for_display(alias, self.bucket, intro_text=_('Everything'))
#        })
#        return c
#
#    def change_bucket(self, bucket_slug, request):
#        log("change_bucket: slug = ", bucket_slug)
#        if bucket_slug:
#            self.bucket = Bucket.objects.get(account=request.session['alias'].account, slug=bucket_slug)
#            log("Setting bucket to %s" % self.bucket.name)
#        else:
#            self.bucket = None
#            log("Setting bucket to None")
#        self.manager.notify_all_except(self.widget_id, WMChangeBucket(bucket=self.bucket), request)
#        self.stale = True
#
#    def refresh_html(self, request):
#        log("WidgetSearch: refresh_html start...")
#        html = ''
#        try:
#            log("WidgetSearch.refresh_html")
#            alias = request.session['alias']
#            c = self.context(request)
#            t = loader.get_template('widgets/search.html')
#            html = t.render(c)
#            self.stale = False
#        except:
#            log("WidgetSearch.refresh_html:", traceback.format_exc())
#        return html


class WidgetComingUp(Widget):

    def __init__(self, manager, widget_id, position='left'):
        super(WidgetComingUp, self).__init__(manager, _('Coming Up'), widget_id, position)
        self.listening_for.extend([WMUpdateScheduledEvents, WMSearch, WMNewEmails, WMUpdateEvents, WMChangeFollowupOnly])
        self.name = WIDGET_NAME_COMINGUP

    def refresh_html(self, request):
        log("WidgetComingUp: refresh_html start...")
        #log("how we got here: ", ''.join(traceback.format_stack()))

        alias = request.session['alias']
        contact = alias.contact
        account, account_user = alias.account, alias.account_user

        n = now()

        queryset = Email.objects.filter(owner_alias=alias).exclude(message_type=EMAIL_TYPE_NOTED)
        #    .filter(email2contact__account=self.manager.account, contacts=contact)\
        #    .filter(events__start__lte=now()+HOWLONG_COMING_UP)

        if self.followup_only:
            queryset = queryset.filter(events__start__gte=n, events__status=EVENT_STATUS_FOLLOWUP_REMINDER)
        else:
            queryset = queryset.filter(events__start__gte=n)
            # No need to filter on status in this query -- (almost?) all events that are >= now will be of these types, and we do further filtering below
            #, events__status__in=[\
            #    EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_SCHEDULED_FADING, EVENT_STATUS_TENTATIVE_FADING, EVENT_STATUS_FOLLOWUP_REMINDER])

        # Filter by search
        if self.search:
            if self.search.search_type == SEARCH_TYPE_TEXT:
                # Searching for emails by text - currently NOT USED
                search_query = self.search.search_query
                if search_query:
                    search_terms = [term.lower() for term in search_query.split()]
                    for term in search_terms:
                        q_search_term = \
                            Q(events__title_long__icontains=term) |\
                            Q(contacts__email__icontains=term) |\
                            Q(contacts__first_name__icontains=term) |\
                            Q(contacts__last_name__icontains=term)
                        if term[0] == '@':
                            q_search_term |= Q(contacts__contact_buckets__tag__text=term[1:].lower())
                        queryset = queryset.filter(q_search_term)
            elif self.search.search_type == SEARCH_TYPE_COMPANY:
                queryset = queryset.filter(email2contact__account=self.manager.account, contacts__company__iexact=self.search.search_query)
            elif self.search.search_type == SEARCH_TYPE_CONTACT:
                queryset = queryset.filter(email2contact__account=self.manager.account, contacts=self.search.search_query)
            elif self.search.search_type in [SEARCH_TYPE_NORESULTS, SEARCH_TYPE_NEWCONTACT]:
                queryset = queryset.none()

        events = []
        for e in queryset:
            events.extend(list(e.events.filter(status__in=[EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_SCHEDULED_FADING, EVENT_STATUS_TENTATIVE_FADING, EVENT_STATUS_FOLLOWUP_REMINDER], start__gte=now())))
        events = uniqify(events)

        user_tz = account_user.get_timezone()
        n = now().astimezone(user_tz)
        display_events = []
        for event in events:
            creator = event.original_email.from_contact
            owner = event.original_email.owner_alias.contact
            attendees = event.original_email.contacts.all()
            local_date = account_user.local_now()
            attendees = uniqify([c for c in attendees if c != contact])
            attendees_not_in_text = [c for c in attendees if (not c.alias) or c.get_tag_or_name_for_display(contact) not in event.title_long]

            people_non_creator, people_non_contact = [], []
            for c in attendees_not_in_text:
                if c != creator:
                    people_non_creator.append(c)
                if c != contact:
                    people_non_contact.append(c)

            people_non_contact_short = people_non_contact[:MAX_CONTACTS_DISPLAY_SCHEDULED]
            log("user_tz: %s" % user_tz)
            log("event: %s" % event.start)
            display_events.append({\
                'event':                event,
                'reminder':             event.title_long,
                'followup_reminder':    event.status == EVENT_STATUS_FOLLOWUP_REMINDER,
                'uid':                  event.uid,
                'id':                   event.id,
                'fading':               event.status in [EVENT_STATUS_SCHEDULED_FADING, EVENT_STATUS_TENTATIVE_FADING],
                'datetime':             format_span(event, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=user_tz),
                'attendees':            attendees,
                'attendees_short':      attendees_not_in_text[:MAX_CONTACTS_DISPLAY_SCHEDULED],
                'attendees_short_more': len(attendees_not_in_text)>MAX_CONTACTS_DISPLAY_SCHEDULED,
                'has_response':         event.event_children.filter(response_type=EVENT_RESPONSE_TYPE_YES).count() > 0,
                'has_perms':            event.has_owner_perms(alias),
                'people_non_creator':   people_non_creator,
                'people_non_contact':     people_non_contact,
                'people_non_contact_short':    people_non_contact_short,
                'people_non_contact_short_more': len(people_non_contact_short) > MAX_CONTACTS_DISPLAY_SCHEDULED
            })

        display_events.sort(key=lambda de: de['event'].start)
        #display_events = display_events[:HOWMANY_COMING_UP]
        c = self.context(request)
        c['events'] = display_events
        t = loader.get_template('widgets/coming_up.html')
        html = t.render(c)
        self.stale = False
        return html
        log("WidgetComingUp: refresh_html end")

    def notify(self, wm, request=None):
        if type(wm) is WMNewEmails:
            #if e and e.events and any([e.events.filter(status__in=[EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_FOLLOWUP_REMINDER]).count() > 0 for e in wm.new_emails]):
            self.clear_cache()
            super(WidgetComingUp, self).notify(wm, request=request)
            log('[] UNCOMMENT ')
        elif type(wm) is WMUpdateEvents:
            self.clear_cache()
            super(WidgetComingUp, self).notify(wm, request=request)
        else:
            super(WidgetComingUp, self).notify(wm, request=request)


def get_widgets_data(request, result_type, widget_ids=None):
    "Get an updated view into a widget"

    s = Session.objects.get(pk=request.session._get_session_key())
    d = s.get_decoded()
    try:
        log("^^^^^^^^^ 5. In the saved session, it is now:", d['widget_managers']['firefox'].widgets[0].search.search_query, request.session._get_session_key())
    except:
        pass

    try:
        alias = request.session['alias']
        # TODO: Make sure active alias
        if not alias:
            raise Exception("get_widgets: must have active alias")

        if widget_ids is None:
            widget_ids = request.GET.get('widget_ids', 'updated')

        cmd = request.GET.get('cmd', None)

        log("get_widgets_data: ", cmd)
        widget_manager = get_widget_manager(request)

        if cmd == 'toggle_size':
            widget_manager.toggle_size_by_id(widget_ids, request)
        elif cmd == 'page':
            page_num = request.GET.get('arg1', 1)
            widget_manager.change_page_by_id(widget_ids, page_num, request)
        elif cmd == 'search':
            search_query = request.GET.get('arg1', '').strip()
            search_type = request.GET.get('arg2', SEARCH_TYPE_TEXT)
            if search_type in ['false', 'null']:
                search_type = SEARCH_TYPE_TEXT
            widget_manager.change_search(search_query, search_type, request, widget_ids=widget_ids)
        elif cmd == 'bucket':
            bucket_slug = request.GET.get('arg1', '')
            widget_manager.change_bucket(bucket_slug, request, widget_ids=widget_ids)
        elif cmd == 'view_followup_only':
            followup_only = bool(int(request.GET.get('arg1', 0)))
            widget_manager.change_followup_only(followup_only)
            widget_manager.change_search('', SEARCH_TYPE_TEXT, request)
        elif cmd == 'go_back':
            widget_manager.go_back(request)
        elif cmd == 'false':
            pass                            # no command
        else:
            raise Exception("get_widgets_json: unknown command %s" % cmd)

        more = False                        # only the "one" widget id needs to return more

        if widget_ids == 'force_refresh_all':
            widget_data = widget_manager.force_refresh_all(request, result_type)
        elif widget_ids.startswith('force_refresh_'):
            widget_data = widget_manager.force_refresh_selected(widget_ids.replace('force_refresh_', ''), request, result_type)
        elif widget_ids == 'all':
            widget_data = widget_manager.retrieve_all(request, result_type)
        elif widget_ids == 'one_stale':                     # Return a set of widgets containing at most one stale (i.e. expensive-to-compute) widget
            first = bool(int(request.GET.get('arg1', 0)))   # Whether or not this is the first in a sequence of such calls
            widget_data, more = widget_manager.retrieve_one_stale(request, first, result_type)
        else:
            action = WIDGET_ACTION_REPLACE_OR_PREPEND if cmd == 'toggle_size' else WIDGET_ACTION_REPLACE_OR_APPEND
            widget_data = widget_manager.retrieve_updated(request, result_type, action=action)

        if result_type == 'json':
            d = {
                'success': True,
                'more': more,
                'widgets': widget_data,
                'search_display': get_search_display(request),
                'bucket_display': get_bucket_display(request)
            }
        elif result_type == 'xml':
            d = '<document><success>true</success><more>%s</more><search_display>%s</search_display><bucket_display>%s</bucket_display>%s</document>' % \
                (str(more).lower(), encode_cdata(get_search_display(request)), encode_cdata(get_bucket_display(request)), widget_data)
        else:
            log ("Unknown result type:", result_type)
            d = ''

        return d

    except NotifyUserException, e:
        log('get_widgets_json: error',traceback.format_exc())
        if result_type == 'json':
            d = {
                 'success': False,
                 'msg': e.message
            }
        elif result_type == 'xml':
            d = '<document><success>false</success><msg>%s</msg></document>' % e.message
        return d
    except:
        log('get_widgets_json: error',traceback.format_exc())
        if result_type == 'json':
            d = {
                'success': False
            }
        elif result_type == 'xml':
            d = '<document><success>false</success></document>'
        else:
            log ("Unknown result type:", result_type)
            d = ''

        return d


@csrf_required
def get_widgets(request):

    result_type = 'json'

    try:
        log("get_widgets start:", request.session['alias'], request.session._get_session_key(), request.GET)
        log("widget status: ", [(w, w.stale, w.search and w.search.search_query) for w in get_widget_manager(request).widgets])

        result_type = request.GET.get('type', 'json')
        if result_type not in ['xml', 'json']:
            log("get_widgets: unknown result type ", result_type)
            result_type = 'json'
        data = get_widgets_data(request, result_type)

        log("get_widgets end:", request.session['alias'], request.GET)
        if result_type == 'xml':
            return HttpResponse(data, mimetype='text/xml')
        else:
            return json_response(request, data)
    except:
        if result_type == 'xml':
            return HttpResponse('<document><success>false</success></document>', mimetype='text/xml')
        else:
            d = {
                'success': False,
            }
            return json_response(request, d)


@csrf_required
def reset_dashboard(request):

    try:
        get_widget_manager(request).change_followup_only(False)
        get_widget_manager(request).change_search('', SEARCH_TYPE_TEXT, request)

        d = {
            'success': True,
        }
        return json_response(request, d)
    except:
        d = {
            'success': False,
        }
        return json_response(request, d)


@csrf_required
def expand_contact(request):
    try:
        log("expand_contact")

        widget_manager = get_widget_manager(request)
        widget_id = request.GET['widget_id']            # name or number
        mode = request.GET.get('mode', 'readonly')
        id_or_slug = request.GET.get('id_or_slug', None) or request.GET.get('id', None)
        alias = request.session['alias']
        name = request.GET.get('name', '')              # For new contacts only, populate their name with this value
        email = request.GET.get('email', '')            # For new contacts only, populate their email address with this value

        if mode != 'new':
            try:
                if id_or_slug.isalpha():
                    contact = Contact.objects.get(owner_account=alias.account, unsubscribe_slug=id_or_slug)
                else:
                    contact = Contact.objects.get(owner_account=alias.account, id=int(id_or_slug))
            except:
                widget_manager.notify_all(WMUpdateAll(), request)
                raise NotifyUserException(_('Cannot expand this contact - it may be deleted.'))

        contact = widget_manager.retrieve_by_id(widget_id).expand(id_or_slug, mode, request, email=email, name=name)

        widget_json = widget_manager.retrieve_updated(request, 'json')

        d = {
            'success': True,
            'widgets': widget_json,
            'search_display': get_search_display(request),
            'bucket_display': get_bucket_display(request),
            'is_new':   (contact is None),
            'contact_email': contact.email if contact else None,
            'id': contact.id if contact else request.GET.get('id', None),
            'contact_name': contact.get_display_name() if contact else (name or None),
        }
        return json_response(request, d)

    except NotifyUserException, e:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message,
        }
        return json_response(request, d)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot expand contact.'),
        }
        return json_response(request, d)


@csrf_required
def collapse_contact(request):
    try:
        log("collapse_contact")

        widget_id = request.GET['widget_id']        # name or number
        widget_manager = get_widget_manager(request)
        widget_manager.retrieve_by_id(widget_id).collapse(request)

        widget_json = widget_manager.retrieve_updated(request, 'json')

        d = {
            'success': True,
            'widgets': widget_json,
            'search_display': get_search_display(request),
            'bucket_display': get_bucket_display(request)
        }
        return json_response(request, d)

    except:
        d = {
            'success': False,
            'msg': _('cannot collapse contact')
        }

        return json_response(request, d)


@csrf_required
def set_conversations_filter(request):
    try:
        log("set_conversations_filter")
        widget_id = request.GET['widget_id']    # name or number
        filter = request.GET['filter']
        get_widget_manager(request).retrieve_by_id(widget_id).set_filter(filter)

        widget_json = get_widget_manager(request).retrieve_updated(request, 'json')

        d = {
            'success': True,
            'widgets': widget_json,
            'search_display': get_search_display(request),
            'bucket_display': get_bucket_display(request)
        }

        result = json.dumps(d)
        return HttpResponse(result)
    except:
        d = {
            'success': False,
            'msg': _('cannot set conversations filters')
        }

        result = json.dumps(d)
        return HttpResponse(result)


@csrf_required
def edit_buckets(request):

    try:
        log("edit_buckets start")

        cmd = request.REQUEST['cmd']
        name = request.REQUEST.get('name', None)
        slug = request.REQUEST.get('slug', None)
        widget_id = request.REQUEST.get('widget_id', None)
        contact_id = request.REQUEST.get('contact_id', None)
        auto_select = bool(int(request.REQUEST.get('auto_select', 0)))
        alias = request.session['alias']
        bucket_added = None
        msg = None

        if cmd == 'add':
            b, created = Bucket.objects.get_or_create_for_alias(alias, name)
            if not created:
                msg = _('A tag named %s already exists.') % name
            bucket_added = b
            get_widget_manager(request).notify_all(WMEditBuckets(cmd, b), request)
            if auto_select:
                get_widget_manager(request).change_bucket(bucket_added.slug, request)
        elif cmd == 'remove':
            if slug:
                queryset = Bucket.objects.filter(account=alias.account, slug=slug)
                for b in queryset:
                    if not (alias.is_admin or alias == b.owner_alias):
                        raise NotifyUserException(_('You do not have permissions to delete tag %s.') % b.name)
                    b.delete()
                    get_widget_manager(request).notify_all(WMEditBuckets(cmd, b), request)

        elif cmd == 'add_contact':
            if slug:
                queryset = Bucket.objects.filter(account=alias.account, slug=slug)
                for b in queryset:
                    widget = get_widget_manager(request).retrieve_by_id(widget_id)
                    widget.add_to_bucket(b, int(contact_id), request)

        elif cmd == 'remove_contact':
            if slug:
                queryset = Bucket.objects.filter(account=alias.account, slug=slug)
                for b in queryset:
                    widget = get_widget_manager(request).retrieve_by_id(widget_id)
                    widget.remove_from_bucket(b, int(contact_id), request)


        else:
            raise Exception("edit_buckets: Unknown command")

        d = {
            'success': True,
            'msg': msg
        }
        if bucket_added:
            d['bucket_name'] = bucket_added.name
            d['bucket_slug'] = bucket_added.slug

        return json_response(request,d)

    except NotifyUserException, e:
        log('edit_buckets: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message
        }
        return json_response(request,d)
    except:
        log('edit_buckets: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _("Cannot edit your categories.")
        }
        return json_response(request,d)


@csrf_required
def update_contact (request):
    "Create or edit a contact"
    try:
        id = int(request.REQUEST['id'])
        widget_id = request.REQUEST['widget_id']    # name or number

        # private = bool(int(request.REQUEST['private']))
        email = request.REQUEST['email'].strip().lower()
        name = request.REQUEST['name'].strip()
        phone = request.REQUEST['phone'].strip()
        title = request.REQUEST['title'].strip()
        company = request.REQUEST['company'].strip()
        website = request.REQUEST['website'].strip()
        description = request.REQUEST['description'].strip()
        twitter_screen_name = request.REQUEST['twitter_screen_name'].strip().lower().strip('@')
        create_new = bool(int(request.REQUEST['create_new']))
        first_name, last_name = get_first_last_name(name)
        alias = request.session['alias']

        email = trim_nonalnum(email)
        if email and not RE_EMAIL.match(email):
            d = {
                'success': False,
                'msg': _("You must provide a valid email address.")
            }
            return json_response(request,d)

        if twitter_screen_name and not RE_TWITTER.match(twitter_screen_name):
            d = {
                'success': False,
                'msg': _("You must provide a valid Twitter user name.")
            }
            return json_response(request,d)

        widget = get_widget_manager(request).retrieve_by_id(widget_id)

        contact = widget.update({
            'email':        email,
            'first_name':   first_name,
            'last_name':    last_name,
            'phone':        phone,
            'title':        title,
            'company':      company,
            'website':      website,
            'description':  description,
            'twitter_screen_name':  twitter_screen_name
        }, request)

        if create_new:
            contact = widget.expand(0, 'new', request)
        else:
            contact = None

        widget_json = get_widget_manager(request).retrieve_updated(request, 'json')

        d = {
            'success': True,
            'widgets': widget_json,
            'search_display': get_search_display(request),
            'bucket_display': get_bucket_display(request),
            'is_new':  create_new and (contact is None),
            'contact_email': contact.email if contact else None,
            'id': id,
            'contact_name': contact.get_display_name() if contact else None,
            'msg': _("Saved contact info.")
        }

        return json_response(request,d)
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _("Could not save contact info.")
        }
        return json_response(request,d)


@csrf_required
def cancel_edit_contact (request):
    """When the user is editing a contact and clicks Back, this takes them back to
    the non-edit detail view of the contact."""

    try:
        id = int(request.REQUEST['id'])
        widget_id = request.REQUEST['widget_id']    # name or number

        widget = get_widget_manager(request).retrieve_by_id(widget_id)

        # Go back to the readonly view --
        #  this fails if user doesn't have permission for id
        #  notify=False because:
        #    1.) We're still zoomed in on the same contact as before (no need to notify other contacts)
        #    2.) Don't want to call reset_page, which would overwrite the contact's back-button info (old_search, etc.)
        widget.expand(id, 'readonly', request, notify=False)

        widget_json = get_widget_manager(request).retrieve_updated(request, 'json')

        d = {
            'success': True,
            'widgets': widget_json,
            'search_display': get_search_display(request),
            'bucket_display': get_bucket_display(request),
            'is_new':  False,
            'id': id
        }

        return json_response(request,d)
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _("Could not go back.")
        }
        return json_response(request,d)


@csrf_required
def edit_contact_extra_info(request):
    "Add secondary email to an account"

    try:
        widget_manager = get_widget_manager(request)
        id = int(request.REQUEST['id'])
        alias = request.session['alias']
        cmd = request.REQUEST['cmd']
        widget_id = request.REQUEST['widget_id']    # name or number
        widget = widget_manager.retrieve_by_id(widget_id)
        type = request.REQUEST['type']
        data = request.REQUEST['data']

        contact = Contact.objects.get(id=id, owner_account=alias.account)

        if cmd == 'add' and type == 'email':
            email = data.strip().lower()
            if not RE_EMAIL.match(email):
                raise NotifyUserException(_("Please enter a valid email address."))
            if Contact.objects.active().filter(owner_account=alias.account).filter(Q(email=email) | Q(extrainfo__data=email)).exclude(alias=None).count() > 0:
                raise NotifyUserException(_("Cannot add the email address of a team member."))

            # Add the secondary email
            #  Doing this always makes the contact active
            contact, created = Contact.objects.get_or_create_smart(\
                viewing_alias = alias,
                contact_to_update = contact,
                update_alias = True,
                email = email,
                status = Contact.STATUS_ACTIVE
            )
            if contact:
                contact.save()
            else:
                raise NotifyUserException(_('Cannot edit contact.'))
        else:
            raise NotifyUserException(_('Cannot edit contact.'))

        widget_manager.notify_all([WMUpdateContacts(), WMUpdateNewsfeeds()], request)
        widget_json = widget_manager.retrieve_updated(request, 'json')

        d = {
            'success': True,
            'widgets': widget_json,
            'search_display': get_search_display(request),
            'bucket_display': get_bucket_display(request),
            'msg': _("Saved contact info.")
        }

        # Refresh the Contacts widget
        #  (notify=False so we don't destroy the history for the Back button)
        widget.expand(id, 'read_only', request, notify=False)
        return json_response(request,d)

    except NotifyUserException, e:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message
        }
        return json_response(request,d)
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _("Could not save contact info.")
        }
        return json_response(request,d)

# exempt, not required! This is called by a jQuery plugin that doesn't know about csrf middleware.
@csrf_exempt
def upload_csv(request):

    import StringIO

    def count_nonblank_items(row):
        n = 0
        for item in row:
            if item:
                n += 1
        return n

    def get_header_row(rows):
        "Sometimes the first row is a list of field names (e.g., exporting contacts from Google)"
        headers=[strip_nonalpha(c).lower() for c in rows[0]]
        if all(headers) and any(['email' in h for h in headers]) and any(['name' in h for h in headers]):
            return rows[0], rows[1:]
        else:
            return None, rows

    def find_best_column_indices(rows):
        columns_count = [0] * len(rows[0])
        for r in rows:
            for i in xrange(len(r)):
                if r[i]:
                    columns_count[i] += 1
        columns_count_tuples = list(enumerate(columns_count))
        columns_count_tuples.sort(key=lambda t: -t[1])          # Sort the columns from most frequently populated to least
        columns_count_tuples = columns_count_tuples[:MAX_CSV_COLUMNS]
        columns_count_tuples.sort(key=lambda t: t[0])           # Sort them again from left to right for display
        return [t[0] for t in columns_count_tuples]

    def slice_row(row, column_indices):
        return [row[i] for i in column_indices]

    def find_best_example_rows(rows):
        import copy
        new_rows = copy.copy(rows)
        new_rows.sort(key=lambda r: -count_nonblank_items(r))
        return new_rows[:2]

    def guess_field_names(header):
        result = []
        for h in header:
            h = strip_nonalpha(h).lower()
            if 'email' in h:
                field_name = 'email'
            elif 'first' in h and 'name' in h:
                field_name = 'first_name'
            elif 'last' in h and 'name' in h:
                field_name = 'last_name'
            elif 'web' in h:
                field_name = 'website'
            elif 'company' in h:
                field_name = 'company'
            elif 'title' in h:
                field_name = 'title'
            elif 'phone' in h:
                field_name = 'phone'
            else:
                field_name = ''
            result.append(field_name if field_name not in result else '')
        return result

    # This is called directly by the Flash uploadify thingy
    try:
        import csv

        f = request.FILES['upload_file_csv']
        csv_string = f.read()
        csv_string = csv_string.replace('\r\n', '\n').replace('\r', '\n')
        csv_stream = StringIO.StringIO(csv_string)

        reader = csv.reader(csv_stream)
        raw_rows = list(reader)
        rows = []
        for r in raw_rows:
            if r:
                row = []
                for c in r:
                    try:
                        c = c.decode('utf-8')
                    except:
                        c = strip_nonascii(c)
                    # Turn various representations of 'blank' into the blank string
                    if (c=='\\N') or (len(c)==1 and not c.isalnum()):
                        c = ''
                    row.append(c)
                rows.append(row)

        # Some basic sanity checks
        if not rows:
            raise Exception()
        n_columns = len(rows[0])
        if n_columns == 0:
            raise Exception()
        header, rows = get_header_row(rows)

        if not (all([len(r) == n_columns for r in rows]) or (header and all([len(r) == n_columns+1 for r in rows]))):
            raise Exception()

        column_indices = find_best_column_indices(rows)
        sliced_rows = [slice_row(row, column_indices) for row in rows]
        sliced_header = slice_row(header, column_indices) if header else None
        example_rows = find_best_example_rows(sliced_rows)

        log("Storing rows:", sliced_rows)
        request.session['csv_rows'] = sliced_rows

        bucket = get_widget_manager(request).retrieve_by_id('contacts').bucket

        import copy
        default_fields = copy.copy(constants.CONTACT_DEFAULT_FIELDS)
        default_fields.append(ContactField('note', _('* Note'), True))

        d = {
            'success':              True,
            'example_rows':         example_rows,
            'default_bucket_slug':  bucket.slug if bucket else '',
            'default_fields':       [f.as_json() for f in default_fields],
            'init_field_choices':   guess_field_names(sliced_header) if sliced_header else [''] * len(example_rows[0])
        }

        # Don't use json_response to return this -- we don't want the application/json data type
        # Unfortunately, the result gets parsed by the jQuery ajax_upload library as HTML...
        # escape nasties to avoid this
        j = json.dumps(d)
        j = j.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;')
        return HttpResponse(j)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not understand your .csv file.')
        }
        return HttpResponse(json.dumps(d))

@csrf_required
def import_csv(request):

    try:
        selections = request.REQUEST['selections']
        category = request.REQUEST['category']
        csv_rows = request.session['csv_rows']
        alias = request.session['alias']
        field_names = selections.split(',')

        if not any([f=='email' for f in field_names]):
            d = {
                'success': False,
                'msg': _("You must select a field as 'email'.")
            }
            return json_response(request,d)

        #####import network.tasks
        #####network.tasks.ImportCSVTask().delay(alias, csv_rows, field_names, category)
#        if not m:
#            raise Exception()

        d = {
            'success': True,
            'msg': _('We are importing your contacts. For large imports, this process can take a while. We will email you as soon as the import is complete.')
        }
        return json_response(request,d)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not import your .csv file.')
        }
        return json_response(request,d)


@csrf_required
def import_plaxo(request):
    try:
        CATEGORY_NAME = 'plaxo'         # Auto-categorize Plaxo contacts into this category
        alias = request.session['alias']

        data = json.loads(request.REQUEST['data'])
        for (name, addr) in data:
            contact, created = Contact.objects.get_or_create_smart(\
                name=name, email=addr, viewing_alias=alias, source=Contact.SOURCE_PLAXO, categories=[CATEGORY_NAME])

        get_widget_manager(request).notify_all([WMUpdateContacts()], request)
        bucket = Bucket.objects.filter(name=CATEGORY_NAME, account=alias.account)[0]

        d = {
            'success': True,
            'bucket_name':  bucket.name,
            'bucket_slug':  bucket.slug
        }

        return json_response(request,d)
    except:
        d = {
            'success': False,
            'msg': _('Could not import contacts from Plaxo.')
        }
        return json_response(request,d)


@global_def
def contacts_compose(request, subdomain=None):
    "A view - the compose popup window"
    try:
        alias = request.session['alias']
        contact_ids = request.GET['contact_ids']    # String, which is a comma-separated list of contact IDs
                                                    # From these, we'll end up eliminating ones with no email addrs
                                                    # We'll allow the user to filter out ones that have been sent
                                                    # to recently (last 48 hours)

        if not alias.account.allow_formmail():
            raise Exception()

        message = loader.find_template_source('modals/contacts_compose_template.html')[0]
        message += alias.get_first_name_or_email()

        t = loader.get_template('modals/contacts_compose.html')
        c = Context({
            'initial_contact_ids':      contact_ids,
            'message':                  message,
            'default_fields':           [f.as_json() for f in constants.CONTACT_DEFAULT_FIELDS]
        })
        c.update(global_context(request))
        return HttpResponse(t.render(c))

    except Exception, e:
        log("outlook_compose error")
        return HttpResponseRedirectAgent(request, '/dash/')


@csrf_required
def contacts_send(request):
    "Send a form mail"
    try:
        alias = request.session['alias']
        mode = request.REQUEST['mode']
        contact_ids = request.REQUEST.get('contact_ids', None)
        num_contacts = request.REQUEST.get('num_contacts', None)
        preview_contact_id = request.REQUEST.get('preview_contact_id', None)
        preview_parse = bool(int(request.REQUEST.get('preview_parse', True)))
        to = request.REQUEST['to']
        from_type = request.REQUEST['from']        # 'alias', 'assistant' or 'assistant_sender'
        subject = request.REQUEST['subject'].strip()
        message = request.REQUEST['message']

        include_unsubscribe_link = (from_type in ['assistant', 'assistant_sender'])
        if not alias.account.allow_formmail():
            raise Exception()

        from assist.reminder import extract_items_from_content
        span, eventspan = None, ''
        user_tz = alias.account_user.get_timezone()
        items = extract_items_from_content(subject, message, now().astimezone(user_tz), {})[0]
        if items and items[0]['at_symbol']:
            span = items[0]['span']
            eventspan = format_span(span, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=user_tz)

        if mode == 'preview':

            num_contacts = int(num_contacts)
            include_unsubscribe_link &= (num_contacts >= HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK)

            # For now, sending to lots of contacts can only be done by the assistant
            if (num_contacts >= HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK) and from_type not in ['assistant', 'assistant_sender'] and not alias.account.allow_mass_formmail_alias_sender:
                raise Exception("Cannot send to %d contacts as alias" % num_contacts)

            preview_contact_id = int(preview_contact_id)
            contact = Contact.objects.viewable(alias).get(id=preview_contact_id)
            email = contact.email
            ####&&&&from assist.tasks import render_user_template
            ###&&&&#message_rendered, subject_rendered, template_good, ics = render_user_template(alias, contact, subject, message, \
            ###&&&&    include_unsubscribe_link=include_unsubscribe_link, parse=preview_parse)
            ###&&&& was if template_good
            if True:
                d = {
                     'success':     True,
                     'preview':     message,
                     'preview_subject': subject,
                     'email':       email,
                     'contact_id':  preview_contact_id,
                     'eventspan':   eventspan
                }
                return json_response(request,d)

            else:
                d = {
                    'success':     False,
                    'msg':         _('There is an error in your email template.')
                }
                return json_response(request,d)

        elif mode == 'send':

            contact_ids = [int(id) for id in contact_ids.split(',')]
            contacts = Contact.objects.viewable(alias).filter(id__in=contact_ids)
            include_unsubscribe_link &= (len(contact_ids) >= HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK)

            # For now, sending to lots of contacts can only be done by the assistant
            if (len(contact_ids) >= HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK) and from_type not in ['assistant', 'assistant_sender'] and not alias.account.allow_mass_formmail_alias_sender:
                raise Exception("Cannot send to %d contacts as alias" % len(contact_ids))

            # Create a fake newsfeed item; all the mails sent out will have original_email equal to this fake mail
            log("Creating fake newsfeed item...")

            content = subject + '\n' + message

            if contacts.count() == 1:
                ####&&&&body, subject, template_good, ics = assist.tasks.render_user_template(alias, contacts[0], subject, message)
                body = message
            else:
                body = message

            from crawl.procmail import procmail_fakeevent
            newsfeed_email = procmail_fakeevent(\
                alias=alias,
                source=Email.SOURCE_FORMMAILCONF,
                message_subtype=EMAIL_SUBTYPE_USER_FORMMAIL_CONFIRM,
                subject=subject,
                body=body,
                contacts=contacts,
                contacts_type=Email2Contact.TYPE_FORMMAIL,
                include_unsubscribe_link=include_unsubscribe_link,
                span=span,
                event_status=EVENT_STATUS_SCHEDULED if span else None,
                send_reminders=False        # No support for event reminders for mass mail (yet)
            )

            args = {
                'contact_ids':                  contact_ids,
                'template':                     message,
                'subject':                      subject,
                'from':                         from_type,
                'original_email_id':            newsfeed_email.id,
                'include_unsubscribe_link':     include_unsubscribe_link
            }

            noted_meta = NotedTaskMeta()
            noted_meta.type = NotedTaskMeta.TYPE_FORMMAIL
            noted_meta.status = NotedTaskMeta.STATUS_STARTED
            noted_meta.alias = alias
            noted_meta.message = ''
            noted_meta.args_json = json.dumps(args)
            noted_meta.save()

            m = noted_meta.delay()
            get_widget_manager(request).notify_all([WMUpdateAll()], request)

            log("contacts_send started SendFormMailTask")

            d = {
                 'success':     True,
                 'msg':         _("We have begun sending emails to the contacts you selected. You will receive a notification email when we're done.")
            }
            return json_response(request,d)

        else:
            raise Exception()

        d = {
            'success': True,
            'msg': _('We have created an email for you.')
        }
        return json_response(request,d)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not send your email.')
        }
        return json_response(request,d)


# Outlook plugin support
@csrf_required
def get_contact_slugs_by_emails(request):
    """
    Outlook plugin support.
    Given a list of email addresses, return a list of "slugs" of the same length as the original list, where
    each "slug" is either:
        - an actual alphanumeric slug (email address is a good contact),
        - False (this email address should be ignored by Outlook - assistant address or team member address)
        - None (this email address isn't an active contact)
    """
    try:
        alias = request.session['alias']
        addrs = request.GET['addrs'].split(',')
        log("get_contact_slugs_by_email: addrs =", addrs)

        contact_slugs = []
        for e in addrs:
            # Only search for contacts that are not you
            contacts = Contact.objects.viewable(alias).filter(Q(email=e) | Q(extrainfo__data=e))
            if contacts.count() > 0:
                if contacts[0].alias:
                    # Address is a team member, tell Outlook to ignore it
                    contact_slugs.append(False)
                else:
                    # Contact is a regular contact, it's good
                    contact_slugs.append(contacts[0].unsubscribe_slug)
            elif is_ignorable_from_address(ADDR_SPACE_RFC822, e, ignore_mailer_daemon=True):
                # Assistant address or other ignorable address
                contact_slugs.append(False)
            else:
                contact_slugs.append(None)

        d = {
            'success':      True,
            'contact_slugs':  contact_slugs
        }

        return json_response(request,d)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not retrieve contact IDs.')
        }
        return json_response(request,d)


@csrf_required
def contacts_action(request):
    "An AJAX call"

    try:
        alias = request.session['alias']
        cmd = request.REQUEST['cmd']
        contact_ids = request.REQUEST['contact_ids']
        contact_ids = [int(id) for id in contact_ids.split(',')]
        contacts = Contact.objects.viewable(alias, include_notresponded=True).filter(id__in=contact_ids)
        widget_manager = get_widget_manager(request)
        n = now()

        log("contacts_action", cmd)

        if cmd=='compose':

            contacts_unsub = [c for c in contacts if not c.subscribed]
            contacts_noemail = [c for c in contacts if not c.email.strip()]

            contacts = [c for c in contacts if (c not in contacts_unsub) and (c not in contacts_noemail)]

            for c in contacts:
                c.status = Contact.STATUS_ACTIVE
                c.save()

            # Find contacts that have been contacted via formmail recently (by this user), so we can warn the user
            recent_formmails = Email.objects.filter(owner_alias=alias, message_subtype=EMAIL_SUBTYPE_USER_FORMMAIL_CONFIRM, \
                date_sent__gte=n-HOWLONG_FORMMAIL_COMPOSE_CONFIRM, contacts__in=contacts, email2contact__account=alias.account)
            contacts_recent = []
            for e in recent_formmails:
                contacts_recent += [c for c in e.contacts.all() if c in contacts]
            contacts_recent = uniqify(contacts_recent)

            contacts_notrecent = [c for c in contacts if c not in contacts_recent]

            msgs_bad_contacts = []

            if contacts_unsub:
                msgs_bad_contacts.append(_('The following contacts are unsubscribed from your form mails and cannot be included:') + '\n\n' + \
                    '\n'.join([c.get_tag_or_name_for_display(alias) for c in contacts_unsub]))

            if contacts_noemail:
                msgs_bad_contacts.append(_('The following contacts have no email address and cannot be included:') + '\n\n' + \
                    '\n'.join([c.get_tag_or_name_for_display(alias) for c in contacts_noemail]))

            if contacts_recent:
                msg_recent_contacts = _('The following contacts have been contacted in the last 48 hours. Are you sure you want to include them in this mail?') + '\n\n' + \
                    '\n'.join([c.get_tag_or_name_for_display(alias) for c in contacts_recent])
            else:
                msg_recent_contacts = ''

            contacts_struct = []
            for c in contacts:
                contacts_struct.append({
                    'id':               c.id,
                    'email':            c.email,
                    'recent':           c in contacts_recent
                })

            d = {
                'success': True,
                'msg': '',
                'msgs_bad_contacts': msgs_bad_contacts,
                'msg_recent_contacts': msg_recent_contacts,
                'contacts': contacts_struct,
                'alias_server_smtp_enabled': alias.server_smtp_enabled,
                'account_allow_mass_formmail_alias_sender': alias.account.allow_mass_formmail_alias_sender,
                'HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK': constants.HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK
            }

            return json_response(request,d)

        elif cmd=='delete':
            if any(c.alias for c in contacts):
                raise NotifyUserException(_("Cannot delete team members from your Contacts list."))
            for c in contacts:
                c.inactivate()
            widget_manager.notify_all([WMUpdateNewsfeeds()], request)   # Contact might be completely deleted, so delete the emails
            widget_manager.notify_all([WMUpdateContacts()], request)
        elif cmd=='categorize':
            bucket_slug = request.REQUEST['bucket_slug']
            bucket = Bucket.objects.get(slug=bucket_slug, account=alias.account)
            for c in contacts:
                c.status = Contact.STATUS_ACTIVE
                if bucket.name == SF_BUCKET_NAME:           # Adding a contact to the "salesforce" category sets it to sync
                    c.sf_sync_pending = True
                c.save()
                bucket.contacts.add(c)
            widget_manager.notify_all([WMUpdateContacts()], request)

        elif cmd=='set_reminder':
            for c in contacts:
                c.status = Contact.STATUS_ACTIVE
                c.save()
            date_to_parse = request.REQUEST['date_to_parse']
            if date_to_parse:
                from assist.parsedate.handlers import ParseDate
                pd = ParseDate(options={'now': now().astimezone(alias.account_user.get_timezone())})
                span = pd.easy_parse(date_to_parse)
                if span:
                    from crawl.procmail import procmail_fakeevent
                    newsfeed_email = procmail_fakeevent(\
                        alias=alias,
                        source=Email.SOURCE_FOLLOWUP_REMINDER,
                        message_subtype=EMAIL_SUBTYPE_USER_FOLLOWUP_REMINDER,
                        subject=_('Follow-up'),
                        body='',
                        contacts=contacts,
                        contacts_type=Email2Contact.TYPE_REF,
                        span=span,
                        event_status=EVENT_STATUS_FOLLOWUP_REMINDER,
                        send_reminders=True
                    )
                else:
                    raise NotifyUserException(_("Can't understand your date/time."))
                widget_manager.notify_all([WMUpdateContacts(), WMNewEmails([newsfeed_email])], request)

        elif cmd=='cancel_reminder':
            for c in contacts:
                queryset = Email.objects.filter(events__status=EVENT_STATUS_FOLLOWUP_REMINDER, owner_alias=alias, contacts=c)
                for e in queryset:
                    e.delete()
            widget_manager.notify_all([WMUpdateContacts(), WMUpdateNewsfeeds(), WMUpdateEvents()], request)

        elif cmd=='activate_contacts':
            contacts = Contact.objects.viewable(alias, include_inactive=True).filter(id__in=contact_ids)
            for c in contacts:
                c.status = Contact.STATUS_ACTIVE
                c.save()
            widget_manager.notify_all([WMUpdateNewsfeeds(), WMUpdateContacts()], request)
            if len(contacts) == 1:
                # Zoom into the given contact
                widget_manager.change_search(contacts[0].pk, SEARCH_TYPE_CONTACT, request)

        d = {
            'success': True,
            'msg': _('We have created an email for you.')
        }
        return json_response(request,d)

    except NotifyUserException, e:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message
        }
        return json_response(request,d)
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your request.')
        }
        return json_response(request,d)


@csrf_required
def email_action(request):
    try:
        alias = request.session['alias']
        cmd = request.REQUEST['cmd']
        message_id = request.REQUEST['message_id']
        contact_ids = request.REQUEST.get('contact_ids', '')
        extra_return_values = {}

        email = Email.objects.get(pk=message_id, owner_account=alias.account)
        log("email_action: cmd = ", cmd, " message_id = ", message_id)

        if cmd == 'mark_completed':
            completed = request.REQUEST['completed']
            email.completed = completed
            email.save()

            get_widget_manager(request).notify_all([WMUpdateNewsfeeds()], request)

        elif cmd == 'mark_incomplete':
            email.completed = False
            email.save()

            get_widget_manager(request).notify_all([WMUpdateNewsfeeds()], request)

        elif cmd == 'create_contact':
            contact_ids = request.REQUEST['contact_ids']
            if contact_ids:
                contact_ids = contact_ids.split(',')
                # activate specific contacts
                contacts = Contact.objects.viewable(alias).filter(id__in=contact_ids)
            else:
                # activate all contacts in this email
                contacts = email.contacts.all()
            for c in contacts:
                if c.status != Contact.STATUS_ACTIVE:
                    c.set_active(alias)
            get_widget_manager(request).notify_all([WMUpdateNewsfeeds(), WMUpdateContacts()], request)

        elif cmd == 'add_aliases':
            alias_ids = request.REQUEST['alias_ids'].split(',')
            updated = False
            ez_names = []
            add_aliases = Alias.objects.active().filter(account=alias.account, pk__in=alias_ids)
            for a in add_aliases:
                c = a.contact
                ec, created = email.create_email2contact_by_contact(c, alias.account, Email2Contact.TYPE_ASSIGNED)
                if created:

                    # If alias 'a' replies to this email from the assistant, which thread email and contact
                    #  will they actually be replying to?
                    reply_email, reply_contact = email.get_latest_contact_and_email_from_thread()

                    c = Context({\
                        'from_alias': alias,
                        'alias': a,
                        'account': a.account,
                        'site': a.get_site(),
                        'thread_root': email,
                        'old_emails': [email],
                        'reply_contact': reply_contact,
                    })

                    if alias == a:
                        subject = _("New assignment:") + ' '
                    else:
                        subject = _("New assignment from @%s:") % alias.ez_name + ' '
                    subject += email.from_contact.get_tag_or_name_for_display(a)
                    other_contacts = uniqify([o for o in email.get_attendees() if o != email.from_contact])
                    if other_contacts:
                        subject += ' ' + _('to') + ' '
                        subject += ', '.join([o.get_tag_or_name_for_display(a) for o in other_contacts[:5]])
                    assistant = alias.account.get_assistant()
                    mail.send_email('main_assign_thread.html', c, subject,
                        (assistant.get_full_name(), assistant.email),
                        (a.get_full_name(), a.email),
                        reply_to=(reply_contact.get_full_name(), reply_contact.email) if reply_contact else None,
                        noted={'message_type': EMAIL_TYPE_NOTED,\
                            'message_subtype': EMAIL_SUBTYPE_NOTED_ASSIGN_THREAD,
                            'owner_account': assistant and assistant.owner_account,
                            'original_email': reply_email
                        }
                    )

                    ez_names.append('@' + a.ez_name)
                    updated = True

            extra_return_values['ez_names'] = ', '.join(ez_names)
            extra_return_values['updated'] = updated

            if updated:
                get_widget_manager(request).notify_all([WMUpdateNewsfeeds()], request)

        elif cmd.startswith('sf_create_'):
            # Get the non-team contact for this email, and return its details
            contacts = email.get_reply_contacts('sf_create', alias.contact)
            if contacts:
                c = contacts[0]
                extra_return_values['contact'] = c.as_json_more(alias=alias)
                extra_return_values['subject'] = email.subject
                extra_return_values['body'] = (email.message_body or '')[:2000] # No point in returning the whole message body, since you can't
                                                                                #  pass very long text through a GET url

        elif cmd == 'delete':
            # Delete only the given email

            # Point the children to None to prevent a cascading delete
            for child in email.original_email_children.all():
                child.original_parent = None
                child.save()

            for child in email.email_children.all():
                child.parent = None
                child.save()

            email.delete()
            get_widget_manager(request).notify_all([WMUpdateNewsfeeds()], request)

        elif cmd == 'delete_thread':
            # Delete the email and all its children (cascading delete)
            email.delete()
            get_widget_manager(request).notify_all([WMUpdateNewsfeeds()], request)


        d = {
            'success': True,
            'msg': _('Assignment successful.')
        }
        d.update(extra_return_values)

        return json_response(request,d)

    except NotifyUserException, e:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message
        }
        return json_response(request,d)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not assign this email.')
        }
        return json_response(request,d)



@csrf_required
def get_social_data(request):
    "Look up a contact using various social services (Rapleaf, etc.)"

    from lib.social.multi import MultiFinder
    from network.views_proxy import calc_proxy_url

    try:
        alias = request.session['alias']
        if not alias:
            raise Exception()

        # no longer using rapleaf
        result = {
            'success': False,
            'memberships': None,
            'memberships_html': None,
            'location': None,
            'age': None,
            'gender': None,
            'image_url': None

        }
        return json_response(request, result)

        contact = Contact.objects.get(owner_account=alias.account, id=int(request.GET['id']))

        image_size = int(request.GET.get('image_size', DEFAULT_SOCIAL_IMAGE_SIZE))

        multi_finder = MultiFinder(default_image_size=image_size)
        social_data = contact.update_social_data(default_image_size=image_size)

        # Format the social data that doesn't fit anywhere else and render it using a template
        memberships = multi_finder.get_basic_info(social_data, multi_finder.INFO_MEMBERSHIPS) or []
        t = loader.get_template('widgets/contacts_social.html')
        context = RequestContext(request, dict={
            'title':            multi_finder.get_basic_info(social_data, multi_finder.INFO_TITLE) or '',
            'memberships':      multi_finder.get_basic_info(social_data, multi_finder.INFO_MEMBERSHIPS) or [],
            'education':        multi_finder.get_basic_info(social_data, multi_finder.INFO_EDUCATION) or [],
            'experience':       multi_finder.get_basic_info(social_data, multi_finder.INFO_EXPERIENCE) or [],
            'contact_detail':   contact.as_json_details()
        })
        memberships_html = t.render(context)

        # Get the ez-location (if any)
        location = multi_finder.get_basic_info(social_data, multi_finder.INFO_LOCATION) or ''
        age = multi_finder.get_basic_info(social_data, multi_finder.INFO_AGE) or ''
        gender = multi_finder.get_basic_info(social_data, multi_finder.INFO_GENDER) or ''
        image_url = calc_proxy_url(multi_finder.get_basic_info(social_data, multi_finder.INFO_IMAGE), request) or None

        # Mark it as stale within the contacts widget, so the updated info will show on a reload
        get_widget_manager(request).notify_all([WMUpdateContactSocialData(contact.id, social_data)])

        result = {
            'success': True,
            'memberships': memberships,
            'memberships_html': memberships_html,
            'location': location,
            'age': age,
            'gender': gender,
            'image_url': image_url

        }
        return json_response(request, result)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not get social data for this contact.')
        }
        return json_response(request,d)


@csrf_required
def edit_social_data(request):

    "Look up a contact using various social services (Rapleaf, etc.)"

    from lib.social.multi import MultiFinder

    try:
        alias = request.session['alias']
        if not alias:
            raise Exception()

        contact = Contact.objects.get(owner_account=alias.account, id=int(request.GET['id']))

        if request.REQUEST['cmd'] == 'delete':
            # Delete a specific type of info from that contact's social data

            multi_finder = MultiFinder()

            social_data = contact.social_data()
            if social_data:
                multi_finder.delete_basic_info(social_data, request.REQUEST['key'])
                contact.social_json = json.dumps(social_data)
                contact.save()

            # Mark it as stale within the widget, so it doesn't show after a reload
            get_widget_manager(request).notify_all([WMUpdateContactSocialData(contact.id, social_data)])

            result = {
                'success': True
            }
            return json_response(request, result)

    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not edit social data for this contact.')
        }
        return json_response(request,d)
