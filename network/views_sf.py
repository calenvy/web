#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
from paypal.standard.signals import payment_was_successful, payment_was_flagged
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os, pprint
from utils import *
from django.core import serializers
from assist import mail
from assist.models import *
import traceback

# This is included as an iframe from the Salesforce widget
def help_view(request):
    t = loader.get_template('sf/help.html')
    c = Context({})
    return HttpResponse(t.render(c))
