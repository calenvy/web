"""Requesting and granting permission for files/contacts"""

try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import calendar
import sys, os
from utils import *
from django.core import serializers
from assist.models import *
from schedule import views
from assist.constants import *
from schedule.periods import *
from assist import mail
from assist import constants
import traceback

    
def request_file_perms(request):
    try:
        from assist import reminder
        
        slug = request.REQUEST['slug'].strip()
        alias = request.session['alias']
        account = alias.account
        
        f = File.objects.get(slug=slug, email__owner_account=account)        
        token = f.get_token(alias)
        if token:
            url = token.get_url()
            has_perms = True
        elif f.has_download_perms(alias):
            token = FileToken.objects.create_token(alias, f)
            url = token.get_url()
            has_perms = True
        else:
            has_perms = False
            owner_alias = f.get_owner_alias(alias)
            url = None
            assistant = account.get_assistant()
            context = Context({
                'assistant':            assistant,
                'requester_alias':      alias,
                'owner_alias':          owner_alias,
                'file':                 f,
                'site':                 alias.get_site()
            })
            
            mail.send_email('main_perms_request.html', context, 
                (_('[File Request]') + ' %s ' % f.name),
                (assistant.get_full_name(), assistant.email),
                (owner_alias.get_full_name(), owner_alias.email),
                # ('Calenvy', 'calenvy@master.com'),
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_REQUEST_FILE_PERMS,
                    'owner_account': assistant and assistant.owner_account},
                alias=alias
            )
        
        d = {
            'success': True,
            'has_perms': has_perms,
            'url': url
            # 'msg': _('We just sent an email to request permission.')
        }
        return json_response(request,d)

    except:
        log('__request_file_perms__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not request file permission.')
        }
        return json_response(request,d)    
        


def request_file_perms_windowmob(request):
    "Special support for Windows Mobile, which can't do real AJAX"
    try:
        from assist import reminder
        
        slug = request.GET['slug'].strip()
        alias = request.session['alias']
        account = alias.account
        f = File.objects.get(slug=slug)
        
        if f.email.owner_account != account:
            raise Exception('hacking alert!')
        
        url, has_perms = None, False
        token = f.get_token(alias)
        if token:
            url = token.get_url()
            has_perms = True
        elif f.has_download_perms(alias):
            token = FileToken.objects.create_token(alias, f)
            url = token.get_url()
            has_perms = True
            
        if url:
            return HttpResponseRedirectAgent(request, url)
        else:
            return HttpResponseRedirectAgent(request, '/')
        
    except:
        d = {
            'success': False,
            'msg': _('Could not retrieve file.')
        }
        return json_response(request, d)       
        
      
      


def grant_file_perms(request, subdomain=None):
    
    log("grant_file_perms")
    alias = request.session.get('alias', None)
    try:
        
        file_slug = request.GET.get('file', None)
        requester_id = request.GET['requester']
        
        log ("file_slug:", file_slug)
        log ("requester_slug: ", requester_id)
        
        #TODO: Make this a real page
        f = File.objects.get(slug=file_slug)
        requester_alias = Alias.objects.get(pk=requester_id)
        token = FileToken.objects.create_token(requester_alias, f)
        url = token.get_url()
        
        assistant = alias.account.get_assistant()
        context = Context({
            'assistant':            assistant,
            'requester_alias':      requester_alias,
            'owner_alias':          alias,
            'file':                 f,
            'url':                  url,
            'alias':                requester_alias,
            'site':                 alias.get_site()
        })

        mail.send_email('main_perms_approved.html', context, 
            (_('[Request Approved]') + ' %s ' % f.name),
            (assistant.get_full_name(), assistant.email),
            (requester_alias.get_full_name(), requester_alias.email),
            noted={'message_type': EMAIL_TYPE_NOTED,\
                'message_subtype': EMAIL_SUBTYPE_NOTED_GRANT_FILE_PERMS,
                'owner_account': assistant and assistant.owner_account},
            alias=alias
        )
                
        # TODO: Make this a real page
        msg = _("Temporary access granted to %s for file %s") % (requester_alias.get_full_name_or_email(), f.name)
        request.messages['notices'].append(msg)
        
        return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')

    except:
        log('__grant_perms__ failed')
        log(traceback.format_exc())
        
        msg = _("Failed granting permissions for the file")
        request.messages['warnings'].append(msg)
        
        if alias and alias.account:
            return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')        
        else:
            return HttpResponseRedirectAgent(request, '/dash/')


@global_def
def grant_perms(request, subdomain=None):
    if "file" in request.GET:
        return grant_file_perms(request, subdomain=subdomain)
    else:
        return HttpResponseRedirectAgent(request, '/dash/')
    




