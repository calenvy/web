from django.db import models
from django.db import connection
from django.db.models import *
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib import admin
from django.template import Context, loader, Template
from django.utils.translation import ugettext as _
import hashlib, random, email, calendar, uuid
import traceback
import json
import copy, pdb
from utils import *
from datetime import *
from models import *
from constants import *
from schedule.periods import *
import mail, ics, parsedate.extended
import parsedate
from parsedate.tokens import Span
import register


def send_warning_max_users_exceeded(account, inactivated_already=False, do_it=True):
    """
    Send the admins for an account an email saying there's too many users.
    The caller is responsible for marking the date when the account will be inactivated.
    """

    higher_services = []
    for s in Service.objects.filter(hidden=False):
        if s > account.service:
            higher_services.append({
                'service': s,
                'features': s.get_features_for_display(for_email=True, other=account.service),
                'price': s.get_medium_terms_string(account)
            })

    services = copy.copy(higher_services)
    services.append({
        'service': account.service,
        'features': account.service.get_features_for_display(for_email=True),
        'price': account.service.get_medium_terms_string(account)
    })

    for alias in account.alias.filter(is_admin=True):

        c = Context({\
            'alias': alias,
            'account': account,
            'higher_services': higher_services,
            'services': services,
            'assistant': account.get_assistant(),
            'site': alias.get_site(),
            'num_excess_users': account.num_active_aliases() - account.max_users,
            'inactivated_already': inactivated_already
        })

        assistant = account.get_assistant()

        if do_it:
            log("... sending unused reminder for %s" % str(alias))
            mail.send_email('main_warn_max_users_exceeded.html', c,
                _('Maximum of %d user%s on your %s account - please upgrade') % (account.max_users, 's' if account.max_users>1 else '', alias.get_site().name),
                (assistant.get_full_name(), assistant.email),
                (alias.contact.get_full_name(), alias.email),
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_WARN_MAX_USERS_EXCEEDED,
                }
            )
            alias.last_unused_account_reminder_date = now()
            alias.save()
        else:
            log("... not sending unused reminder for %s because do_it is False" % str(alias))


def send_warning_max_users_exceeded_all(do_it=True):
    """
    Find accounts that have too many users for their service level ( / max_users).
    Mark these accounts for inactivation in the future, and send the admins an email.
    """

    for account in Account.objects.filter(status=ACCOUNT_STATUS_ACTIVE, date_inactivate_max_users_exceeded=None):
        if account.max_users and (not account.trial_period) and (not account.service.is_free(account.promo)) and (account.num_active_aliases() > account.max_users):
            log('Warning account %s because too many users: %d > %d' % (str(account), account.num_active_aliases(), account.max_users))
            if do_it:
                account.date_inactivate_max_users_exceeded = now() + HOWLONG_INACTIVATE_ACCOUNT_MAX_USERS_EXCEEDED
                account.save()

                send_warning_max_users_exceeded(account, inactivated_already=False, do_it=do_it)
            else:
                log('... skipping because do_it = False')
    log('... done.')


def inactivate_max_users_exceeded_all(do_it=True):
    """
    Inactivate accounts that have too many users for their service level.
    These accounts have already received a warning email in the past, so they've had time to upgrade or delete the extra users.
    """
    log('inactivate_max_users_exceeded_all...')
    for account in Account.objects.filter(status=ACCOUNT_STATUS_ACTIVE, date_inactivate_max_users_exceeded__lt=now()).exclude(date_inactivate_max_users_exceeded=None):
        if account.max_users and (account.num_active_aliases() > account.max_users):
            log('Inactivating account %s because too many users: %d > %d' % (str(account), account.num_active_aliases(), account.max_users))
            if do_it:
                account.status = ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED
                account.date_inactivate_max_users_exceeded = None   # We've already inactivated it, mark this as done
                account.date_modified = now()
                account.save()

                send_warning_max_users_exceeded(account, inactivated_already=True, do_it=do_it)
            else:
                log('... skipping because do_it = False')
    log('... done.')



def send_unused_account_reminder(alias, sales_contact, newsfeed_email, do_it=True):
    """
    Send a promotional email containing some recent blog posts.
    Key it to the "customers" list in the "cc" account (on live) so customers can unsubscribe.

    alias: The alias we are sending to
    sales_contact: The corresponding contact in the "cc" account
    """

    # Paranoia
    if not sales_contact.subscribed:
        log("send_unused_account_reminder: avoiding sending to %s because unsubscribed" % sales_contact.email)
        return

    log("send_unused_account_reminder: %s" % str(alias))
    from basic.blog.models import Post

    blog_posts = Post.objects.published().filter(created__gte=alias.last_login_date)
    if alias.last_unused_account_reminder_date:
        blog_posts = blog_posts.filter(created__gte=alias.last_unused_account_reminder_date)
    blog_posts = blog_posts.order_by('-created')
    #blog_posts_trunc = list(blog_posts[:MAX_REMINDER_UNUSED_BLOG_POSTS])

    if blog_posts:
        c = Context({\
            'alias': alias,
            'account': alias.account,
            'assistant': alias.account.get_assistant(),
            'blog_posts': blog_posts,
            'last_login_date':  format_date(alias.last_login_date, tzinfo=alias.account_user.get_timezone()),
            'site': alias.get_site(),
            'sales_contact': sales_contact,
            'newsfeed_email': newsfeed_email
        })

        subject = Template(blog_posts[0].title).render(c)

        assistant = alias.account.get_assistant()

        if do_it:
            log("... sending unused reminder for %s" % str(alias))
            mail.send_email('main_unused_account.html', c, subject,
                (settings.SALES_NAME, settings.SALES_EMAIL),
                (alias.contact.get_full_name(), alias.email),
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_UNUSED_REMINDER,
                }
            )
            alias.last_unused_account_reminder_date = now()
            alias.save()
        else:
            log("... not sending unused reminder for %s because do_it is False" % str(alias))


def send_unused_account_reminders_all(do_it=True):
    """Send all promotional mails containing some recent blog posts.
    Called by a daily cron job."""

    log("send_unused_account_reminders_all start")

    # Determine the subject line of the promotional emails, but only for the newsfeed admin ping
    from basic.blog.models import Post

    try:
        sales_account = Account.objects.active().get(base_url=settings.SALES_ACCOUNT)
        sales_alias = Alias.objects.active().get(account=sales_account, email=settings.SALES_EMAIL)
    except:
        log("send_unused_account_reminders_all: account %s doesn't exist!" % settings.SALES_ACCOUNT)
        return

    # Find out who we're sending promotional emails to
    aliases_to_contact = []             # List of tuples: (alias, contact) where 'contact' is the contact in the cc account

    aliases = Alias.objects.active().exclude(beta_terms_date=None)\
        .filter(is_admin=True)\
        .filter(last_login_date__lt=now()-HOWLONG_REMINDER_UNUSED)\
        .filter(last_unused_account_reminder_date__lt=now()-HOWLONG_REMINDER_UNUSED)

    aliases = uniqify_by_email(aliases) # don't send multiple reminders to the same email
    for alias in aliases:
        # Find the corresponding contact in the "cc" account --
        #  if not subscribed, or doesn't exist in cc, then don't send it!
        #  if no recent blog posts to be sent to that contact, then don't send it!
        sales_contacts = Contact.objects.filter(owner_account=sales_account, email=alias.email, subscribed=True)
        if sales_contacts.count() > 0:
            sales_contact = sales_contacts[0]
            # Are there any blog posts to send to this person?
            blog_posts = Post.objects.published().filter(created__gte=alias.last_login_date)
            if alias.last_unused_account_reminder_date:
                blog_posts = blog_posts.filter(created__gte=alias.last_unused_account_reminder_date)
            if blog_posts.count() > 0:
                aliases_to_contact.append((alias, sales_contact))

    if len(aliases_to_contact) > 0:

        c = Context({'site': Site.objects.get_current()})
        subject = Template(blog_posts[0].title).render(c)

        from crawl.procmail import procmail_fakeevent

        newsfeed_email = procmail_fakeevent(\
            alias=sales_alias,
            source=Email.SOURCE_FORMMAILCONF,
            message_subtype=EMAIL_SUBTYPE_USER_FORMMAIL_CONFIRM,
            subject=subject,
            body='[recent blog posts follow...]',
            contacts_type=Email2Contact.TYPE_FORMMAIL,
            event_status=None,
            contacts = [x[1] for x in aliases_to_contact],
            send_reminders=False
        )

        for alias, sales_contact in aliases_to_contact:
            send_unused_account_reminder(alias, sales_contact, newsfeed_email, do_it=do_it)


def send_expiration_soon_reminder(alias, do_it=True):
    """
    Send emails to one trial account which is about to expire, encouraging them to upgrade
    """
    log("send_expiration_soon_reminder: account=", alias.account, "alias=", alias)

    from basic.blog.models import Post
    site = alias.get_site()
    assistant = alias.account.get_assistant()
    blog_posts = Post.objects.published().filter(created__gte=alias.last_login_date).order_by('-created')

    c = Context({\
        'alias': alias,
        'account': alias.account,
        'assistant': assistant,
        'site': site,
        'has_ssl':              site.domain.lower() in settings.SSL_DOMAINS,
        'blog_posts': blog_posts,
        'last_login_date':  format_date(alias.last_login_date, tzinfo=alias.account_user.get_timezone()),
    })

    subject = _("Your trial account at %s expires tomorrow!") % site.name

    if do_it:
        log("... sending expiration-soon reminder for ", alias.account, alias)
        mail.send_email('main_expiration_soon.html', c, subject,
            (assistant.get_full_name(), assistant.email),
            (alias.contact.get_full_name(), alias.email),
            noted={'message_type': EMAIL_TYPE_NOTED,\
                'message_subtype': EMAIL_SUBTYPE_NOTED_EXPIRATION_SOON_REMINDER,
            }
        )
    else:
        log("... not sending expiration soon reminder for %s because do_it is False" % str(alias))


def send_expiration_soon_reminders_all(do_it=True):
    """
    Send emails to one trial account which is about to expire, encouraging them to upgrade
    """
    log("send_expiration_soon_reminders_all start")
    n = now()
    aliases = Alias.objects.active().filter(is_admin=True)\
        .filter(account__trial_period=True)\
        .filter(account__date_expires__gt=n, account__date_expires__lte=n+ONE_DAY)\
        .exclude(beta_terms_date=None)

    # Filter on trial accounts for which we have no Paypal information
    for alias in aliases:
        if Payment.objects.filter(account=alias.account).count() == 0:
            send_expiration_soon_reminder(alias, do_it=do_it)



def send_reminder_for_occurrence(item, contact, additional_emails=None, do_it=True):
    """
    Send one reminder email to contact about a scheduled event.
        Item can be:
        - a dictionary returned by the Period class, containing
            'occurrence' and 'class'
        - an Event object
    """

    import constants                # needed for the template below

    try:
        alias = contact.alias       # May or may not be None

        if type(item) == dict:      # A dictionary returned by Period class
            occurrence, overlap = item['occurrence'], item['class']
            event = occurrence.event
            start, end = occurrence.start, occurrence.end
        elif type(item) == Event:
            overlap = None
            event = item
            start, end = event.start, event.end
        else:
            raise NotImplementedError("Unknown reminder item type: %s" % str(type(item)))

        # Lots of paranoia to avoid sending out reminders when we shouldn't

        if (event.source not in Email.ALLOWED_SOURCES_FOR_REMINDERS):
            log ("send_reminder_for_occurrence: skipping event", event, "because not from one of the allowed sources for reminders")
            return

        if event.guessed:
            log ("send_reminder_for_occurrence: event is a guessed event, skipping", event)
            return

        if not event.send_reminders:
            log ("send_reminder_for_occurrence: send_reminders = False for this event, skipping", event)
            return

        owner = event.get_owner()
        creator = event.get_creator()

        if not owner or (not owner.alias.account) or (owner.alias.account.status != ACCOUNT_STATUS_ACTIVE):
            log ("send_reminder_for_occurrence: event missing active owner, skipping", event)
            return

        if (not owner.alias.account.assistant_emails_anytime):
            log ("send_reminder_for_occurrence: skipping event", event, "because assistant_emails_anytime is disabled for owner")
            return

        assistant = event.owner_account.get_assistant()
        if not assistant:
            log ("send_reminder_for_occurrence: event missing assistant, skipping", event)
            return

        try:
            assistant_account = assistant.owner_account
            if (assistant_account.status != ACCOUNT_STATUS_ACTIVE):
                log("send_reminder_for_occurrence: event missing active assistant, skipping", event)
                return

            if (not assistant_account.assistant_emails_anytime):
                log ("send_reminder_for_occurrence: skipping event", event, "because assistant_emails_anytime is disabled for the assistant's account")
                return
        except:
            log ("send_reminder_for_occurrence: skipping event", event, "because assistant doesn't belong to an account")
            return

        # Don't check whether alias belongs to active account; outsiders can get reminders too
        if alias and not (alias.account.assistant_emails_anytime):
            log ("send_reminder_for_occurrence: skipping event", event, "because assistant_emails_anytime is disabled for alias")
            return

        attendees = uniqify(event.original_email.contacts.all())
        attendees.sort(key=lambda c: c.get_tag_or_name_for_display(contact))
        other_attendees = uniqify([c for c in (attendees + [creator]) if c != contact])
        other_attendee_names = [c.get_tag_or_name_for_display(contact) for c in other_attendees]
        other_attendee_list = ', '.join(other_attendee_names)

        display_tz = False

        if alias and alias.account_user:
            user_tz = alias.account_user.get_timezone()
            display_tz=(alias.account_user.timezone != owner.alias.account_user.timezone)
        else:
            user_tz = owner.alias.account_user.get_timezone()
            display_tz=True

        local_start = start.astimezone(user_tz)

        if alias and alias.account.get_assistant() == assistant:
            self_assistant = True           # Assistant is emailing one of its own users
            template_file = 'main_remind_self.html'
        else:
            if event.from_external_ics:
                # This check is important -- for external_ics events, don't send reminders outside the team
                log ("send_reminder_for_occurrence: skipping event", event, "because it is from_external_ics and is sending outside its own team [1]")
                return
            if not owner.alias or not owner.alias.account.assistant_emails_others:
                log ("send_reminder_for_occurrence: skipping event", event)
                return
            self_assistant = False          # Assistant is emailing an outsider
            display_tz=True
            template_file = 'main_remind_friend.html'

        # Figure out the subject line
        if alias and alias.account.get_assistant() == assistant:
            subject_prefix = _("[Reminder] ")
        else:
            subject_prefix = _("Just a reminder... ")
        subject = subject_prefix + ' ' +\
            ellipsize(event.title_long) + ' ' +\
            ((_('with') + ' ' + other_attendee_list) if other_attendee_list else '') +\
            ' (' + format_time(local_start) + ')'

        old_emails = mail.find_thread_for_formatting(event.original_email, additional_emails=additional_emails)

        #date_span = format_span(event, display_future_weekdays=True, \
        #    display_today=True, display_tomorrow=True, display_upper=False,
        #    display_both=True, display_sometime=True, display_tz=display_tz, tzinfo=user_tz)
        date_span = format_span(event, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=tzinfo, display_date=True)


        c = Context({
            'full_name':            contact.get_display_name(),
            'constants':            constants,
            'status':               event.status,
            'reminder':             event.title_long,
            'datetime':             format_datetime(local_start, display_today=True, display_past_weekdays=True, display_tz=display_tz, tzinfo=user_tz),
            'overlap':              overlap,
            'date_span':            date_span,
            'day_of_week':          calendar.day_name[local_start.weekday()],
            'date':                 '%d/%d' % (local_start.month, local_start.day),
            'time':                 format_time(local_start, display_tz=display_tz, tzinfo=user_tz),
            'assistant_first_name': assistant.first_name,
            'assistant_email':      assistant.email,
            'assistant_footer':     '',                         # TODO: use a footer
            'self_assistant':       self_assistant,
            'phone':                event.phone,
            'attendees':            attendees,
            'creator':              creator,
            'owner':                owner,
            'old_emails':           old_emails,
            'old_emails_length':    len(old_emails),
            'tzinfo':               user_tz,
            'site':                 alias.get_site() if alias else Site.objects.get_current(),
            'contact':              contact,
            'alias':                alias
        })

        tag_or_company = '' # TODO: add a tag to the subject line, like "Reminder: meeting at 2pm -->(Bazaarvoice)", in this case (Bazaarvoice) is the tag.

        extra_headers = {}

        if contact == creator:
            if other_attendees:
                reply_to = other_attendees[0].get_full_name(), other_attendees[0].email
            else:
                reply_to = None
        else:
            reply_to = creator.get_full_name(), creator.email

        if do_it:
            mail.send_email(template_file, c, subject,
                (assistant.get_full_name(), assistant.email),
                (contact.get_full_name(), contact.email),
                extra_headers = extra_headers,
                reply_to = reply_to,
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_REMINDER,
                    'owner_account': assistant and assistant.owner_account,
                    'event': event},
    #            ics=[ics.ics_from_events(event, to=alias)]
                ics = [],                    # Don't send .ics files after the first one (for now)
                alias=(owner.alias if contact != owner else None)
            )
        else:
            log("send_reminder_for_occurrence: not sending to %s because do_it=False", alias.email)

        # TODO: Per-alias last reminder sent
#        event.last_reminder_sent = now()
#        event.save()
    except:
        log(traceback.format_exc())


def send_reminders_for_occurrence_item(item, additional_emails=None, do_it=True):
    """
    For a scheduled event, send reminders to all the people involved in the event
    """

    event = item['occurrence'].event

    contacts = event.original_email.contacts.filter(\
        email2contact__type__in = Email2Contact.ALLOWED_TYPES_FOR_REMINDERS,
        email2contact__active=True)

    # For TENTATIVE events, only send reminders to the event owner
    if event.status == EVENT_STATUS_TENTATIVE:
        contacts = contacts.filter(pk=event.original_email.owner_alias.contact.pk)

    for c in contacts:
        send_reminder_for_occurrence(item, c, additional_emails=additional_emails, do_it=do_it)

    if do_it:
        event.last_reminder_sent=now()
        event.save()


def send_reminders_for_account(account, lookahead=HOWLONG_REMINDER_LAST_MINUTE, howlong_reminder=HOWLONG_REMINDER_WAIT, do_it=True):
    """
    Send all reminders for scheduled events that are due to be sent for the given account
    """
    log("send_reminders_for_account: account=", account.base_url)

    # Paranoia
    if not account.assistant_emails_anytime:
        log ("send_reminders_for_account: skipping account", account.base_url, "because assistant_emails_anytime is disabled")
        return

    # Scheduled events

    n = now()
    events = Event.objects.scheduled()
    events = events.filter(owner_account=account)
    events = events.filter(source__in=Email.ALLOWED_SOURCES_FOR_REMINDERS)
    events = events.filter(start__gt=n, start__lte=n+lookahead) # Disable this line when we start doing periodic events again (see below)
    events = events.filter(send_reminders=True)                 # IMPORTANT!
    events = events.filter(Q(last_reminder_sent=None) | Q(last_reminder_sent__lt=n-howlong_reminder))
    events = events.distinct()

    # Find the periods (this essentially does nothing until we re-enable periodic events)
    period = Period(events, n, n+lookahead)
    occurrence_dicts = period.get_occurrence_partials(started=True)

    for item in occurrence_dicts:
        send_reminders_for_occurrence_item(item, do_it=do_it)

    # Followup reminders

    events = Event.objects.filter(status=EVENT_STATUS_FOLLOWUP_REMINDER)
    events = events.filter(owner_account=account)
    events = events.filter(start__gt=n-ONE_DAY, start__lte=n+5*ONE_MINUTE)
    events = events.filter(send_reminders=True)                 # IMPORTANT!
    events = events.exclude(from_external_ics=True)             # Probably not needed if filtering on send_reminders=True, but paranoia
    events = events.filter(last_reminder_sent=None)
    events = events.distinct()

    # Find the periods (this essentially does nothing until we re-enable periodic events)
    period = Period(events, n-ONE_DAY, n+5*ONE_MINUTE)
    occurrence_dicts = period.get_occurrence_partials(started=True)

    for item in occurrence_dicts:
        additional_emails = None
        event = item['occurrence'].event
        additional_emails = []
        contacts = event.original_email.contacts.filter(email2contact__type=Email2Contact.TYPE_REF)
        for c in contacts:
            last_email = Email.objects.filter(owner_alias=event.original_email.owner_alias)\
                .exclude(message_type=EMAIL_TYPE_NOTED)\
                .filter(source__in=[Email.SOURCE_CRAWLED, Email.SOURCE_API, Email.SOURCE_QUICKBOX, Email.SOURCE_TWITTER, Email.SOURCE_DIRECT])\
                .filter(contacts=event.original_email.owner_alias.contact)\
                .filter(contacts__email2contact__contact=c, contacts__email2contact__type__in=[Email2Contact.TYPE_FROM, Email2Contact.TYPE_TO, Email2Contact.TYPE_CC])\
                .order_by('-date_sent')
            additional_emails.extend(list(last_email.distinct()[:2]))

        send_reminders_for_occurrence_item(item, additional_emails=additional_emails, do_it=do_it)




def send_reminders_all_last_minute(lookahead=HOWLONG_REMINDER_LAST_MINUTE, do_it=True):
    """
    Send all scheduled event reminders that are due to be sent
    Called every 15 minutes by the cron job
    lookahead = how far in advance of the actual event to send the reminder
    do_it - set to False for debugging (a dry run that doesn't actually send reminders)
    """
    log("reminder.send_reminders_all_last_minute...")
    for account in Account.objects.active().filter(assistant_emails_anytime=True):
        send_reminders_for_account(account, lookahead=lookahead, do_it=do_it)


def mark_old_faded_events_completed():
    '''for the nightly cron job'''
    queryset = Event.objects.filter(status__in=[EVENT_STATUS_SCHEDULED_FADING, EVENT_STATUS_TENTATIVE_FADING], last_updated__lte=now()-ONE_DAY)
    for event in queryset:
        event.status = EVENT_STATUS_COMPLETED
        event.last_updated = now()
        event.save()


def send_event_response_request(er, do_it=True):
    '''Send out a single EventResponse request email'''

    log("send_event_response_request:", er.pk)

    try:
        # Some extra paranoia
        if not (er.event and er.event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE]):
            log ("send_event_response_request: skipping event", event, "because it's not a scheduled event")
            return

        if er.event.end < now():
            log ("send_event_response_request: skipping event", event, "because event has already passed")
            return

        event = er.event
        owner = event.get_owner()
        creator = event.get_creator()

        if (event.source not in Email.ALLOWED_SOURCES_FOR_REMINDERS):
            log ("send_event_response_request: skipping event", event, "because not from one of the allowed sources for reminders")
            return

        # Some more paranoia
        if er.event and er.contact in [creator, owner]:
            log ("send_event_response_request: skipping event", event, "because EventResponse is for creator or owner")
            return

        contact = er.contact
        assistant = er.assistant
        if er.request_email:            # event created via email
            subject = er.request_email.subject_normalized
            old_contact = er.request_email.from_contact
            noted = {'message_type': er.request_email.message_type,
                'message_subtype': er.request_email.message_subtype,
                'event': er.event,
                'owner_account': assistant and assistant.owner_account,
            }
        else:
            return False

        c = Context({\
            'contact':                  contact,
            'alias':                    contact.alias,
            'old_emails':               [er.request_email],
            'assistant':                assistant,
            'site':                     contact.alias.get_site(),
            'old_contact':              old_contact
        })

        if do_it:
            mail.send_email('main_ask_response.html', c,
                    # _('[Response Needed] Re: ') + subject,
                    _('Re: ') + mail.strip_subject_reply_or_forward(subject),
                    (assistant.get_full_name(), assistant.email),
                    (contact.get_full_name(), contact.email),
                    reply_to=(owner.get_full_name(), owner.email),
                    noted=noted,
                    alias=owner.alias
                )
        else:
            log("send_event_response_request: skipping mail to %s because do_it=False" % alias.email)

        er.last_request_sent = now()
        if er.num_requests_sent is None:
            er.num_requests_sent = 1
        else:
            num_requests_sent = er.num_requests_sent
            er.num_requests_sent = num_requests_sent + 1
        er.save()
        return True
    except:
        log(traceback.format_exc())
        log("Warning: could not send event response %d" % er.pk)
        return False


def send_event_response_requests_all(force_send=False, do_it=True):
    '''Ask slackers to respond to our events'''
    log("send_event_response_requests_all...")
    for er in EventResponse.objects.filter(status=EVENT_RESPONSE_STATUS_AWAITING):
        # EXPIRATION
        if er.contact and er.contact.alias and er.contact.alias.account and (er.contact.alias.account.status == ACCOUNT_STATUS_ACTIVE):
            if force_send or er.should_send_request():
                if do_it:
                    log("reminder.send_event_response_requests_all: actually sending eventresponse to %s" % str(er.contact))
                    send_event_response_request(er)
                else:
                    log("reminder.send_event_response_requests_all: not sending eventresponse to %s" % str(er.contact))
        # TODO: Should we send reminders to people who aren't users?

def find_events_of_each_type(email):
    result = {}

    if not email:
        return result

    for event in email.events.all():
        if event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE]:
            if 'event_scheduled' not in result:
                result['event_scheduled'] = event
        elif event.status == EVENT_STATUS_CANCELED:
            if 'event_scheduled' not in result:
                result['event_scheduled'] = event

    return result

def procmail_reminder(email, extras, content, quoted_headers, quoted_content, files, ics_files, from_contact, mode, debug=False):
    """
    The main routine for handling messages that aren't crawled (quickbox messages, Twitter messages,
    messages emailed directly to the assistant)

    This is mostly legacy code, eventually we might want to replace this with less complex
    code that uses a message_dict structure like for the crawler.
    """

    log ("procmail_reminder start...")

    if debug: pdb.set_trace()       # EXCUSE
    stats = mode['stats']
    account = mode['account']
    assistant = account.get_assistant()

    stats.start('procmail_reminder')

    # Prefer the earlier entries in the list mode['parent_emails'], since these are the more recent mails
    parent_event_dict = find_events_of_each_type(mode['original_parent_email'])
    if 'event_scheduled' in parent_event_dict:  mode['parent_event_scheduled'] = parent_event_dict['event_scheduled']

    if mode['original_parent_email']:
        parent_email = mode['original_parent_email']
        for event in parent_email.events.all():
            if event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE]:
                if 'parent_event_scheduled' not in mode:
                    mode['parent_event_scheduled'] = event
            elif event.status == EVENT_STATUS_CANCELED:
                if 'parent_event_scheduled' not in mode:
                    mode['parent_event_scheduled'] = event

    processed, forwarded, private = False, False, True  # Assume we couldn't process this email,
                                                        #   until we find out otherwise
    n = now()
    content = content[:MAX_PROCMAIL_REMINDER_LENGTH]

    try:
        log('procmail_reminder')
        result = False
        if from_contact and from_contact.alias and from_contact.alias.account_user: # TODO: allow Contacts to have timezones
            user_tz = from_contact.alias.account_user.get_timezone()
        elif (email.source != Email.SOURCE_TWITTER) and email.timezone:
            user_tz = email.get_timezone()
        elif email.owner_alias:
            user_tz = email.owner_alias.account_user.get_timezone()
        elif email.crawled_alias:
            user_tz = email.crawled_alias.account_user.get_timezone()
        else:
            user_tz = timezone_from_timezonestr(settings.TIME_ZONE)
        now_date = email.date_sent.astimezone(user_tz)

        attendees = uniqify([ec.contact for ec in Email2Contact.objects.filter(email=email, type__in=[Email2Contact.TYPE_TO, Email2Contact.TYPE_CC])])

        if email and (email.source not in [Email.SOURCE_QUICKBOX, Email.SOURCE_TWITTER]):
#            subject = mail.strip_subject_reply_or_forward(email.subject_normalized)
            # If this is a reply to another email, only count the subject line if it's changed since then
            if mail.is_subject_reply(email.subject_normalized):
                subject = ''
            else:
                subject = mail.strip_subject_reply_or_forward(email.subject_normalized)

#            if mode['parent_emails']:
#                if all(subject == mail.strip_subject_reply_or_forward(parent_email.subject_normalized) for parent_email in mode['parent_emails']):
#                    subject = ''
#            else:
#                subject = mail.strip_subject_reply_or_forward(email.subject_normalized)
        else:
            subject = content                   # One-liner
            content = ''

        debug_content = []

        log('procmail_reminder / subject: %s' % subject)

        # Eliminate this feature, now that we have guessed events and scanning,
        # it's more trouble than it's worth [eg 5/29/09]
#        if quoted_content:
#            if mail.is_subject_forward(email.subject_normalized) or not email.subject_normalized.strip():
#                if not content.strip() or len(quoted_content.strip()) / len(content.strip()) > 4:
#                    content += quoted_content

        subj_and_content = '\n'.join([subject, content]).strip()
        if not subj_and_content and not ics_files and not files:  # Nothing to process, just return
            return True, False                  #  (but say we processed it, so that procmail_email doesn't do anything further)

        log('procmail_reminder / chronic [start]')

        stats.start('chronic')
        items, tags, subject_overall, body_overall, more_debug_content = extract_items_from_content(subject, content, now_date, mode)
        stats.end('chronic')

        log('procmail_reminder / chronic [end]')

        tags = uniqify(tags)
        if not mode['is_guest'] and mode['account']:
            network_tag = mode['account'].base_url.lower()
        else:
            network_tag = None

        # Extract the tags, classify them
        tags = [t for t in tags if t==network_tag or (not re.match(r'^\d{1,2}$', t))]

        log('procmail_reminder / tags [%s]' % tags)

        classified_tags = {}
        item_attendees = attendees
        if (not mode['is_guest']) and (email.source != Email.SOURCE_TWITTER):
            for t in tags:
                # For everything that looks like a tag, figure out what to do with it...
                tag_tuple = classify_tag(t, attendees, mode)
                if tag_tuple is not None:
                    tag, type, other_contacts, parent_event, delete_tag = classify_tag(t, attendees, mode)
                    # Not using this parent_event stuff now, we are strictly using in-reply-to fields
        #            if parent_event:
        #                mode['parent_event'] = parent_event
        #                mode['parent_emails'].append(parent_event.original_email)
        #                mode['is_reply'] = True
        #                continue
                    item_attendees += other_contacts
                    classified_tags[tag] = (tag, type, other_contacts)


        item_attendees = uniqify_by_email(item_attendees)
        item_attendees = [other_contact for other_contact in item_attendees if other_contact != from_contact]


        log('procmail_reminder / item_attendees [%s]' % item_attendees)

        # Process any embedded commands
        if mode['source'] in [Email.SOURCE_QUICKBOX, Email.SOURCE_DIRECT] and not mode['is_guest'] and from_contact.alias:

            # One of the few places left in reminder.py where we still use alias :)
            alias = from_contact.alias

            m = re.search(r'(?mi)^\s*(invite)((\s|[,;.:])*(?P<email>[A-Za-z0-9._%+-]+@([A-Za-z0-9.-]+\.)+[A-Za-z]+))+', subj_and_content)
            if m:
                if alias.is_admin:
                    subphrase = subj_and_content[m.start():m.end()]
                    emails = re.findall(r"\b([A-Za-z0-9._%+-]+@([A-Za-z0-9.-]+\.)+[A-Za-z]+)\b", subphrase)
                    emails = [x[0] for x in emails]     # get first group of each returned string
                    register.add_new_aliases(emails, alias.account, timezone=alias.account_user.timezone, mode=mode, admin_alias=alias)
                    processed = True
                    log('procmail_reminder / processed invite [%s]' % emails)
        # Create any events needed (for now, can't do this in same email as commands)
        if (not processed):

            log('procmail_reminder / NOT process')

            # If this is a reply, people who might want to get notifications
            reply_contacts = []
            reply_mode = mode.get('reply_mode', 'default')
            if mode['is_reply']:
                if mode['original_parent_email']:
                    parent_email = mode['original_parent_email']
                    if parent_email:
                        reply_contacts.extend(parent_email.get_reply_contacts(reply_mode, contact=from_contact))

            # Don't reply to ourselves via email (but do over Twitter)
            if not (mode['original_parent_email'] and mode['original_parent_email'].is_twitter()):
                reply_contacts = [other_contact for other_contact in reply_contacts if other_contact != from_contact]

            if 'private' in classified_tags:
                del classified_tags['private']         # Don't store @private as a tag, we know it's private from e.private

            n = now()

            # Create the different kinds of events that appear in this email
            # Note: The order of the below code blocks matters!
            events = []
            event, parent_event = None, None
            mode['event_index'] = 0     # Increment this for each event created -- used for creating event UIDs

            # Process any .ics files
            ics_itemlists = [ics.items_from_ics_file(ics_file, user_tz, owner_alias=email.owner_alias) for ics_file in ics_files]
            for ics_itemlist in ics_itemlists:

                # handle multiple events in an .ics file
                for item, attendee_tuples in ics_itemlist:
                    event, parent_event = process_ics(from_contact, assistant, item, attendee_tuples, reply_contacts, private,\
                                classified_tags, email, user_tz, mode,
                                tagonly_params=(item_attendees, reply_contacts, subject_overall, body_overall, subject, extras))
                    if event:
                        events.append(event)

            # Process any reminder items (only if not crawled_alias and no .ics files)
            if from_contact and (not events) and (not ics_files) and \
                (not mail.is_subject_event_response(email.subject_normalized)) and \
                (not mail.is_subject_forward(email.subject_normalized)):
                for item in items[0:1]:     # For now, just process the first item found
#                    if (mode['source'] != Email.SOURCE_CRAWLED) or \
#                        ((mode['source'] == Email.SOURCE_CRAWLED) and item.get('at_symbol', False)):
                    guessed = ((not item.get('at_symbol', False)) and mode.get('require_at_symbol', True)) or mode['is_guest']
                    event, parent_event = process_scheduled(from_contact, attendees, assistant, item, reply_contacts, private,\
                        classified_tags, email, user_tz, mode, guessed=guessed,
                        tagonly_params=(item_attendees, reply_contacts, subject_overall, body_overall, subject, extras))
                    if event:
                        events.append(event)

            # Save the snippet
            if subject_overall['tags'] or not body_overall['summary']:
                summary, more = subject, False
            else:
                summary = ' '.join([word for word in (subject_overall['summary'] + ' ' + body_overall['summary']).split()])
                summary, more = ellipsize_and_more(summary, length_min=SUMMARY_TAGONLY_ELLIPSIZE_MIN, length_max=SUMMARY_TAGONLY_ELLIPSIZE_MAX)
            email.summary = summary

            if not email.message_type:
                email.message_type = EMAIL_TYPE_USER
            if not email.message_subtype:
                email.message_subtype = EMAIL_SUBTYPE_USER_TAGONLY

            email.save()

            # Add the cross-reference info, if any
            if email.owner_alias:
                if 'ref_bucket_slug' in mode:
                    buckets = Bucket.objects.filter(slug=mode['ref_bucket_slug'], account=email.owner_account)
                else:
                    buckets = []
                if 'ref_contact_id' in mode:
                    contacts = Contact.objects.filter(id=mode['ref_contact_id'], owner_account=mode['account'])
                    for c in contacts:
                        for b in buckets:
                            c.contact_buckets.add(b)
                        email.create_email2contact_by_contact(c, mode['account'], Email2Contact.TYPE_REF, viewing_alias=email.owner_alias)


            processed = True        # For now, every email going through procmail_register is ALWAYS processed,
                                    #  even if there's nothing interesting in it

            for event in events:
                email.events.add(event)

            if not mode['is_guest']:
                root = email.find_thread_root()
                if root and (root != email) and Email2Contact.objects.filter(email=root, contact=from_contact, type=Email2Contact.TYPE_ASSIGNED).count() > 0:
                    root.completed = True
                    root.save()

            # TODO: Get rid of this garbage when we eliminate event threading

            # Link this email's events to the childrens' events
            if events:
                event = events[0]
                for child_email in mode['child_emails']:
                    child_events = child_email.events.all()
                    for child_event in child_events:
                        if not child_event.parent:
                            if private:
                                child_event.perms = EVENT_PERMS_PRIVATE
                        child_event.parent = events[0]
                        child_event.activated = max(event.activated, child_event.activated)
                        event.activated = child_event.activated
                        event.newsfeed_thread_date = max(event.newsfeed_thread_date, child_event.newsfeed_thread_date)
                        child_event.save()

            # Link this email's events to the parents' events
            for event in events:
                if parent_event and parent_event != event:
                    event.parent = parent_event
                    event.save()

                    parent_event.newsfeed_thread_date = max(parent_event.newsfeed_thread_date, event.newsfeed_thread_date)
                    parent_event.activated = max(parent_event.activated, event.activated)
                    parent_event.save()



        stats.end('procmail_reminder')

#        if email:
#            mail.send_debug_mail(email.from_address, '\n'.join(debug_content))

        log(debug_content)

    except:
        log(traceback.format_exc())

    return (processed or forwarded, private)


def classify_tag(tag, attendees, mode):
    account = mode['account']

    # First, find tag among names of attendees
    word = tag.lower()

    # All returns everyone...
    if account and (word == 'all'):
        all = Contact.objects.active().filter(owner_account=account).exclude(alias=None)
        return (tag, TAG_TYPE_PERSON, list(all), None, False)

    # Try to find tag among the attendees first
    for c in attendees:
        if c.alias:
            if c.alias.matches_tag(tag, ez_name_only=True):
                return (tag, TAG_TYPE_PERSON, [c], None, False)

    # Try to find tag within the account
    if account:
        a = account.find_alias_by_tag(tag)
        if a:
            return (tag, TAG_TYPE_PERSON, [a.contact], None, False)

    # Try to find tag within the list of buckets
    if Bucket.objects.filter(account=account, name=word).count() > 0:
        return (tag, TAG_TYPE_BUCKET, [], None, False)

    # Don't create a tag
    return None


def find_conflicts_for_event(contact, event):
    # Find any other meetings that the alias is attending which may conflict with a proposed meeting
    all_events = Event.objects.scheduled().filter(original_email__contacts=contact).distinct()      # TODO: Only active contacts
    period = Period(all_events, event.start, event.end)
    conflicts = period.get_occurrence_partials()
    return [c for c in conflicts if c['occurrence'].event != event]


def generic_event(from_contact, email, status, summary, more, private, mode):
    event = Event()
    n = now()

    event.owner_account = mode['account']
    event.status = status
    event.original_status = status
    event.title_long = summary
    event.created_on = n
    event.last_updated = n
    event.start = email.date_sent or n
    event.end = email.date_sent or n
    event.original_email = email
    event.newsfeed_date = email.date_sent or n
    event.newsfeed_thread_date = email.date_sent or n
    event.source = email.source
    event.activated = True
    event.guessed = False
    event.modified = False
    event.dtstamp = n
    event.display_more_link = more or mode.get('display_more', False)
    if (mode and mode.get('silent', False)) or (email.source not in Email.ALLOWED_SOURCES_FOR_REMINDERS):
        log("Silent mode -- send_reminders=False [1]")
        event.send_reminders = False

    m = re.search(r"(?i)(\bno\b|nay|won't|doesn't|can't|decline|deklin|rejected|cancel|cannot|bbad\b)", summary)
    if m:
        event.response_type = EVENT_RESPONSE_TYPE_NO
    else:
        m = re.search(\
                      r"(?i)(yes|yeah?|aye|\bsure|\bok\b|okay|okey|\bO\.K\.|fine|works|accept|akzept|certainly|affirmative|check|confirm|" +
                      r"yep\b|yup\b|\bgood(?!\s*morning|afternoon|evening|night|day|weekend)|" +
                      r"\bi\s+will(?!\s*not)|\bi\s+can\b(?!(\'t|\s*not)))", summary)
        if m:
            event.response_type = EVENT_RESPONSE_TYPE_YES
        else:
            event.response_type = EVENT_RESPONSE_TYPE_UNKNOWN

    event.perms = EVENT_PERMS_PRIVATE if private else EVENT_PERMS_PROTECTED
    unique_id = str(mode['account'].base_url) + str(email.message_id) + str(email.date_sent) + str(mode.get('event_index', ''))
    log("generic_event: Creating hash from unique_id ", unique_id)
    event.uid = hashlib.md5(unique_id).hexdigest()+'@'+settings.NOTED_DOMAIN_MAIN
    if Event.objects.filter(uid=event.uid).count() > 0:
        event.uid = hashlib.md5(create_slug(length=20)).hexdigest() + '@' + settings.NOTED_DOMAIN_MAIN
    if 'event_index' in mode:
        mode['event_index'] += 1
    if from_contact.alias and from_contact.alias.account_user:
        event.timezone = from_contact.alias.account_user.timezone
    elif email:
        event.timezone = email.timezone
    else:
        event.timezone = settings.TIME_ZONE
    return event


def create_event_responses(item_attendees, event, email, contact, setup_reminders=False):
    alias = contact.alias
    awaiting = event.source in Email.ALLOWED_SOURCES_FOR_REMINDERS
    result = []

    for c in uniqify(item_attendees + [contact]):
        if (c == contact) or (c in [event.original_email.owner_alias.contact if event.original_email.owner_alias else None, event.original_email.from_contact]):
            er = EventResponse()
            er.slug = create_slug(length=10)
            er.status = EVENT_RESPONSE_STATUS_NONE
            er.event = event
            er.contact = contact
            er.assistant = alias.account.get_assistant()
            er.request_email = email
            er.last_request_sent = now()
            er.save()
        else:
            er = EventResponse()
            er.slug = create_slug(length=10)
            if setup_reminders:
                if awaiting and (1<=len(item_attendees)<=2):
                    er.status = EVENT_RESPONSE_STATUS_AWAITING
                else:
                    er.status = EVENT_RESPONSE_STATUS_NONE
            else:
                er.status = EVENT_RESPONSE_STATUS_NONE
            er.event = event
            er.contact = c
            er.assistant = alias.account.get_assistant()
            er.request_email = email
            er.last_request_sent = now()
            er.save()


def process_ics(from_contact, assistant, item, attendee_tuples, reply_contacts, private,\
                classified_tags, email, user_tz, mode, tagonly_params=None):

    log("process_ics", item)

    # TODO: what if alias is not the organizer of the event?
    # TODO: Detect duplicate events from the calendar

    event, parent_event = None, None

    if item['method'] == 'REQUEST':
        attendees = [attendee for attendee, partstat in attendee_tuples]
        event, parent_event = process_scheduled(from_contact, attendees, assistant, item, reply_contacts, private,\
                classified_tags, email, user_tz, mode, ics_file=item['ics'], guessed=False)
        for c in attendees:
            if Email2Contact.objects.filter(email=email, contact=c).count() == 0:
                email.create_email2contact_by_contact(c, mode['account'], Email2Contact.TYPE_ICS)

    elif item['method'] == 'REPLY':
        response_type = None
        if 'uid' in item:
            queryset = Event.objects.filter(uid=item['uid'])
            if queryset.count() == 1:
                parent_event = queryset.get()
                for attendee, partstat in attendee_tuples:
                    response_type = None
                    if attendee == from_contact:
                        if partstat.upper() == 'ACCEPTED':
                            response_type = EVENT_RESPONSE_TYPE_YES
                        elif partstat.upper() == 'DECLINED':
                            response_type = EVENT_RESPONSE_TYPE_NO
                        elif partstat.upper() == 'TENTATIVE':
                            response_type = EVENT_RESPONSE_TYPE_TENTATIVE
                    if response_type:
                        item_attendees, reply_contacts, subject_overall, body_overall, subject, extras = tagonly_params
                        log("Special call to process_tagonly...")
                        event, tagonly_parent_event = process_tagonly(from_contact, item_attendees, reply_contacts, assistant, subject_overall, body_overall, subject, private, classified_tags, email, extras, user_tz, mode, modify_parent=False, send_forwarded_mail=False)
                        log("special call done")
                        event.response_type = response_type
                        event.save()
                        modify_scheduled_event_with_response(event, parent_event, mode=mode)

    # TODO: Replying to ICS with "proposing a new time" method

    return event, parent_event



def process_tagonly(from_contact, attendees, reply_contacts, assistant, subject_overall, body_overall, subject, private,\
                    classified_tags, email, extras, user_tz, mode, modify_parent=True, send_forwarded_mail=True):

    try:
        log("process_tagonly: tags = ", classified_tags)

        if 'nostore_email' in mode:
            return None, None

        event, parent_event = None, None
        if 'parent_event' in mode:
            parent_event = mode['parent_event']
        if 'parent_event_scheduled' in mode:
            parent_event = mode['parent_event_scheduled']
            # Modify the parent event at this point
        elif 'parent_event_tagonly' in mode:
            parent_event = mode['parent_event_tagonly']

        if subject_overall['tags'] or not body_overall['summary']:
            summary, more = subject, False
        else:
            summary = ' '.join([word for word in (subject_overall['summary'] + ' ' + body_overall['summary']).split()])
            summary, more = ellipsize_and_more(summary, length_min=SUMMARY_TAGONLY_ELLIPSIZE_MIN, length_max=SUMMARY_TAGONLY_ELLIPSIZE_MAX)

        event = generic_event(from_contact, email, EVENT_STATUS_TAGONLY, summary, more, private, mode)

        item_attendees = attendees
        #is_question = (event.display_type == Event.DISPLAY_QUESTION)

#        if ((mode['source'] != Email.SOURCE_CRAWLED) and (not classified_tags)) and (not is_question):
#            classified_tags['note'] = ('note', TAG_TYPE_OBJECT, [])

#        if (mode['source'] == Email.SOURCE_CRAWLED):
#            event.activated = bool(classified_tags or is_question or (parent_event and parent_event.activated))

        if email:
            email.message_type = EMAIL_TYPE_USER
            email.message_subtype = EMAIL_SUBTYPE_USER_TAGONLY
            email.save()

        log("process_tagonly: saving event with uid = ", event.uid)
        event.save()

        # TODO: Factor out this common stuff, too
        #  (Eventually, we replace all the create_relation crap with this
        all_accounts = [mode['account']]

#        all_aliases = uniqify_by_email([alias, email.crawled_alias] + item_attendees + reply_aliases)
#        for a in all_aliases:
#            event.original_all_aliases.add(a)
#            event.all_aliases.add(a)

        # TODO: Factor out this common stuff
        event.save()

        # event.creator = alias
        event.owner_account = mode['account']
        for tagname, tagtype, other_alias in classified_tags.values():
            tag, created = Tag.objects.get_or_create(text=tagname, type=tagtype)
            event.tags.add(tag)
        event.save()

        # Forward mail and set up EventResponses, if needed
        forward_contacts = [f for f in uniqify(item_attendees + reply_contacts) if Email2Contact.objects.filter(email=email, contact=f).count()==0]
        for f in forward_contacts:
            email.create_email2contact_by_contact(f, mode['account'], Email2Contact.TYPE_FWD)

        modified_scheduled_event = False
        if modify_parent and parent_event and parent_event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_CANCELED]:
            # This will add the WMessage
            if modify_scheduled_event_with_response(event, parent_event, send_confirmations=not parent_event.from_external_ics, mode=mode):
                send_forwarded_mail = False
                modified_scheduled_event = True

        log("forward_contacts:", forward_contacts)
        if (not mode['is_bcc']) and (mode['source'] in Email.ALLOWED_SOURCES_FOR_REMINDERS) and not (mode.get('forwarded_mode', None)):
            if (mode.get('public_twitter', False) or forward_contacts) and send_forwarded_mail and email and (email.source_subtype != Email.SOURCE_SUBTYPE_TWITTER_RETWEET):
                noted={'message_type': EMAIL_TYPE_NOTED,\
                       'message_subtype': EMAIL_SUBTYPE_NOTED_FORWARD,
                       'event': event,
                       'owner_account': assistant and assistant.owner_account
                }
                mail.send_forwarded_mail(email, mode['account'].get_assistant(), from_contact, forward_contacts, is_assistant_for_name=not mode['is_guest'], noted=noted, use_smtp_if_possible=True, mode=mode)

        if (parent_event and parent_event.original_email.is_twitter()) or mode.get('public_twitter', False):
            event.display_type = Event.DISPLAY_TWITTER
            event.save()

        if not modified_scheduled_event:
            if forward_contacts:
                wm = _('Noted, and forwarded to %s.') % ', '.join([c.get_tag_or_name_for_display(mode['account'], include_email=True) for c in forward_contacts])
            else:
                wm = _('Noted.')
            mode['wmessages'].append(wm)

        return event, parent_event

    except NotifyUserException, exc:
        mode['wmessages'].append(exc.message)
        mode['alert'] = exc.message     # Display this as a Javascript alert (quickbox only)
        return None, None
    except:
        log("process_tagonly: exception")
        log(traceback.format_exc())
        return None, None


def process_scheduled(from_contact, attendees, assistant, item, reply_contacts, private,\
                          classified_tags, email, user_tz, mode, ics_file=None, guessed=False, tagonly_params=None):
    try:
        log("process_scheduled: tags = ", classified_tags)

        if ('summary' not in item) or ('span' not in item):
            return None, None
        if item['span'] is None:
            return None, None

        event, parent_event, created, modified = None, None, False, False

        # Attendees may be of type Contact, or a tuple (partstat, alias) where partstat is a string from an .ics file
        #  like ACCEPTED, DECLINED or TENTATIVE
        item_attendees = [(c[0] if type(c) is tuple else c) for c in attendees]
        if mode['reply_mode'] == 'email':
            pass
        else:
            item_attendees = uniqify(item_attendees + reply_contacts)
        item_attendees = [other_contact for other_contact in item_attendees if other_contact != from_contact]

        if 'parent_event' in mode:
            parent_event = mode['parent_event']
        if 'parent_event_scheduled' in mode:
            parent_event = mode['parent_event_scheduled']
            # Modify the parent event at this point

        summary = item.get('summary', '')
        m = re.search(r"(?i)(decline|reject|cancel)", email.subject_normalized)
        if m:
            return None, None

        # Get or create event
        uid = None
        if 'uid' in item:
            uid = item['uid']
            q_event = Event.objects.filter(uid=uid)
            if q_event.count() >= 1:
                event, created = q_event[0], False

        if event is None:
            created = True

            # Point events should get some default length
            span = item['span']
            if span.width() == timedelta(0):
                span.end = span.start + HOWLONG_MEETING_DEFAULT


#            # Hack to prevent words like "today" from creating a reminder item if the day's almost gone
#            # In this case, just create a "tag only" event
#            if span.width() >= ONE_DAY and span.end - now() < 3*ONE_HOUR and tags:
#                return None

            if span.end < now():                # Don't create events in the past --
                return None, None               #  let this become a tag-only event instead

            if guessed:
                item_attendees, reply_contact, subject_overall, body_overall, subject, extras = tagonly_params
                event, parent_event = process_tagonly(from_contact, item_attendees, reply_contacts, assistant, subject_overall, body_overall, subject, private, \
                    classified_tags, email, extras, user_tz, mode, modify_parent=False)
                event.guessed, created = True, True
            else:
                more = False            # TODO: Fix this when we clean up all the event summary garbage!
                event, created = generic_event(from_contact, email, EVENT_STATUS_SCHEDULED, summary or _("Todo"), more, private, mode), True
            if 'uid' in item:
                event.uid = uid
            if 'dtstamp' in item:
                event.dtstamp = item['dtstamp']
            event.from_external_ics = bool(ics_file)
            event.ics = ics_file

            event.start = span.start
            event.end = span.end
            rule = span.get_rule()
            if rule:
                rule.save()
                event.rule = rule

            if 'phone' in item:
                event.phone = item['phone']

            if not guessed:
                tentative = item['tentative']
                if tentative:
                    event.status = EVENT_STATUS_TENTATIVE
                else:
                    event.status = EVENT_STATUS_SCHEDULED

            event.original_status = event.status

            for c in item_attendees:
                if c not in email.contacts.all():
                    email.create_email2contact_by_contact(c, mode['account'], Email2Contact.TYPE_FWD)

            # If replying to an existing scheduled event with another event, cancel the parent event
            if parent_event and parent_event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE]:
                if (parent_event.start == event.start) and (parent_event.end == event.end):
                    event = None
                    return event, parent_event
                parent_event.status = EVENT_STATUS_CANCELED
                parent_event.save()

            event.save()            # Do this so we can create relations

        email.message_type = EMAIL_TYPE_USER
        email.message_subtype = EMAIL_SUBTYPE_USER_CREATE_REMINDER
        email.save()

        # Send confirmations -- questions...
        if created and (not modified) and (not ics_file) and (not guessed) and not mode['is_guest']:
            if mode.get('silent', False):
                log("Not sending event confirmations due to silent mode")
            else:
                mode['wmessages'].append(_('Added new event to your calendar.'))

                log("calling send_event_confirmations [1]")
                send_event_confirmations(event, email, assistant,\
                    send_conflicts=True, include_attendees=not mode['is_bcc'], attach_ics=True, mode=mode)

                if event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE] and \
                    not modified and not mode['is_bcc'] and not event.from_external_ics:
                        setup_reminders = (event.status == EVENT_STATUS_TENTATIVE) # and (mode['source'] != Email.SOURCE_CRAWLED)
                        create_event_responses(item_attendees, event, email, from_contact, setup_reminders=setup_reminders)

        # TODO: Factor out this common code
        # for scheduled events, this applies to Twitter replies only!
        forward_contacts = list(email.contacts.filter(email2contact__type=Email2Contact.TYPE_FWD))

        log("forward_contacts:", forward_contacts)
        if (not mode['is_bcc']) and (mode['source'] in Email.ALLOWED_SOURCES_FOR_REMINDERS) and parent_event and parent_event.original_email and parent_event.original_email.is_twitter() and (email.source_subtype != Email.SOURCE_SUBTYPE_TWITTER_RETWEET):
            if forward_contacts:
                if email:
                    noted={'message_type': EMAIL_TYPE_NOTED,\
                           'message_subtype': EMAIL_SUBTYPE_NOTED_FORWARD,
                           'event': event,
                           'owner_account': assistant and assistant.owner_account
                    }
                    mail.send_forwarded_mail(email, mode['account'].get_assistant(), from_contact, forward_contacts, is_assistant_for_name=not mode['is_guest'], noted=noted, use_smtp_if_possible=True, mode=mode)

#        TODO: What about Twitter replies?
#        if parent_event and parent_event.original_email.is_twitter():
#            email.type = Email.TYPE_TWITTER

        return event, parent_event

    except:
        log("process_scheduled: exception")
        log(traceback.format_exc())

        return None, None


def send_event_confirmations(e, email, assistant, send_conflicts=False, include_attendees=False, additional_contacts=None, additional_emails=None, attach_ics=True, mode=None):

    if e.from_external_ics:
        return

    # No confirmations for expired events
    if e.end and (e.end <= now()):
        return

    if e.source not in Email.ALLOWED_SOURCES_FOR_REMINDERS:
        log ("send_event_confirmations: event is of source", e.source, "skipping")
        return

    if e.status not in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_CANCELED]:
        raise Exception("send_event_confirmations: %d must be scheduled/tentative event" % e.pk)

    if e.guessed:
        log ("send_event_confirmations: event is guessed", e, "skipping")
        return

    # TODO: Use conflicts to determine tentative / scheduled?

    result_contacts = []

    contact = e.original_email.owner_alias.contact
    if contact:
        if not contact.alias.send_confirmations:
            log ("send_event_confirmations: creator alias", contact.alias, "has send_confirmations=False, skipping")
            return

        if send_conflicts:
            contact_conflicts = find_conflicts_for_event(contact, e)
        else:
            contact_conflicts = []
        if send_self_confirmation(contact, e, email, assistant, conflicts=contact_conflicts, attach_ics=attach_ics, additional_emails=additional_emails):
            result_contacts.append(contact)

    if include_attendees:
        if additional_contacts is None:
            additional_contacts = []
        item_attendees = list(email.contacts.all()) + additional_contacts
        item_attendees = uniqify([c for c in item_attendees if c != contact])
        for attendee in item_attendees:
            if send_conflicts:
                attendee_conflicts = find_conflicts_for_event(attendee, e)
            else:
                attendee_conflicts = []
            if send_attendee_confirmation(contact, e, email, assistant, attendee, conflicts=attendee_conflicts, attach_ics=attach_ics, additional_emails=additional_emails, mode=mode):
                result_contacts.append(attendee)

    if result_contacts and mode:
        mode['wmessages'].append(_("Confirmation mail sent to %s.") % ', '.join([c.email for c in result_contacts]))

    e.last_reminder_sent = now()
    e.save()


def modify_scheduled_event_with_response(child_event, parent_event, send_confirmations=True, mode=None):
    log ("modify_scheduled_event_with_response")

    conf_emails_sent = False        # Flag we'll return, tells whether or not we actually sent out any confirmation emails

    contact = child_event.get_creator()
    if not contact:
        return

    response_type = child_event.response_type

    r, created = EventResponse.objects.get_or_create(event=parent_event, contact=contact)
    if created:
        log("Response to event %d from contact %s, creating new EventResponse" % (parent_event.pk, str(contact)))
        r.slug = create_slug(length=10)
        r.status = EVENT_RESPONSE_STATUS_AWAITING
        r.event = parent_event
        r.contact = contact
        r.assistant = parent_event.owner_account.get_assistant()
        r.request_email = parent_event.original_email
        r.last_request_sent = now()

    log("procmail: Marking event response from %s as received..." % str(contact))
    if not ((r.status == EVENT_RESPONSE_STATUS_RECEIVED) and (response_type == EVENT_RESPONSE_TYPE_UNKNOWN)):
        r.status = EVENT_RESPONSE_STATUS_RECEIVED
        # if r.response_type in []
        r.child_event = child_event
        r.response_type = response_type
    r.save()

    # Remove/add responder from the event
    if response_type == EVENT_RESPONSE_TYPE_NO:
        Email2Contact.objects.filter(email=parent_event.original_email, contact=contact).delete()
    elif response_type in [EVENT_RESPONSE_TYPE_YES, EVENT_RESPONSE_TYPE_TENTATIVE]:
        if Email2Contact.objects.filter(email=parent_event.original_email, contact=contact, active=True).count() == 0:
            parent_event.original_email.create_email2contact_by_contact(contact, contact.owner_account, type=Email2Contact.TYPE_ADDED)

    # Determine what the new status should be for this event
    responses = EventResponse.objects.filter(event=parent_event)
    if responses:
        total_count, no_count = responses.count(), 0
        for r in responses:
            if r.status == EVENT_RESPONSE_STATUS_RECEIVED and r.response_type == EVENT_RESPONSE_TYPE_NO:
                no_count += 1
        if no_count >= max(total_count - 1, 1):
            new_status = EVENT_STATUS_CANCELED
        elif any(r.status == EVENT_RESPONSE_STATUS_RECEIVED and r.response_type == EVENT_RESPONSE_TYPE_YES for r in responses):
            new_status = EVENT_STATUS_SCHEDULED
        else:
            new_status = parent_event.original_status

        log("new_status:", new_status)
        if new_status != parent_event.status:
            creator_contact = parent_event.get_creator()
            if creator_contact:
                parent_event.status = new_status
                parent_event.save()
                # Don't send event confirmations when the response is crawled, even if the original event was set up by email
                #  and even if the account has assistant_emails_anytime turned on!
                if send_confirmations and not parent_event.from_external_ics and not (mode and mode.get('silent', False)) and (parent_event.end > now()) and (mode.get('source') in Email.ALLOWED_SOURCES_FOR_REMINDERS):
                    original_emails = Email.objects.filter(events=parent_event).order_by('date_sent')
                    # TODO: If it's a BCC mail, don't include the attendees
                    if (new_status == EVENT_STATUS_CANCELED):
                        attach_ics = True
                        additional_contacts = [contact]
                    else:
                        attach_ics = False
                        additional_contacts = []

                    send_event_confirmations(parent_event, original_emails[0], \
                        parent_event.owner_account.get_assistant(),
                        send_conflicts=False, include_attendees=True, additional_contacts=additional_contacts, additional_emails=child_event.original_email, attach_ics=attach_ics, mode=mode)
                    conf_emails_sent = True
                parent_event.save()

    return conf_emails_sent


def format_conflicts_for_display(contact, conflicts, tzinfo):
    display_conflicts = []
    for item in conflicts:
        occurrence, overlap = item['occurrence'], item['class']
        event = occurrence.event
        start = occurrence.start.astimezone(tzinfo)
        end = occurrence.end.astimezone(tzinfo)
        attendees = uniqify(list(event.original_email.contacts.all()))
        attendees = [c for c in attendees if c != contact]
        attendees.sort(key=lambda c: c.get_tag_or_name_for_display(contact))
        display_conflicts.append({
            'date_span':        format_span(occurrence, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=tzinfo, display_date=True),
            'event':            event,
            'attendees':        attendees
        })
    return display_conflicts


def send_self_confirmation(contact, event, email, assistant, conflicts=None, attach_ics=True, mode=None, additional_emails=None):
    # This function MUST be able to work if mode=None (might be called from mark_guessed_events_scheduled)
    try:
        if mode and mode.get('silent', False):
            return

        alias = contact.alias
        if event.source not in Email.ALLOWED_SOURCES_FOR_REMINDERS:
            log ("send_alias_confirmation: event is of source", event.source, "skipping")
            return False

        if not mail.email_supports_ics(alias.email):
            attach_ics = False
        extra_headers = mail.extra_headers_for_reply(email)
        tzinfo = alias.account_user.get_timezone()
        local_start = event.start.astimezone(tzinfo)
        event_date = '%s/%s' % (local_start.month, local_start.day)
        event_time = format_time(event.start, tzinfo=tzinfo)
        day_of_week = calendar.day_name[local_start.weekday()]
        attendees = uniqify(list(event.original_email.contacts.all()))
        attendees = [c for c in attendees if c != contact]
        attendees.sort(key=lambda c: c.get_tag_or_name_for_display(contact))
        if event.status == EVENT_STATUS_TENTATIVE:
            subject_heading = _("[Tentative]")
        elif event.status == EVENT_STATUS_SCHEDULED:
            subject_heading = _("[Confirmed]")
        elif event.status == EVENT_STATUS_CANCELED:
            subject_heading = _("[Canceled]")
        if conflicts is None:
            conflicts = []
        span = Span(event.start, event.end)

        #date_span = format_span(event, display_future_weekdays=True, \
        #    display_today=True, display_tomorrow=True, display_upper=False,
        #    display_both=True, display_sometime=True, tzinfo=tzinfo)
        #Arvi
       
        date_span = format_span(event, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=tzinfo, display_date=True)

        import constants
        context = Context({
            'status':               event.status,
            'constants':            constants,
            'assistant':            assistant,
            'attendees':            attendees,
            'alias':                alias,
            'phone':                event.phone,
            'date':                 event_date,
            'date_span':            date_span,
            'day_of_week':          day_of_week,
            'event':                event,
            'site':                 alias.get_site(),
            'tzinfo':               tzinfo,
            'conflicts':            format_conflicts_for_display(contact, conflicts, tzinfo),
            'old_emails':           mail.find_thread_for_formatting(email, additional_emails=additional_emails),
        })
        mail.send_email('main_confirm_self.html', context,
            subject_heading + ' ' + ellipsize(event.title_long) + ' (' +\
                format_date(event.start, display_upper=True, tzinfo=tzinfo) + ')',
            (assistant.get_full_name(), assistant.email),
            (alias.get_full_name(), alias.email),
            reply_to = (attendees[0].get_full_name(), attendees[0].email) if attendees else None,
            extra_headers = extra_headers,
            noted={
                'message_type': EMAIL_TYPE_NOTED,
                'message_subtype': EMAIL_SUBTYPE_NOTED_CONFIRMATION,
                'event': event,
                'owner_account': assistant and assistant.owner_account
            },\
            ics=[ics.ics_from_events(event, to=contact)] if attach_ics else [],
            # Don't use alias=alias, mode=mode here -- no need to store email sent straight to the owner
        )
        return True
    except:
        log('Could not send_alias_confirmation',traceback.format_exc())
        return False

def send_attendee_confirmation(contact, event, email, assistant, attendee, conflicts=None, attach_ics=True, additional_emails=None, mode=None):
    # This function MUST be able to work if mode=None (might be called from mark_guessed_events_scheduled)
    # contact = event owner/organizer
    # attendee = who we're sending this confirmation to
    try:
        if mode and mode.get('silent', False):
            return

        if event.source not in Email.ALLOWED_SOURCES_FOR_REMINDERS:
            log ("send_attendee_confirmation: event is of source", event.source, "skipping")
            return False

        if (attendee.alias and attendee.alias.account.get_assistant() == assistant):
            self_assistant = True
        else:
            self_assistant = False
            if not contact.owner_account.assistant_emails_others:
                log ("send_attendee_confirmation: Account %s assistant_emails_others=False, skipping" % str(mode['account']))
                return False

        if contact.alias and not contact.alias.send_confirmations:
            log("send_attendee_confirmation: alias ", contact.alias, "has send_confirmations=False, skipping")
            return False

        # TODO: Time zones for contacts
        if attendee.alias and (attendee.alias.account == contact.owner_account):
            display_tz = (attendee.alias.account_user.timezone != contact.alias.account_user.timezone)
        else:
            display_tz = True

        if not mail.email_supports_ics(contact.email):
            attach_ics = False
        if not mail.email_supports_ics(attendee.email):
            attach_ics = False
        if conflicts is None:
            conflicts = []
        extra_headers = mail.extra_headers_for_reply(email)
        if attendee.alias and attendee.alias.account_user:
            tzinfo = attendee.alias.account_user.get_timezone()
        else:
            tzinfo = contact.alias.account_user.get_timezone()
            display_tz = True
        # local_start = event.start.astimezone(tzinfo)
        # local_end = event.end.astimezone(tzinfo)
        # all_day = is_all_day(local_start, local_end)
        span = Span(event.start, event.end)
        #date_span = format_span(span, display_future_weekdays=True, \
        #    display_today=True, display_tomorrow=True, display_upper=False,
        #    display_both=True, display_sometime=True, display_tz=display_tz, tzinfo=tzinfo)

        date_span = format_span(span, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=tzinfo, display_date=True)


        import constants
        c = Context({\
            # Do *NOT* add 'alias' here -- we don't want to display the Alias's login URL to the attendee!
            # We use creator_alias instead
            'creator_alias':        contact.alias,
            'assistant':            assistant,
            'intro':                not attendee.contacted_by_assistant,
            'self_assistant':       self_assistant,
            'assistant_alias':      contact.alias,

            # 'day_of_week':          calendar.day_name[local_start.weekday()],
            # 'date':                 '%d/%d' % (local_start.month, local_start.day),
            # 'time_start':           format_time(local_start),
            # 'time_end':             format_time(local_end),
            # 'all_day':              all_day,
            'phone':                event.phone,
            'status':               event.status,
            'constants':            constants,
            'date_span':            date_span,
            'event':                event, #added by Alex to include event_text
            'site':                 contact.alias.get_site(),
            'tzinfo':               tzinfo,
            'conflicts':            format_conflicts_for_display(contact, conflicts, tzinfo),
            'old_emails':           mail.find_thread_for_formatting(email, additional_emails=additional_emails),
        })

        date_span_subject = format_span(span, display_future_weekdays=True, \
            display_today=True, display_upper=True, display_both=True, display_sometime=True, display_tz=display_tz, tzinfo=tzinfo)

        if event.status == EVENT_STATUS_TENTATIVE:
            if attach_ics:
                subject_heading = _("[Please confirm]")
            else:
                subject_heading = _("[Tentative]")
        elif event.status == EVENT_STATUS_SCHEDULED:
            subject_heading = _("[Confirmed]")
        elif event.status == EVENT_STATUS_CANCELED:
            subject_heading = _("[Canceled]")
        mail.send_email('main_confirm_friend.html', c,
            subject_heading + ' ' + ellipsize(event.title_long) + ' (' + date_span_subject + ')',
            (assistant.get_full_name(), assistant.email),
            (attendee.get_full_name(), attendee.email),
            reply_to = (contact.get_full_name(), contact.email),
            extra_headers = extra_headers,
            noted={'message_type': EMAIL_TYPE_NOTED,\
                'message_subtype': EMAIL_SUBTYPE_NOTED_CONFIRMATION,
                'event': event,
                'owner_account': assistant and assistant.owner_account,
            },
            ics=[ics.ics_from_events(event, to=attendee)] if attach_ics else [],
            alias=contact.alias, mode=mode
        )

        if attendee:
            attendee.contacted_by_assistant = True
            attendee.save()

        return True
    except:
        log('Could not send_attendee_confirmation',traceback.format_exc())
        return False

def correct_wordpunct(wp_tokens):
    '''Rejoin some things that wordpunct splits, that we don't want it to split:
    - phone numbers like 444-234-5678, 555-1234
    - tags like @elizabeth, tagged datetime stuff like @5:30
    - (colon-separated times like 12:34 are handled in the Chronic parser instead)
    '''

    words = []
    i = 0
    while i < len(wp_tokens):
#        if words and (words[-1] == '@') and (re.match(r'[A-Za-z0-9]\w*', wp_tokens[i])):
#            words[-1] += wp_tokens[i]
#            i += 1
        if words and (re.match(r'[\-0-9]+-', words[-1])) and (re.match(r'\d{3}\d*', wp_tokens[i])):
            words[-1] += wp_tokens[i]
            i += 1
        elif words and (re.match(r'\d{3}\d*', words[-1])) and wp_tokens[i] == '-':
            words[-1] += wp_tokens[i]
            i += 1
        else:
            words.append(wp_tokens[i])
            i += 1
    return words

def extract_items_from_text(text, now_date, mode):

    # Tokenize
    text = ' '.join(text.split())

    # This re tokenizes our text, keeping intact @ symbols with their words and leaving alone
    # phone numbers like 555-222-1234, but otherwise splitting along punctuation.

    text_chronic = RE_EMAIL_SEARCH.sub('', text)
    text_chronic = RE_URL.sub('', text_chronic)

    words = [m[0] for m in RE_TOKENIZE.findall(text_chronic)]
    words_lower = [word.lower() for word in words]

    # wordpunct_tokenize splits out
    p = parsedate.extended.ParseDateExtended(options={'now': now_date})
    spans = []

#    # Some magic words we can use, that often appear...
#    m = re.search(r"(?i)WHEN:(.+?)$", text)
#    if m:
#        span = p.parse(m.group(1))
#        if span:
#            spans.append(span)

    orig_chronic_tokens, chronic_tokens = [], []

    if re.search(r'(^|\s)@', text) or (not mode.get('require_at_symbol', True)):
        # Call Chronic
        orig_chronic_tokens, chronic_tokens, spans = p.find_spans(words)

    event_summary = ellipsize(text, length_min = SUMMARY_LENGTH_MAX-20, length_max=SUMMARY_LENGTH_MAX, length_with_url_max=SUMMARY_LENGTH_MAX+25)

    tentative = bool(RE_QUES.search(text))

    # Extract tags, transform them if necessary
    raw_tags = [t[2] for t in re.findall(r'(^|[^\w])(@(\w+(\.\w+)?))', text)]
    tags = []
    for tag in raw_tags:
        tag = tag.lower()
        # Discard tag if it's a special date word
        discard = False
        for token in chronic_tokens:
            if token.tags and (token.orig_word.lower() == tag):
                discard = True
        for token in orig_chronic_tokens:
            if token.pre_normalized and (token.orig_word.lower() == tag):
                discard = True
        if discard:
            continue
        if tag in TAG_SHORTCUTS:
            tags.append(TAG_SHORTCUTS[tag])
        else:
            tags.append(tag)

    # Search for phone numbers
    phone = None
    m = re.search(r"\b\(?(?P<area>\d\d\d)[)-.]? ?(?P<prefix>\d\d\d)( ?[-.] ?| )(?P<number>\d\d\d\d)\b", text)
    if m:
        if m.group('area'):
            phone = '%s-%s-%s' % (m.group('area'), m.group('prefix'), m.group('number'))
        else:
            phone = '%s-%s' % (m.group('prefix'), m.group('number'))

    log("phone:", phone)

    # TODO: Associate contacts, summaries with particular spans
    items = []
    for span in spans:
        items.append({\
            'span':     span,
            'at_symbol':    (hasattr(span, 'peak_type') and span.peak_type == CHRONIC_PEAK_TAGGED),
            'summary':  event_summary,
            'description':  '',
            'phone':    phone,
            'tentative': tentative
        })

    overall = ({\
        'summary':  event_summary,
        'description':  '',
        'phone':    phone,
        'tags':     tags
    })

    debug_content = []              # Not used much anymore

    return overall, items, debug_content

def extract_items_from_content(subject, content, now_date, mode):

    stats = mode.get('stats', None)
    if stats: stats.start('subject_items')
    subject_overall, subject_items, subject_debug_content = extract_items_from_text(subject, now_date, mode)
    if stats: stats.end('subject_items')
    if stats: stats.start('body_items')
    body_overall, body_items, body_debug_content = extract_items_from_text(content, now_date, mode)
    if stats: stats.end('body_items')

    result_item = {
        'tentative':        False,
        'description':      ''
        # We'll fill in span and summary later
    }

    spans = []

    if subject_overall['phone']:
        result_item['phone'] = subject_overall['phone']
    else:
        result_item['phone'] = body_overall['phone']

    if subject_items:
        item = subject_items[0]
        #result_item['span'] = item['span']
        spans.append(item['span'])
        result_item['summary'] = subject
        result_item['summary_includes_date'] = True
        if not subject.strip():
            result_item['summary'] += ' ' + body_overall['summary']
        result_item['tentative'] |= item['tentative']
    else:
        if body_items:
            item = body_items[0]
            result_item['span'] = item['span']
            result_item['summary'] = subject_overall['summary'] + ' ' + item['summary']
            result_item['summary_includes_date'] = False

    if body_items:
        item = body_items[0]
        result_item['tentative'] |= item['tentative']
        spans.append(item['span'])

    # Find the One True Span...
    spans = [item['span'] for item in subject_items]
    spans.extend([item['span'] for item in body_items])

    result_span = None
    for span in spans:
        if span.width() < ONE_DAY and ((not result_span) or (span.width() < result_span.width()) or (span.peak_type > result_span.peak_type)):
            result_span = span
    if result_span is None:
        for span in spans:
            if result_span is None:
                result_span = span
            else:
                if span.peak_type > result_span.peak_type:
                    result_span = span

    result_item['span'] = result_span
    result_item['at_symbol'] = hasattr(result_span, 'peak_type') and (result_span.peak_type == CHRONIC_PEAK_TAGGED)

    tags = subject_overall['tags'] + body_overall['tags']
    debug_content = subject_debug_content + body_debug_content
    if result_item:
        return [result_item], tags, subject_overall, body_overall, debug_content
    else:
        return [], tags, subject_overall, body_overall, debug_content
